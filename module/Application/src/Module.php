<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014-2016 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
   
    public function getServiceConfig(){
        return [
            'factories' => [
                'application_template_render'=>  function ($sm){
                    $cmf = new \Application\Template\Render();
                    $cmf->setServiceManager($sm);
                    return $cmf;
                }, 
           ]    
        ];
    }            
}
