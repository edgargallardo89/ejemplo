<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoreMotivale
 *
 * @ORM\Table(name="core_motivale")
 * @ORM\Entity(repositoryClass="Application\Repository\CoreMotivaleRepository")
 */
class CoreMotivale
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=255, nullable=false)
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(name="estatus", type="string", nullable=false)
     */
    private $estatus;


}

