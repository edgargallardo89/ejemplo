<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoreUserAddresses
 *
 * @ORM\Table(name="core_user_addresses", indexes={@ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CoreUserAddressesRepository")
 */
class CoreUserAddresses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="ext_number", type="string", length=255, nullable=true)
     */
    private $extNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="int_number", type="string", length=255, nullable=true)
     */
    private $intNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=true)
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="town", type="string", length=255, nullable=true)
     */
    private $town;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var boolean
     *
     * @ORM\Column(name="main", type="boolean", nullable=false)
     */
    private $main;

    /**
     * @var \Application\Entity\OauthUsers
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\OauthUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return CoreUserAddresses
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     *
     * @return CoreUserAddresses
     */
    public function setStreet(string $street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtNumber()
    {
        return $this->extNumber;
    }

    /**
     * @param string $extNumber
     *
     * @return CoreUserAddresses
     */
    public function setExtNumber(string $extNumber)
    {
        $this->extNumber = $extNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntNumber()
    {
        return $this->intNumber;
    }

    /**
     * @param string $intNumber
     *
     * @return CoreUserAddresses
     */
    public function setIntNumber(string $intNumber = null)
    {
        $this->intNumber = $intNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     *
     * @return CoreUserAddresses
     */
    public function setZipCode(string $zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     *
     * @return CoreUserAddresses
     */
    public function setReference(string $reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     *
     * @return CoreUserAddresses
     */
    public function setLocation(string $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return CoreUserAddresses
     */
    public function setCity(string $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param string $town
     *
     * @return CoreUserAddresses
     */
    public function setTown(string $town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return CoreUserAddresses
     */
    public function setState(string $state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isMain()
    {
        return $this->main;
    }

    /**
     * @param boolean $main
     *
     * @return CoreUserAddresses
     */
    public function setMain(bool $main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * @return OauthUsers
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param OauthUsers $user
     *
     * @return CoreUserAddresses
     */
    public function setUser(OauthUsers $user)
    {
        $this->user = $user;

        return $this;
    }
}

