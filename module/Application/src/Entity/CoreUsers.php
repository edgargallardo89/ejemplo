<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoreUsers
 *
 * @ORM\Table(name="core_users", uniqueConstraints={@ORM\UniqueConstraint(name="AK_username", columns={"username"})}, indexes={@ORM\Index(name="role_id", columns={"role_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CoreUsersRepository")
 */
class CoreUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $displayName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $enabled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="profile_fulfilled", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     */
    private $profileFulfilled;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone1", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $telephone1;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone2", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $telephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $mobile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="job_title", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $jobTitle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $modifiedAt;

    /**
     * @var \Application\Entity\CoreUsers
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\CoreUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $createdBy;

    /**
     * @var \Application\Entity\OauthUsers
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\OauthUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="username", referencedColumnName="username", nullable=true)
     * })
     */
    private $username;

    /**
     * @var \Application\Entity\CoreRoles
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\CoreRoles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $role;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return CoreUsers
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return CoreUsers
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return CoreUsers
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return CoreUsers
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return CoreUsers
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return CoreUsers
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set profileFulfilled
     *
     * @param boolean $profileFulfilled
     *
     * @return CoreUsers
     */
    public function setProfileFulfilled($profileFulfilled)
    {
        $this->profileFulfilled = $profileFulfilled;

        return $this;
    }

    /**
     * Get profileFulfilled
     *
     * @return boolean
     */
    public function getProfileFulfilled()
    {
        return $this->profileFulfilled;
    }

    /**
     * Set telephone1
     *
     * @param string $telephone1
     *
     * @return CoreUsers
     */
    public function setTelephone1($telephone1)
    {
        $this->telephone1 = $telephone1;

        return $this;
    }

    /**
     * Get telephone1
     *
     * @return string
     */
    public function getTelephone1()
    {
        return $this->telephone1;
    }

    /**
     * Set telephone2
     *
     * @param string $telephone2
     *
     * @return CoreUsers
     */
    public function setTelephone2($telephone2)
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    /**
     * Get telephone2
     *
     * @return string
     */
    public function getTelephone2()
    {
        return $this->telephone2;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return CoreUsers
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return CoreUsers
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return CoreUsers
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     *
     * @return CoreUsers
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CoreUsers
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return CoreUsers
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set createdBy
     *
     * @param \Application\Entity\CoreUsers $createdBy
     *
     * @return CoreUsers
     */
    public function setCreatedBy(\Application\Entity\CoreUsers $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Application\Entity\CoreUsers
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set username
     *
     * @param \Application\Entity\OauthUsers $username
     *
     * @return CoreUsers
     */
    public function setUsername(\Application\Entity\OauthUsers $username = null)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return \Application\Entity\OauthUsers
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set role
     *
     * @param \Application\Entity\CoreRoles $role
     *
     * @return CoreUsers
     */
    public function setRole(\Application\Entity\CoreRoles $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \Application\Entity\CoreRoles
     */
    public function getRole()
    {
        return $this->role;
    }
}

