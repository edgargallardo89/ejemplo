<?php



/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */

namespace Application\Import;

/**

 * Description of Import

 *

 * @author dev

 */

use Application\Import\Csv\Import;

use Doctrine\ORM\EntityManager;

use Application\Entity\CoreFileUploads;

use Application\Entity\OauthUsers;



class ImportFactory

{



    protected $identity;

    /**

     *

     * @var type 

     */

    protected $config;

    

    /**

     *

     * @var type 

     */

    protected $em;   



    /**

     * 

     * @return type

     */

    function getConfig() {

        return $this->config['adbox-std-import'];

    }



    /**

     * 

     * @param type $config

     */

    function setConfig($config) {

        $this->config = $config;

    }



    /**

     * 

     * @param type $services

     * @return \Application\Import\ImportFactory

     */

    public function __invoke($services) {

        $config = $services->get('config');

        $this->setConfig($config);

        $this->em = $services->get(EntityManager::class);

        $this->identity = $services->get('authentication')

                ->getIdentity()->getAuthenticationIdentity();        

        return $this;

    }

   

    /**

     * Imports CSV file and returns its content

     * 

     * @param  array $data

     * @return string

     * @throws \InvalidArgumentException

     */

    public function importCsv($data)

    {

        $config = $this->getConfig();

        $excepcion =  'Max of rows'.$config['maxfieldcsv'];        

        $import = new Import();

        $filename = $data['tmp_name'];

        $csv = $import->import(

                $filename,

                $config['useFirstRecordAsHeader'],

                $config['delimiter']

                );



        $rowsCount = $import->count();

        

        if ($rowsCount <= $config['maxfieldcsv'] and $rowsCount > 0) {

            return $csv;   

        }

        

        $excepcion = (0 === $rowsCount)

            ? 'Invalid_file_format_empty' 

            : 'Invalid_file_format_maxExceeded';

        

        throw new \InvalidArgumentException($excepcion);

    }



}