<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Application\Entity\CoreCedis;
use Application\Entity\CoreConfigs;
use Application\Entity\CoreGroups;
use Application\Entity\CoreMessages;
use Application\Entity\CoreRoles;
use Application\Entity\CoreSendemail;
use Application\Entity\CoreUserAddresses;
use Application\Entity\OauthAccessTokens;
use Application\Entity\OauthUsers;
use Doctrine\ORM\EntityRepository;
use Zend\Crypt\Password\Bcrypt;

/**
 *
 */
class CoreUsersRepository extends EntityRepository
{
    public function fetchByOne($username)
    {
        return $this
            ->createQueryBuilder('U')->select('R.role')
            ->join('U.role', 'R')
            ->where('U.username = :username')->setParameter('username', $username)
            ->getQuery()->getSingleResult();
    }

    /**
     * Create User
     *
     * @param object $data
     * @return mixed
     */
    public function create($data, $params)
    {
        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($data, $currentRepo, $params) {

            $userExists = $currentRepo->exists($data->email);

            if (true === $userExists) {
                throw new \Exception('User_Exists');
            }

            $oauthUser = new \Application\Entity\OauthUsers();
            $user = new \Application\Entity\CoreUsers();

            //Secure Password non-encrypted
            $password = bin2hex(random_bytes(4));

            //Generate Secure Password usign BCrypt
            $bcrypt = new Bcrypt();
            $securePass = $bcrypt->create($password);

            $oauthUser
                ->setUsername($data->email)
                ->setPassword($securePass);

            $em->persist($oauthUser);

            //Register Role (id: 2)
            $role = $em->getReference(CoreRoles::class, 2);

            $user
                ->setCreatedAt(new \DateTime())
                ->setEmail($data->email)
                ->setFirstName($data->name)
                ->setLastName($data->lastname)
                ->setProfileFulfilled(0)
                ->setEnabled(1)
                ->setRole($role)
                ->setUsername($oauthUser);

            $em->persist($user);
            $params['email'] = $data->email;

            $senemailEntry = (new CoreSendemail())
                ->setTemplate($params['template'])
                ->setDateAdded(new \DateTime())
                ->setFrom($params['from'])
                ->setTo($params['email'])
                ->setCc($params['cc'])
                ->setSubject($params['subject'])
                ->setStatus('new');
            $em->persist($senemailEntry);

            return $password;
        });
    }

    /**
     * User-performed Profile fulfillment
     *
     * @param integer $id
     * @param array $data
     * @return boolean
     */
    public function fulfillProfile($id, $data)
    {
        /*
         * Profile fulfillment Transaction Workflow
         * * * Notes : https://bis-adventa.atlassian.net/wiki/display/ADBOX/Complemento+de+datos
         */
        $currentRepo = $this;

        $this->_em->transactional(function () use ($currentRepo, $id, $data) {
            $user = $currentRepo->find($id);
            $oauthUser = $user->getUsername();

            $user
                //->setUsername()
                //->setEmail($data->email)
                ->setFirstName($data->firstName)
                ->setLastName($data->lastName)
                ->setMobile($data->mobile)
                ->setModifiedAt(new \DateTime())
                ->setProfileFulfilled(1)
                ->setSurname($data->surname)
                ->setTelephone1($data->telephone1)
                ->setTelephone2($data->telephone2);

            //If password is submitted: Generate Secure Password usign BCrypt
            if (isset($data->password)) {
                $oauthUser->setPassword((new Bcrypt())->create($data->password));
            }

            //If no creator is set, user assumed as User-created account 
            //and
            //If submitted email is not equals to stored email: Set email and oauth user identifier
            if ((null === $user->getCreatedBy()) and ($user->getEmail() !== $data->email)) {
                $oauthUser->setUsername($data->email);
                $user->setEmail($data->email);
                $user->setUsername($oauthUser);
            }

            return true;
        });
    }

    /**
     * Check existing User
     *
     * @param string $identifier
     * @return boolean
     */
    public function exists($identifier)
    {
        $user = $this->findOneBy(array('email' => $identifier));

        return !(null === $user);
    }

    /**
     * Profile Data
     *
     * @param string $identifier
     * @return array
     */
    public function getProfile($identifier)
    {
        /*
         * Profile data Transaction Workflow
         * * * Notes : (None)
         * .---------------------------------------------.
         * .                    Steps                    |
         * .---------------------------------------------.
         * . 1) Get Core User data (core_users)          .
         * . 2) Get Messages (core_messages)             .
         * . 3) Get Private Config values (core_configs) .
         * .---------------------------------------------.
         */

        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($currentRepo, $identifier) {
            //Get Core User
            $user = $currentRepo
                ->createQueryBuilder('U')
                ->where('U.username = :username')
                ->setParameter('username', $identifier, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

            //Get Messages (a.k.a. translations)
            $messages = $em->getRepository(CoreMessages::class)
                ->createQueryBuilder('M')
                ->select('M.key,M.value')
                ->where('M.visibility = :visibility')
                ->setParameter('visibility', 0, \Doctrine\DBAL\Types\Type::BOOLEAN)
                ->getQuery()
                ->getArrayResult();

            //Get Private Site Configs
            $configs = $em->getRepository(CoreConfigs::class)
                ->createQueryBuilder('M')
                ->select('M.key,M.value')
                ->where('M.visibility = :visibility')
                ->setParameter('visibility', 0, \Doctrine\DBAL\Types\Type::BOOLEAN)
                ->getQuery()
                ->getArrayResult();

            //Reorder config as key-value array
            $config = array();
            foreach ($configs as $item) {
                $config[$item['key']] = $item['value'];
            }

            //Reorder messages as key-value array
            $lang = array();
            foreach ($messages as $message) {
                $lang[$message['key']] = $message['value'];
            }

            //Date Modifiers
            $user['createdAt'] = $user['createdAt']->format('Y-m-d H:i:s');
            $user['modifiedAt'] = (null !== $user['modifiedAt']) ? $user['modifiedAt']->format('Y-m-d H:i:s') : null;

            $user['updatableFields'] = [
                ['name' => 'firstName'],
                ['name' => 'lastName'],
                ['name' => 'telephone1'],
                ['name' => 'telephone2'],
                ['name' => 'mobile'],
                ['name' => 'password'],
            ];

            if (true === (bool)$user['profileFulfilled']) {
                $user['updatableFields'][] = ['name' => 'email'];
            }

            return [
                'config' => array_merge(['lang' => $lang], $config),
                'user' => array_merge(
                    $user,
                    [
                        'permissions' => [
                            'permission-sample' => [
                                'allow' => ['GET', 'POST']
                            ],
                            'permission-sample-2' => [
                                'allow' => ['GET', 'POST']
                            ],
                        ]
                    ]
                )
            ];
        });
    }

    /**
     * Get User data
     *
     * @param string $identifier
     * @return array
     */
    public function getUser($identifier)
    {
        /*
         * User data Transaction Workflow
         * * * Notes : (None)
         * .---------------------------------------------.
         * .                    Steps                    |
         * .---------------------------------------------.
         * . 1) Get Core User data (core_users)          .
         * .---------------------------------------------.
         */

        $currentRepo = $this;

        return $this->_em->transactional(function () use ($currentRepo, $identifier) {
            //Get Core User
            $user = $currentRepo
                ->createQueryBuilder('U')
                ->where('U.username = :username')
                ->setParameter('username', $identifier, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

            $user['createdAt'] = $user['createdAt']->format('Y-m-d H:i:s');
            $user['modifiedAt'] = (null !== $user['modifiedAt']) ? $user['modifiedAt']->format('Y-m-d H:i:s') : null;

            return $user;
        });
    }

    public function hasEnabledUser($identifier)
    {

        $currentRepo = $this;

        return $this->_em->transactional(function () use ($currentRepo, $identifier) {
            //Get Core User
            $user = $currentRepo
                ->createQueryBuilder('U')
                ->where('U.username = :username')
                ->setParameter('username', $identifier, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

            return $user;
        });
    }

    /**
     * Creates a new user without email notification and optional address
     *
     * @param mixed|array $data
     * @return bool|mixed
     */
    public function createSimple($data)
    {
        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($currentRepo, $data) {
            $oauthUsersRepo = $em->getRepository(OauthUsers::class);

            // Hack: if it's TRUE it means it's a single user, false equals multiple
            if (isset(((array)$data)['email'])) {
                $this->insertData($oauthUsersRepo, $data);
            } else {
                foreach ($data as $row) {
                    $this->insertData($oauthUsersRepo, (object)$row);
                }
            }

            return ["message" => "success"];
        });
    }

    /**
     * Creates users from csv upload
     *
     * @param array $data
     * @return bool|mixed
     */
    public function uploadCsv($data)
    {
        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($currentRepo, $data) {
            $oauthUsersRepo = $em->getRepository(OauthUsers::class);

            return $oauthUsersRepo->bulkCreate(
                $data['users']
            );
        });
    }

    /**
     * Insert logic separated from createSimple($data)
     *
     * @param $oauthUsersRepo
     * @param $data
     * @throws \Exception
     */
    private function insertData($oauthUsersRepo, $data)
    {
        $em = $this->_em;

        if ($oauthUsersRepo->exists($data->email) === true) {
            throw new \Exception('User_Exists');
        }

        // Create user
        $oauthUser = new OauthUsers();

        // Basic information
        $oauthUser
            ->setUsername($data->email)
            ->setEmail($data->email)
            ->setFirstName($data->firstName)
            ->setLastName($data->lastName)
            ->setDisplayName($data->firstName . ' ' . $data->lastName . (property_exists($data, 'surname') ? ' ' . $data->surname : ''))
            ->setSurname(property_exists($data, 'surname') ? $data->surname : null)
            ->setTelephone1(property_exists($data, 'telephone1') ? $data->telephone1 : null)
            ->setTelephone2(property_exists($data, 'telephone2') ? $data->telephone2 : null)
            ->setMobile(property_exists($data, 'mobile') ? $data->mobile : null)
            ->setCreatedAt(new \DateTime())
            ->setProfileFulfilled(0)
            ->setEnabled(1);

        // Set password
        $bcrypt = new Bcrypt();

        $oauthUser->setPassword($bcrypt->create($data->password));

        // Set profile
        $role = $em->getRepository(CoreRoles::class)->find($data->roleId);

        if ($role === null) {
            throw new \Exception('Invalid_Role');
        }

        $oauthUser->setRole($role);

        // Set group
        $group = $em->getRepository(CoreGroups::class)->find($data->groupId);

        if ($group === null) {
            throw new \Exception('Invalid_Group');
        }

        $oauthUser->addGroup($group);
        $group->addUser($oauthUser);

        // Save
        $em->persist($oauthUser);

        // Set address
        $deliveryMode = $em->getRepository(CoreConfigs::class)->findByKey('checkout.delivery.mode');
        $mode = $deliveryMode['value'];

        switch ($mode) {
            case 'cedis':
                // Field validation
                if (!property_exists($data, 'cedis')) {
                    break;
                }

                // Properties validation
                $cedisArray = $data->cedis;

                if ($this->isKeyValid($cedisArray, 'id')) {
                    // New CeDis
                    $cedis = $em->getRepository(CoreCedis::class)->find($cedisArray['id']);

                    if ($cedis === null) {
                        throw new \Exception('Invalid_Cedis');
                    }

                    // Add new CeDis
                    $oauthUser->addCedis($cedis);
                    $cedis->addUser($oauthUser);
                } else {
                    throw new \Exception('Invalid_Cedis_Data');
                }
                break;
            case 'user-address':
                // Field validation
                if (!property_exists($data, 'address')) {
                    break;
                }

                // Properties validation
                $addressArray = $data->address;

                if ($this->isKeyValid($addressArray, 'street')
                    && $this->isKeyValid($addressArray, 'extNumber')
                    && $this->isKeyValid($addressArray, 'intNumber', false)
                    && $this->isKeyValid($addressArray, 'zipCode')
                    && $this->isKeyValid($addressArray, 'reference')
                    && $this->isKeyValid($addressArray, 'location')
                    && $this->isKeyValid($addressArray, 'city', false)
                    && $this->isKeyValid($addressArray, 'town')
                    && $this->isKeyValid($addressArray, 'state')
                ) {
                    // New address
                    $address = new CoreUserAddresses();

                    $address
                        ->setUser($oauthUser)
                        ->setMain(true)
                        ->setStreet($addressArray['street'])
                        ->setExtNumber($addressArray['extNumber'])
                        ->setZipCode($addressArray['zipCode'])
                        ->setReference($addressArray['reference'])
                        ->setLocation($addressArray['location'])
                        ->setTown($addressArray['town'])
                        ->setState($addressArray['state']);

                    if (isset($addressArray['intNumber'])) {
                        $address->setIntNumber($addressArray['intNumber']);
                    }

                    if (isset($addressArray['city'])) {
                        $address->setCity($addressArray['city']);
                    }

                    $em->persist($address);
                } else {
                    throw new \Exception('Invalid_Address_Data');
                }
                break;
            default:
                throw new \Exception('Invalid_Checkout_Delivery_Mode');
        }
    }

    /**
     * @param string $username
     * @param mixed|array $data
     * @param bool $fromAdmin
     *
     * @return bool|mixed
     */
    public function editInformation($username, $data, $fromAdmin = false)
    {
        return $this->_em->transactional(function ($em) use ($username, $data, $fromAdmin) {
            $oauthUsersRepo = $em->getRepository(OauthUsers::class);
            $oauthUser = $oauthUsersRepo->findOneBy(['username' => $username]);

            // Validate email existence
            if ($oauthUsersRepo->existing($oauthUser->getId(), $data->email)) {
                throw new \Exception('User_Exists');
            }

            // Update information
            $oauthUser
                ->setFirstName($data->firstName)
                ->setLastName($data->lastName)
                ->setDisplayName($data->firstName . ' ' . $data->lastName . (property_exists($data, 'surname') ? ' ' . $data->surname : ''))
                ->setSurname(property_exists($data, 'surname') ? $data->surname : null)
                ->setTelephone1(property_exists($data, 'telephone1') ? $data->telephone1 : null)
                ->setTelephone2(property_exists($data, 'telephone2') ? $data->telephone2 : null)
                ->setMobile($data->mobile)
                ->setModifiedAt(new \DateTime());

            // Called from admin/users/...
            if ($fromAdmin) {
                $role = $em->getRepository(CoreRoles::class)->find($data->roleId);

                if ($role === null) {
                    throw new \Exception('Invalid_Role');
                }

                $oauthUser
                    ->setRole($role)
                    ->setProfileFulfilled($data->profileFulfilled)
                    ->setEnabled($data->enabled);

                // Edit user group
                $group = $em->getRepository(CoreGroups::class)->find($data->groupId);

                if ($group === null) {
                    throw new \Exception('Invalid_Group');
                }

                $groups = $oauthUser->getGroups();

                foreach ($groups as $row) {
                    $oauthUser->removeGroup($row);
                    $row->removeUser($oauthUser);
                }

                $oauthUser->addGroup($group);
                $group->addUser($oauthUser);
            }

            // Update username if email has been changed
            if ($oauthUser->getEmail() !== $data->email) {
                $bcrypt = new Bcrypt();

                // Apply password validation when it's not called from administrator
                if (!$fromAdmin) {
                    if (!property_exists($data, 'password') ||
                        !$bcrypt->verify($data->password, $oauthUser->getPassword())
                    ) {
                        throw new \Exception('Password_Mismatch');
                    }
                }

                $oldUsername = $oauthUser->getUsername();

                // When username equals email then update the username to the new mail
                if ($oldUsername == $oauthUser->getEmail()) {
                    $oauthUser->setUsername($data->email);
                    $em->getRepository(OauthAccessTokens::class)->updateTokensByUser($oldUsername, $data->email);
                }

                $oauthUser->setEmail($data->email);

                
            }

            return ["message" => "success"];
        });
    }

    /**
     * @param string $username
     * @param mixed|array $data
     * @return mixed
     */
    public function editPassword($username, $data)
    {
        return $this->_em->transactional(function ($em) use ($username, $data) {
            $oauthUsersRepo = $em->getRepository(OauthUsers::class);
            $oauthUser = $oauthUsersRepo->findOneBy(['username' => $username]);

            $bcrypt = new Bcrypt();

            if (!$bcrypt->verify($data->currentPassword, $oauthUser->getPassword())) {
                throw new \Exception('Password_Mismatch');
            } elseif ($bcrypt->verify($data->password, $oauthUser->getPassword())) {
                throw new \Exception('Current_Password');
            } else {
                $oauthUser->setPassword($bcrypt->create($data->password));
            }

            return ["message" => "success"];
        });
    }

    /**
     * @param string $username
     * @param mixed|array $data
     * @return bool|mixed
     */
    public function editDelivery($username, $data)
    {
        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($currentRepo, $username, $data) {
            // Read configuration
            $deliveryMode = $em->getRepository(CoreConfigs::class)->findByKey('checkout.delivery.mode');
            $deliveryUserEditable = $em->getRepository(CoreConfigs::class)->findByKey('checkout.delivery.userEditable');

            $mode = $deliveryMode['value'];
            $userEditable = $deliveryUserEditable['value'];

            // Validate configuration
            if ($userEditable == 'editable') {
                $oauthUser = $em->getRepository(OauthUsers::class)->findOneBy(['username' => $username]);

                switch ($mode) {
                    case 'cedis':
                        // Field validation
                        if (!property_exists($data, 'cedis')) {
                            throw new \Exception('Missing_Cedis_Data');
                        }

                        // Properties validation
                        $cedisArray = $data->cedis;

                        if ($currentRepo->isKeyValid($cedisArray, 'id')) {
                            // Remove old CeDis
                            if ($oauthUser->getCedis()->count() > 0) {
                                $oldCedis = $oauthUser->getCedis()->first();

                                // Remove
                                $oauthUser->removeCedis($oldCedis);
                                $oldCedis->removeUser($oauthUser);
                            }

                            // New CeDis
                            $cedis = $em->getRepository(CoreCedis::class)->find($cedisArray['id']);

                            if ($cedis === null) {
                                throw new \Exception('Invalid_Cedis');
                            }

                            // Add new CeDis
                            $oauthUser->addCedis($cedis);
                            $cedis->addUser($oauthUser);
                        } else {
                            throw new \Exception('Invalid_Cedis_Data');
                        }
                        break;
                    case 'user-address':
                        // Field validation
                        if (!property_exists($data, 'address')) {
                            throw new \Exception('Missing_Address_Data');
                        }

                        // Properties validation
                        $addressArray = $data->address;

                        if ($currentRepo->isKeyValid($addressArray, 'street')
                            && $currentRepo->isKeyValid($addressArray, 'extNumber')
                            && $currentRepo->isKeyValid($addressArray, 'intNumber', false)
                            && $currentRepo->isKeyValid($addressArray, 'zipCode')
                            && $currentRepo->isKeyValid($addressArray, 'reference')
                            && $currentRepo->isKeyValid($addressArray, 'location')
                            && $currentRepo->isKeyValid($addressArray, 'city', false)
                            && $currentRepo->isKeyValid($addressArray, 'town')
                            && $currentRepo->isKeyValid($addressArray, 'state')
                        ) {
                            $address = $em->getRepository(CoreUserAddresses::class)->findOneBy(['user' => $oauthUser->getId()]);

                            // If address doesn't exist then create a new one
                            if ($address === null) {
                                $address = new CoreUserAddresses();
                                $address->setMain(true);
                                $address->setUser($oauthUser);

                                $em->persist($address);
                            }

                            // Update new values
                            $address
                                ->setStreet($addressArray['street'])
                                ->setExtNumber($addressArray['extNumber'])
                                ->setZipCode($addressArray['zipCode'])
                                ->setReference($addressArray['reference'])
                                ->setLocation($addressArray['location'])
                                ->setTown($addressArray['town'])
                                ->setState($addressArray['state']);

                            if (isset($addressArray['intNumber'])) {
                                $address->setIntNumber($addressArray['intNumber']);
                            }

                            if (isset($addressArray['city'])) {
                                $address->setCity($addressArray['city']);
                            }
                        } else {
                            throw new \Exception('Invalid_Address_Data');
                        }
                        break;
                    default:
                        throw new \Exception('Invalid_Configuration');
                }
            } else {
                throw new \Exception('Address_Non_Editable');
            }

            return ["message" => "success"];
        });
    }

    /**
     * Verifies if a key exists in the searched array and has a value.
     *
     * @param array $search
     * @param string $key
     * @param bool $required
     * @return bool
     */
    private function isKeyValid($search, $key, $required = true)
    {
        return !$required || (key_exists($key, $search) && isset($search[$key]) && $search[$key] !== "");
    }
}
