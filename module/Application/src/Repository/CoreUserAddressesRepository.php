<?php

namespace Application\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class CoreUserAddressesRepository extends EntityRepository
{
    public function getAddress($id)
    {
        $query = $this
            ->createQueryBuilder('A')
            ->where('A.user = :id')
            ->setParameter('id', $id, Type::INTEGER)
            ->setMaxResults(1);

        try {
            return $query->getQuery()->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $e) {
            return null;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}