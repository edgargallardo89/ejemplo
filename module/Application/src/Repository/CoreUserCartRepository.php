<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\CoreUserCart;
use Application\Entity\CoreUserCartItems;

/**
 * 
 */
class CoreUserCartRepository extends EntityRepository
{    
    /**
     * Get User Cart (Data array)
     * 
     * @param string $username
     * @return array
     */
    public function getCart($username, $serverUrl)
    {
        $currentRepo = $this;
        
        /*
         * Cart data
         * * * Notes : (None)
         * .----------------------------------------.
         * .                 Steps                  |
         * .----------------------------------------.
         * . 1) Get Cart Items data (core_products) .
         * . 2) Get Cart totals data                .
         * . 3) Get Cart version                    .
         * .----------------------------------------.
         */
        return $this->_em->transactional(function ($em) use($username, $serverUrl, $currentRepo) {            
            $itemsRepo = $em->getRepository(CoreUserCartItems::class);            
            $totals = $itemsRepo->getCartTotalsByUsername($username);
                        
            return [
                'items' => $itemsRepo->getCartItemsByUsername($username, $serverUrl),
                'totals' => array_pop($totals),
                'version' => $currentRepo->getVersion($username) ?: 0,
            ];
        });        
    }
    
    /**
     * Get User Cart version (concurrency)
     * 
     * @param string $username
     * @return integer 
     */
    public function getVersion($username)
    {
        return $this
            ->createQueryBuilder('C')
            ->select('C.version')
            ->innerJoin('C.user', 'U')
            ->where('U.username = :username')
            ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
            ->getQuery()
            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR);
    }
    
    /**
     * Get User Cart (Entity)
     * 
     * @param string $username
     * @param integer $version
     * @return \Application\Entity\CoreUserCart
     */
    public function getUserCart($username, $version) 
    {
        //Find user cart
        /** @var $cart \Application\Entity\CoreUserCart **/
        $cart = $this->_em
            ->getRepository(CoreUserCart::class)
            ->createQueryBuilder('C')
            ->select('C')
            ->innerJoin('C.user','U')
            ->where('U.username = :username')
            ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
            ->getQuery()
            ->getOneOrNullResult();
        
        //If not cart entry found, create a newer one
        if (is_null($cart)) {
            $cart = $this->createCart($username);
        } else {
            //Check version (using Optimistic Lock)
            $cart = $this->_em->find(CoreUserCart::class, $cart->getId(), \Doctrine\DBAL\LockMode::OPTIMISTIC, $version);
        }
        
        return $cart;
    }
    
    /**
     * Creates a new Cart Entry
     * 
     * @param  string       $username
     * @return CoreUserCart
     */
    public function createCart($username)
    {
        //Get the user
        /** @var $cart \Application\Entity\OauthUsers **/
        $user = $this->_em
            ->getRepository(\Application\Entity\OauthUsers::class)
            ->createQueryBuilder('U')
            ->select('U')
            ->where('U.username = :username')
            ->getQuery()
            ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
            ->getSingleResult();

        /** @var $cart \Application\Entity\CoreUserCart **/
        $cart = new CoreUserCart();

        $cart->setUser($user);
        $this->_em->persist($cart);
        $this->_em->flush();
        
        return $cart;
    }
    
    /**
     * Clear User Cart (deletes all items) 
     * 
     * @param string  $username
     * @param integer $version
     * @return mixed
     * @throws \Exception
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function clearCart($username, $version)
    {
        //Set Transaction as Serializable
        $this->_em->getConnection()->setTransactionIsolation(
            \Doctrine\DBAL\Connection::TRANSACTION_SERIALIZABLE
        );
        
        $currentRepo = $this;
        
        return $this->_em->transactional(function ($em) use ($currentRepo, $username, $version){
            $cart = $currentRepo->getUserCart($username, (int)$version);
            $userId = $cart->getUser()->getId();

            //Removes all User Cart Items (by Username)
            $em->getRepository(CoreUserCartItems::class)->removeItemsByUserId($userId);
            
            //Update action on Cart entity
            //triggers version from being updated (concurrency)
            $cart->setUpdatedAt(new \DateTime());
            
            return true;
        });
    }
}