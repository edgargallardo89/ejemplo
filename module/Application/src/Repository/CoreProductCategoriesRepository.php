<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\OauthUsers;

/**
 * CoreProductCategories
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CoreProductCategoriesRepository extends EntityRepository
{    
    /**
     * PUT-related persist method
     * 
     * @param integer $id
     * @param array $data
     * @return boolean
     * @throws \Exception
     */
    public function put($id, $data)
    {
        return $this->_em->transactional(function () use($id, $data) {
            $category = $this->find($id);
            $category->setFileName($data['fileName']);
            $category->setModifiedAt(new \DateTime());

            return true;
        });
    }
    
    /**
     * Get Product Categories data
     * 
     * @param string $username Username
     * @param string $serverUrl Custom Image Url base url (cdn, internal, etc)
     * @return mixed
     */
    public function getCategories($username, $serverUrl)
    {
        /*
         * Product Categories
         * * * Notes : (None)
         * .-----------------------------------------------------.
         * .                    Steps                            |
         * .-----------------------------------------------------.
         * . 1) Get Product Categories (core_product_categories) .
         * .-----------------------------------------------------.
         */
        
        $currentRepo = $this;
        
        return $this->_em->transactional(function () use ($currentRepo, $serverUrl, $username) {
            $userRoleId = $this->_em
                ->getRepository(OauthUsers::class)
                ->createQueryBuilder('U')
                ->select('R.id')
                ->innerJoin('U.role', 'R')
                ->where('U.username = :username')
                ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getSingleScalarResult();
                        
            $categories = $currentRepo
                ->createQueryBuilder('CAT')
                ->select('partial CAT.{id,name,description,fileName,sort,createdAt,modifiedAt},COUNT(P) AS productCount')
                ->innerJoin('CAT.products', 'P')
                ->innerJoin('CAT.roles', 'R')
                ->where('CAT.enabled = 1')
                ->andWhere('R.id = :role_id')
                ->andWhere('P.enabled = 1')
                ->setParameter('role_id', $userRoleId, \Doctrine\DBAL\Types\Type::INTEGER)
                ->groupBy('CAT.id,CAT.name,CAT.description,CAT.fileName,CAT.sort,CAT.createdAt,CAT.modifiedAt')
                ->orderBy('CAT.sort', 'ASC')
                ->getQuery()
                ->getArrayResult();
            
            return array_map(
                function (&$ele) use ($serverUrl) {
                    $category = $ele[0];
                    $category['productCount'] = $ele['productCount'];                    
                    $category['fileName'] = empty($category['fileName']) ? null : $serverUrl.$category['fileName'];
                    $category['createdAt'] = ($category['createdAt'] instanceof \DateTime) ? $category['createdAt']->format('Y-m-d H:i:s') : null;
                    $category['modifiedAt'] = ($category['modifiedAt'] instanceof \DateTime) ? $category['modifiedAt']->format('Y-m-d H:i:s') : null;
                    
                    return $category;
                },
                $categories
            );
        });
    }
}
