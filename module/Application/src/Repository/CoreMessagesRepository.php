<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Application\Entity\CoreConfigs;

use Application\Entity\CoreMessages;
use Doctrine\ORM\EntityRepository;

/**
 *
 */
class CoreMessagesRepository extends EntityRepository
{
    /**
     * Fetch all config values
     *
     * @return array
     */
    public function fetchAll()
    {
        $settings = $this->_em->getRepository(CoreConfigs::class)
                ->createQueryBuilder('M')->select('M.key,M.value')->where('M.visibility = 1')->getQuery()->getArrayResult();

        $messages = $this->createQueryBuilder('M')->select('M.key,M.value')->where('M.visibility = 1')->getQuery()->getArrayResult();

        $messageItems = [];
        $settingItems = [];

        array_map(
            function ($e) use (&$messageItems) {
                $messageItems[$e['key']] = $e['value'];
            },
            $messages
        );

        array_map(
            function ($e) use (&$settingItems) {
                $settingItems[$e['key']] = $e['value'];
            },
            $settings
        );

        return array_merge(
            ['lang' => $messageItems,],
            $settingItems
        );
    }

    /**
     * Update a message string from a key
     *
     * @param $key
     * @param $data
     * @return bool|mixed
     */
    public function updateByKey($key, $data)
    {
        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($currentRepo, $key, $data) {
            if (!preg_match('/^.{1,200}$/', $key)) {
                throw new \Exception('Invalid_Key_Format');
            }

            // Get the resource to edit
            $message = $currentRepo->findOneByKey($key);

            // If it doesn't exist then create a new entry
            if ($message === null) {
                $message = new CoreMessages();
                $message->setKey($key);
                $message->setValue($data->value);
                $message->setDescription('');
                $message->setVisibility(true);

                $em->persist($message);
            } else {
                $message->setValue($data->value);
            }

            return ["message" => "success"];
        });
    }
}