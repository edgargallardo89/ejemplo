<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\CoreFileUploads;
use Application\Entity\CoreFileUploadReasons;
use ReflectionClass;

/**
 * 
 */
class CoreFileUploadReasonsRepository extends EntityRepository
{
    public function getSettings()
    {
        $refl = new ReflectionClass(CoreFileUploads::class);
        $reasons = $this->createQueryBuilder('R')
            ->select('R.id,R.reason')
            ->getQuery()
            ->getArrayResult();
        
        //Exclude TYPE_RESULTS
        $fileTypes = array_filter(
            $refl->getConstants(),
            function ($key) {
                return $key !== 'results';
            }
        );

        //Replace translations
        array_walk(
            $fileTypes,
            function (&$ele) {
                switch($ele) {
                    case 'adjustments' :
                        $ele = 'Ajustes';
                        break;
                    case 'corrections' :
                        $ele = 'Correcciones';
                        break;
                    case 'extras' :
                        $ele = 'Extras';
                        break;
                    default:
                        break;
                }
            }
        );
                
        return [
            'reasons' => array_map(
                function ($e) {
                    return [$e['id'] => $e['reason']];
                },
                $reasons
            ),
            'fileType' => $fileTypes,
        ];
    }
}