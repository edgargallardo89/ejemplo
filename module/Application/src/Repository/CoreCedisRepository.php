<?php

namespace Application\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class CoreCedisRepository extends EntityRepository
{
    public function getCedis($id)
    {
        $query = $this
            ->createQueryBuilder('C')
            ->innerJoin('C.users', 'U')
            ->where('U.id = :id')
            ->setParameter('id', $id, Type::INTEGER)
            ->setMaxResults(1);

        try {
            return $query->getQuery()->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $e) {
            return null;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}