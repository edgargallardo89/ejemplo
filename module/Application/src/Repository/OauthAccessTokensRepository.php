<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\OauthAccessTokens;

/**
 * OauthAccessTokens
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OauthAccessTokensRepository extends EntityRepository
{    
    /**
     * Expires User Tokens by deleting 'em
     * 
     * @param string $identifier
     * @return mixed
     */
    public function expireTokensByUser($identifier)
    {
        return $this
            ->createQueryBuilder()
            ->delete(OauthAccessTokens::class, 'OAT')
            ->where('OAT.user_id = :user_id')
            ->setParameter('user_id', $identifier, \Doctrine\DBAL\Types\Type::STRING)
            ->getQuery()
            ->execute();        
    }
    
    /**
     * Update Access Tokens by User
     * 
     * @param string $identifier
     * @return array
     */
    public function updateTokensByUser($oldIdentifier, $newIdentifier)
    {
        /*
         * Expire (delete) User Tokens Transaction Workflow
         * * * Notes : (None)
         * .------------------------------------------------.
         * .                    Steps                       |
         * .------------------------------------------------.
         * . 1) Update old user tokens (core_access_tokens) .
         * .------------------------------------------------.
         */    
        $qb = $this->createQueryBuilder('OAT');
        
        return $qb->update(OauthAccessTokens::class, 'OAT')
            ->set('OAT.userId', $qb->expr()->literal($newIdentifier))
            ->where('OAT.userId = :user_id')
            ->setParameter('user_id', $oldIdentifier, \Doctrine\DBAL\Types\Type::STRING)
            ->getQuery()
            ->execute();        
    }
}
