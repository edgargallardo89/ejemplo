<?php

namespace Application\Repository;

use Application\Entity\CoreGroups;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class CoreGroupsRepository extends EntityRepository
{
    /**
     * Gets one group with all its users
     *
     * @param string $id
     * @return mixed
     * @throws NoResultException|\Exception
     */
    public function fetchOne($id)
    {
        $result = $this
            ->createQueryBuilder('G')
            ->where('G.id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();

        $row['id'] = $result->getId();
        $row['name'] = $result->getName();
        $row['createdAt'] = $this->formatDate($result->getCreatedAt());
        $row['deletedAt'] = $this->formatDate($result->getDeletedAt());
        $row['users'] = array();

        foreach ($result->getUsers() as $u) {
            $user['id'] = $u->getId();
            $user['username'] = $u->getUsername();
            $user['email'] = $u->getEmail();
            $user['displayName'] = $u->getDisplayName();

            $row['users'][] = $user;
        }

        return $row;
    }

    /**
     * Searches an user group
     *
     * @param string $userId
     * @return mixed
     * @throws NoResultException|\Exception
     */
    public function fetchOneByUser($userId)
    {
        $result = $this
            ->createQueryBuilder('G')
            ->join('G.users', 'U')
            ->where('U.id = :userId')
            ->setParameter('userId', $userId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($result !== null) {
            $row['id'] = $result->getId();
            $row['name'] = $result->getName();

            return $row;
        }

        return null;
    }

    /**
     * Gets all available groups
     *
     * @return array
     * @throws \Exception
     */
    public function fetchAll()
    {
        $result = $this
            ->createQueryBuilder('G')
            ->andWhere('G.deletedAt IS NULL')
            ->getQuery()
            ->getArrayResult();

        return array_map(function ($elem) {
            $elem['createdAt'] = $this->formatDate($elem['createdAt']);
            $elem['deletedAt'] = $this->formatDate($elem['deletedAt']);

            return $elem;
        }, $result);
    }

    /**
     * Creates a new group
     *
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function create($data)
    {
        $now = new \DateTime();

        // Name must not exists
        if ($this->exists($data->name)) {
            throw new \Exception('Group_Name_Exists');
        }

        // Instance
        $group = new CoreGroups();
        $group->setName($data->name);
        $group->setCreatedAt($now);

        $this->_em->persist($group);
        $this->_em->flush();

        return ["message" => "success"];
    }

    /**
     * Updates a group
     *
     * @param mixed $id
     * @param mixed $data
     * @return mixed
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $now = new \DateTime();

        // Search group
        $group = $this->findOneBy(['id' => $id]);

        if ($group === null) {
            throw new NoResultException();
        }

        // Update instance
        $group->setName($data->name);

        $this->_em->persist($group);
        $this->_em->flush();

        return ["message" => "success"];
    }

    /**
     * Deletes a group
     *
     * @param mixed $id
     * @return mixed
     * @throws \Exception
     */
    public function delete($id)
    {
        $now = new \DateTime();

        // Search group
        $group = $this->findOneBy(['id' => $id]);

        if ($group === null) {
            throw new NoResultException();
        }

        // Check if deleted
        if ($group->getDeletedAt() !== null) {
            throw new \Exception('Group_Already_Deleted');
        }

        // Update instance
        $group->setDeletedAt($now);

        $this->_em->persist($group);
        $this->_em->flush();

        return true;
    }

    /**
     * Validate if a group name exists within active groups only
     *
     * @param string $name
     * @return bool
     */
    private function exists($name)
    {
        $query = $this->createQueryBuilder('G');

        $group = $query->where('G.name = :name')
            ->andWhere($query->expr()->isNull('G.deletedAt'))
            ->setParameter('name', $name, Type::STRING)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        return $group !== null;
    }

    /**
     * @param \DateTime $dateObj
     * @return string|null
     */
    private function formatDate($dateObj)
    {
        if ($dateObj != null && $dateObj instanceof \DateTime) {
            return $dateObj->format('Y-m-d H:i:s');
        }

        return null;
    }
}