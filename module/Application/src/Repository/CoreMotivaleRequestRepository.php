<?php
namespace Application\Repository;

use Application\Entity\CoreMotivaleRequest;
use Application\Entity\OauthUsers;
use Doctrine\ORM\EntityRepository;
use Application\Entity\CoreMotivale;

class CoreMotivaleRequestRepository extends EntityRepository
{
    /**
     * 
     * @param type $data
     * @return type
     */
    public function create($data,$identity)
    {   
        if($this->hasNewPublish())
        {
            $this->_em->transactional(function ($em) use($identity){ 
                $coreMotivaleRequest = new CoreMotivaleRequest();
                $randomname = bin2hex(random_bytes(4)).'.json';     
                $userIdoauth = $em->getRepository(OauthUsers::class)
                        ->getUser($identity['user_id']);
                $userId = $em->getReference(OauthUsers::class, $userIdoauth);            
                $coreMotivaleRequest
                        ->setUser($userId)
                        ->setFilename($randomname)
                        ->setAction('new');            
                $em->persist($coreMotivaleRequest);
            });              
        }   
        return $this->getCurrentTask('process');
    }
    
    /**
     * verifica si hay publicacion
     * 
     * @return type
     */
    private function hasNewPublish(){
        $isNewPublish = false;
        $task = $this->findOneBy(array('action' => array('new','process'))); 
        if(is_null($task)){
            $isNewPublish = true;
        }
        elseif($task->getAction()==='new'||$task->getAction()==='process')
        {
            $isNewPublish = false;
        }    
        return $isNewPublish;
    }
    
    /**
     * 
     * @return type
     */
    private function getCurrentTask($action='new')
    {
        $task = $this->findOneBy(array('action' => array('new','process'))); 
        if(is_null($task)){
            return false;
        }
        return [
            'id'=>$task->getId(),
            'status'=>$action
        ];
                
    }
    
    /**
     * 
     * @param type $id
     * @return boolean
     */
    public function fetch($id){
        $task = $this->findOneBy(array('id' => $id)); 
        if(is_null($task)){
            return false;
        }
        
        if($task->getAction()==='error'){
            return 'error';
        }
        
        $estatus = $this->_em->transactional(function ($em){ 
                   $new = $em->getRepository(CoreMotivale::class)
                           ->count('new');
                   $process = $em->getRepository(CoreMotivale::class)
                           ->count('process');
                   return [
                       'new'=>  is_bool($new)?0:$new,
                       'process'=>is_bool($process)?0:$process,
                   ];                   
        });

        return [
            'id'=>$task->getId(),
            'status'=>$task->getAction()==="process"?"new":$task->getAction(),
            'count'=>$estatus
        ];        
    }

}