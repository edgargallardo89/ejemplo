<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class CoreRolesRepository extends EntityRepository{
    
    public function fetchAll(){
        return $this
                ->createQueryBuilder('U')->select('U.id, U.role')->getQuery()->getArrayResult();
    }
}