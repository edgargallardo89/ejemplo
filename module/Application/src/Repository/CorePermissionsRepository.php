<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class CorePermissionsRepository extends EntityRepository{
    
    public function fetchAll(){
        return $this
                ->createQueryBuilder('P')->select('P.id, R.id as roleId, RE.id as resourceId, P.permission, RE.methodhttp, RE.resource')
                ->innerJoin('P.role', 'R')
                ->innerJoin('P.resource', 'RE')
                ->getQuery()->getArrayResult();
    }
}
