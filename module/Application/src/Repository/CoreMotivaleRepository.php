<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class CoreMotivaleRepository extends EntityRepository
{
    /**
     * 
     * @param type $estatus
     * @return type
     */
    public function count($estatus){
        $currentRepo = $this;        
        return $this->_em->transactional(function () use($currentRepo,$estatus) {
            $motivale = $currentRepo
                ->createQueryBuilder('U')
                ->select(['COUNT(U) AS cont'])    
                ->where('U.estatus = :estatus')
                ->setParameter('estatus', $estatus, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
            return $motivale['cont'];
        });        
    }
}