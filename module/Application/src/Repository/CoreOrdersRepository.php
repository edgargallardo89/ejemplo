<?php

namespace Application\Repository;

use Adteam\Core\Checkout\Entity\CoreOrderAddressses;
use Adteam\Core\Checkout\Entity\CoreOrderCedis;
use Application\Entity\CoreCedis;
use Application\Entity\CoreOrderProducts;
use Application\Entity\CoreOrders;
use Application\Entity\CoreProducts;
use Application\Entity\OauthUsers;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class CoreOrdersRepository
 * @package Application\Repository
 */
class CoreOrdersRepository extends EntityRepository
{
    /**
     * Fetch all orders with product count
     *
     * @param string $username User identity
     * @return array
     */
    public function fetchAllByUser($username)
    {
        // Get user info
        $oauthUsersRepo = $this->_em->getRepository(OauthUsers::class);
        $oauthUser = $oauthUsersRepo->findOneBy(['username' => $username]);

        // Get all orders by user
        $results = $this->createQueryBuilder('O')
            ->select('partial O.{id, total, revision, deletedAt, createdAt}, SUM(OP.quantity) AS productCount')
            ->innerJoin('Application\Entity\CoreOrderProducts', 'OP', Join::WITH, 'O.id = OP.order')
            ->where('O.user = :oauthUser')
            ->setParameter('oauthUser', $oauthUser)
            ->groupBy('O.id')
            ->getQuery()
            ->getArrayResult();

        // Arrange data
        $orders = [];

        foreach ($results as $row) {
            $item = $row[0];

            $item['createdAt'] = isset($item['createdAt']) ? $item['createdAt']->format('Y-m-d H:i:s') : null;
            $item['deletedAt'] = isset($item['deletedAt']) ? $item['deletedAt']->format('Y-m-d H:i:s') : null;
            $item['productCount'] = $row['productCount'];

            $orders[] = $item;
        }

        return $orders;
    }

    /**
     * Fetch a single order
     *
     * @param string $username User identity
     * @param string $orderId Order identifier
     * @param string $url Site url
     * @return array
     */
    public function fetchOneByUser($username, $orderId, $url)
    {
        // Get user info
        $oauthUsersRepo = $this->_em->getRepository(OauthUsers::class);
        $oauthUser = $oauthUsersRepo->findOneBy(['username' => $username]);

        // Get order
        $result = $this->createQueryBuilder('O')
            ->select('partial O.{id, total, revision, deletedAt, createdAt} AS orderData, C.id AS createdById')
            ->leftJoin('O.createdBy', 'C')
            ->andWhere('O.user = :oauthUser')
            ->andWhere('O.id = :orderId')
            ->setParameter('oauthUser', $oauthUser)
            ->setParameter('orderId', $orderId)
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);

        // Arrange data
        $item = $result['orderData'];
        $item['address'] = $this->getAdress($orderId);
        $item['cedis'] = $this->getCedis($orderId);
        $item['items'] = $this->getProducts($orderId, $url);
        $item['createdAt'] = isset($item['createdAt']) ? $item['createdAt']->format('Y-m-d H:i:s') : null;
        $item['deletedAt'] = isset($item['deletedAt']) ? $item['deletedAt']->format('Y-m-d H:i:s') : null;

        return $item;
    }

    /**
     * @param string $orderId
     * @return array
     */
    public function getAdress($orderId)
    {
        $order = $this->_em->getReference(CoreOrderAddressses::class, $orderId);
        $result = $this->_em->getRepository(CoreOrderAddressses::class)->findOneBy(array('order' => $order));

        if (is_null($result)) {
            return null;
        }

        return [
            'street' => $result->getStreet(),
            'extNumber' => $result->getExtNumber(),
            'intNumber' => $result->getIntNumber(),
            'zipCode' => $result->getZipCode(),
            'reference' => $result->getReference(),
            'location' => $result->getLocation(),
            'city' => $result->getCity(),
            'town' => $result->getTown(),
            'state' => $result->getState()
        ];
    }

    /**
     * @param string $orderId
     * @return array
     */
    public function getCedis($orderId)
    {
        $order = $this->_em->getReference(CoreOrderCedis::class, $orderId);
        $result = $this->_em->getRepository(CoreOrderCedis::class)->findOneBy(array('order' => $order));

        if (is_null($result)) {
            return null;
        }

        $cedis = $this->_em->getRepository(CoreCedis::class)->findOneBy(array('id' => $result->getCedis()->getId()));

        return [
            'cedisId' => $cedis->getCedisId(),
            'name' => $cedis->getNamesCedis(),
            'street' => $cedis->getStreet(),
            'extNumber' => $cedis->getExtNumber(),
            'intNumber' => $cedis->getIntNumber(),
            'location' => $cedis->getLocation(),
            'reference' => $cedis->getReference(),
            'city' => $cedis->getCity(),
            'state' => $cedis->getState(),
            'zipCode' => $cedis->getZipCode(),
            'telephone' => $cedis->getTelephone(),
            'extra' => $cedis->getExtra()
        ];
    }

    /**
     * @param string $orderId
     * @param $url
     * @return array
     */
    public function getProducts($orderId, $url)
    {
        $entities = [];

        $order = $this->_em->getReference(CoreOrders::class, $orderId);
        $result = $this->_em->getRepository(CoreOrderProducts::class)->findBy(['order' => $order]);

        foreach ($result as $item) {
            $entities[] = [
                'productId' => $item->getProduct()->getId(),
                'sku' => $item->getSku(),
                'description' => $item->getDescription(),
                'brand' => $item->getBrand(),
                'fileName' => $url . $this->getImage($item->getProduct()->getId()),
                'title' => $item->getTitle(),
                'price' => $item->getPrice(),
                'quantity' => $item->getQuantity(),
                'lineTotal' => $item->getPrice() * $item->getQuantity()
            ];
        }

        return $entities;
    }

    /**
     * @param string $productId
     * @return string
     */
    public function getImage($productId)
    {
        $result = $this->_em->getRepository(CoreProducts::class)->findOneBy(array('id' => $productId));

        if (!is_null($result)) {
            return $result->getFileName();
        }

        return '';
    }

    /**
     * Gets the report data that contains every available information
     *
     * @return array
     */
    public function getReportData()
    {
        return $this
            ->createQueryBuilder('T')
            ->select('T.id as orderId, T.deletedAt, T.createdAt, U.telephone1, U.telephone2, U.mobile,
                U.id AS userId, U.username, U.firstName, U.lastName, U.surname, U.email, U.displayName,
                U.enabled AS status, U.profileFulfilled, R.role AS roleName, PC.name AS productCategory,
                CC.cedisId AS cedisId, CC.namesCedis as cedisName, C.sku, C.title, C.quantity, C.price, C.realPrice,
                A.street, A.extNumber, A.intNumber, A.location, A.reference, A.city, A.state, A.zipCode')
            ->innerJoin('T.user', 'U')
            ->innerJoin('U.role', 'R')
            ->innerJoin(CoreOrderProducts::class, 'C', Join::WITH, 'C.order = T.id')
            ->innerJoin(CoreProducts::class, 'P', Join::WITH, 'P.id = C.product')
            ->innerJoin('P.categories', 'PC')
            ->leftJoin(CoreOrderAddressses::class, 'A', Join::WITH, 'A.order = T.id')
            ->leftJoin(CoreOrderCedis::class, 'E', Join::WITH, 'E.order = T.id')
            ->leftJoin(CoreCedis::class, 'CC', Join::WITH, 'CC.id = E.cedis')
            ->orderBy('T.createdAt', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Gets total orders information
     * @param float $factor Pesos multiplier
     * @return array
     */
    public function getTotals($factor)
    {
        $results = $this
            ->createQueryBuilder('O')
            ->select('COUNT(O.id) AS total_orders')
            ->addSelect('SUM(O.total * :factor) AS total_pesos')
            ->addSelect('SUM(O.total) AS total_points')
            ->setParameter(':factor', $factor)
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);

        return array_map(function ($value) {
            return $value === null ? 0 : $value;
        }, $results);
    }

    /**
     * Gets total orders by month
     * @return array
     */
    public function getTotalsByMonth()
    {
        $emConfig = $this->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('MONTH', 'Application\Doctrine\Month');
        $emConfig->addCustomDatetimeFunction('YEAR', 'Application\Doctrine\Year'); 
        
        $results = $this
            ->createQueryBuilder('O')
            ->select('YEAR(O.createdAt) AS created_year')
            ->addSelect('MONTH(O.createdAt) AS created_month')
            ->addSelect('COUNT(O.id) AS total')
            ->groupBy('created_year')
            ->addGroupBy('created_month')
            ->orderBy('O.createdAt', 'ASC')
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        return array_map(function ($value) {
            $value = 0;
            if(isset($value['created_month'])){
                $value['created_month'] = $this->getMonthName(intval($value['created_month']));
            }
            return $value;
        }, $results);
    }

    /**
     * Translates month's name
     * @param $index
     * @return mixed
     */
    private function getMonthName($index)
    {
        $months = [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        ];

        return $months[--$index];
    }
}