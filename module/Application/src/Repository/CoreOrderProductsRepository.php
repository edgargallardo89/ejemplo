<?php

namespace Application\Repository;

use Application\Entity\CoreProductCategories;
use Application\Entity\CoreProducts;
use Application\Entity\CoreProductsXCategories;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class CoreOrderProductsRepository
 * @package Application\Repository
 */
class CoreOrderProductsRepository extends EntityRepository
{
    /**
     * Gets total orders grouped by category
     * @return array
     */
    public function getTotalsByCategory()
    {
        return $this
            ->createQueryBuilder('OP')
            ->select('PC.id AS category_id')
            ->addSelect('PC.name AS category_name')
            ->addSelect('SUM(OP.quantity) AS total')
            ->innerJoin(CoreProductsXCategories::class, 'PXC', Join::WITH, 'OP.product = PXC.product')
            ->innerJoin(CoreProductCategories::class, 'PC', Join::WITH, 'PXC.category = PC.id')
            ->groupBy('PC.id')
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * Gets the top selling products
     * @param int $limit Max results
     * @return array
     */
    public function getTopSelling($limit)
    {
        return $this
            ->createQueryBuilder('OP')
            ->select('P.id AS product_id, P.title AS product_name')
            ->addSelect('SUM(OP.quantity) AS total')
            ->innerJoin(CoreProducts::class, 'P', Join::WITH, 'OP.product = P.id')
            ->groupBy('P.id')
            ->orderBy('total', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}