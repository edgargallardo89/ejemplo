<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\CoreUserCartItems;
use Application\Entity\CoreProducts;
use Application\Entity\OauthUsers;
use Application\Entity\CoreUserCart;

/**
 * 
 */
class CoreUserCartItemsRepository extends EntityRepository
{
    /**
     * Sets an User Cart Item (existing or not)
     * using Optimistick Locking strategy
     * 
     * @param string  $username  Username (Id)
     * @param integer $productId Product Id
     * @param array   $data      Madatory values on the array: [version,quantity]
     * @return boolean
     */
    public function setCartItem($username, $productId, $data)
    {
        //Set Transaction as Serializable
        $this->_em->getConnection()->setTransactionIsolation(
            \Doctrine\DBAL\Connection::TRANSACTION_SERIALIZABLE
        );
        
        $currentRepo = $this;
        
        return $this->_em->transactional(
            function ($em) use ($currentRepo, $username, $productId, $data) {
                $cart = $em
                    ->getRepository(CoreUserCart::class)
                    ->getUserCart($username, $data["version"]);
                
                $itemExist = $currentRepo->getCartItem($username, $productId);
                
                /** @var $item \Application\Entity\CoreUserCartItems **/
                $item = is_null($itemExist) ? new CoreUserCartItems() : $itemExist;

                /** @var $product \Application\Entity\CoreProducts **/
                $product = $em->getRepository(CoreProducts::class)->getUserProduct($productId, $username);

                if (null === $itemExist) {
                    $user = $em->getRepository(OauthUsers::class)
                        ->createQueryBuilder('U')
                        ->where('U.username = :username')
                        ->andWhere('U.enabled = :isEnabled')
                        ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
                        ->setParameter('isEnabled', 1, \Doctrine\DBAL\Types\Type::BOOLEAN)
                        ->getQuery()
                        ->getSingleResult();
                }

                //Sets the user if a new item was created
                if (isset($user)) {
                    $item->setUser($user);
                }
                
                //Update action on Cart entity
                //triggers version from being updated (concurrency)
                $cart->setUpdatedAt(new \DateTime());
                
                $item->setProduct($product);
                $item->setQuantity($data["quantity"]);
                                
                $em->persist($item);
                                
                return true;
            }
        );
    }
    
    /**
     * Removes a User Cart Item
     * using Optimistic Locking Strategy
     * 
     * @param string  $username
     * @param integer $productId
     * @param integer $version
     * @return boolean
     * @throws \Exception
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function removeCartItem($username, $productId, $version)
    {        
        //Set Transaction as Serializable
        $this->_em->getConnection()->setTransactionIsolation(
            \Doctrine\DBAL\Connection::TRANSACTION_SERIALIZABLE
        );
        
        $currentRepo = $this;
        
        return $this->_em->transactional(
            function ($em) use ($currentRepo, $username, $productId, $version) {
                $cart = $em->getRepository(CoreUserCart::class)
                    ->getUserCart($username, $version);

                /** @var $item \Application\Entity\CoreUserCartItems **/
                $item = $currentRepo->getCartItem($username, $productId);
                
                if (is_null($item)) {
                    throw new \Exception('Invalid_Cart_Item');
                }

                //Removes user cart item
                $em->remove($item);
                
                //Update action on Cart entity
                //triggers version from being updated (concurrency)
                $cart->setUpdatedAt(new \DateTime());
                                
                return true;
            }
        );
    }
    
    /**
     * Removes all Items by User Id (Collection clear)
     * 
     * @param integer $id
     * @return mixed
     */
    public function removeItemsByUserId($id)
    {
        //Removes all User Cart Items (by Username)
        return $this->createQueryBuilder('I')
            ->delete(CoreUserCartItems::class, 'I')
            ->where('I.user = :userId')
            ->setParameter('userId', $id, \Doctrine\DBAL\Types\Type::INTEGER)
            ->getQuery()
            ->execute();
    }
    
    /**
     * Get User Cart products (by username)
     * 
     * @param string $username
     * @return array
     */
    public function getCartItemsByUsername($username, $serverUrl)
    {
        $dql = 'SELECT ';
        $dql.= 'partial C.{id,quantity,dateAdded}, partial P.{id,sku,title,description,brand,fileName,price}, partial CAT.{id,name} ';
        $dql.= 'FROM '.CoreUserCartItems::class.' C ';
        $dql.= 'INNER JOIN C.product P ';
        $dql.= 'INNER JOIN P.categories CAT ';
        $dql.= 'INNER JOIN '.OauthUsers::class.' U WITH U.id = C.user ';
        $dql.= 'WHERE U.username = :username ';
        $dql.= 'ORDER BY C.dateAdded DESC ';

        $products = $this->_em->createQuery($dql)
            ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
            ->getArrayResult();

        foreach ($products as &$item) {
            //Removing additional mandatory doctrine data
            unset($item['id']);

            // Format date
            $item['dateAdded'] = $item['dateAdded']->format('Y-m-d H:i:s');

            //Calcs lineTotal
            $item['lineTotal'] = ($item['product']['price'] * $item['quantity']);

            //Assigns image path to product image file
            $item['product']['fileName'] = !empty($item['product']['fileName']) ? $serverUrl . $item['product']['fileName'] : null;
        }

        return $products;
    }
    
    /**
     * Get User Cart totals (by username)
     * 
     * @param string $username
     * @return array
     */
    public function getCartTotalsByUsername($username)
    {                
        $dql = 'SELECT ';
        $dql.= 'SUM(C.quantity) as totalQty,SUM(C.quantity*P.price) as lineTotal ';
        $dql.= 'FROM '.CoreUserCartItems::class.' C ';
        $dql.= 'INNER JOIN '.OauthUsers::class.' U WITH U.id = C.user ';
        $dql.= 'INNER JOIN '.CoreProducts::class.' P WITH P.id = C.product ';
        $dql.= 'WHERE U.username = :username ';
        //$dql.= 'C.quantity,P.price,SUM(C.quantity) as quantityT,SUM(C.quantity*P.price) as lineTotal ';        
        //$dql.= '(SELECT SUM(C2.quantity) FROM '.CoreUserCart::class.' C2 WHERE C2.user = C.user) as lineTotal ';
        
        return $this->_em->createQuery($dql)
            ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
            ->getScalarResult();
    }
    
    /**
     * Gets a Cart Item by Username and Product Id
     * 
     * @param string $username
     * @param integer $productId
     * @return Object
     * @throws Exception|NonUniqueResultException If more than one record is matched
     */
    public function getCartItem($username, $productId)
    {
        return $this->createQueryBuilder('I')
            ->select('I')
            ->innerJoin('I.user', 'U')
            ->innerJoin('I.product', 'P')
            ->where('U.username = :username')
            ->andWhere('P.id = :productId')
            ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
            ->setParameter('productId', $productId, \Doctrine\DBAL\Types\Type::INTEGER)
            ->getQuery()
            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_OBJECT);
    }
    
    public function truncate()
    {
        $sql ="TRUNCATE TABLE core_user_cart_items";
        $this->_em->getConnection()->query($sql);     
    }
}