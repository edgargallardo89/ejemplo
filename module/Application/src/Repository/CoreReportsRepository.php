<?php

namespace Application\Repository;

use Application\Entity\CoreReports;
use Application\Import\ImportFactory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Zend\Http\Response\Stream;

class CoreReportsRepository extends EntityRepository
{
    /**
     * Upload route
     */
    const UPLOAD_ROUTE = '/data/upload/reports/';

    /**
     * Application configuration array
     *
     * @var array
     */
    private $config;

    /**
     * Import service class
     *
     * @var ImportFactory
     */
    private $importFactory;

    /**
     * Users repository class
     *
     * @var OauthUsersRepository
     */
    private $oauthUsersRepository;

    /**
     * Sets the configuration array
     *
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
        if (isset($this->config['reportsCcp'])) {
            $this->config['reports'] = $this->config['reportsCcp'];
        }
    }

    /**
     * Sets the import service class
     *
     * @param ImportFactory $importFactory
     */
    public function setImportFactory($importFactory)
    {
        $this->importFactory = $importFactory;
    }

    /**
     * Sets the users repository class
     *
     * @param OauthUsersRepository $oauthUsersRepository
     */
    public function setOauthUsersRepository($oauthUsersRepository)
    {
        $this->oauthUsersRepository = $oauthUsersRepository;
    }

    /**
     * Process and download a csv report based on saved configuration
     *
     * @param mixed $id
     * @return Stream
     * @throws NoResultException|\Exception
     */
    public function downloadCsv($id)
    {
        // Validate configuration
        if ($this->config === null) {
            throw new \Exception('Configuration_Not_Set');
        }

        // Validate if configuration is not present
        if (!isset($this->config['reports'])) {
            throw new \Exception('Missing_Report_Configuration');
        }

        // Validate import service
        if ($this->importFactory === null) {
            throw new \Exception('ImportFactory_Not_Set');
        }

        // Get report
        $report = $this->find(['id' => $id]);

        if ($report === null) {
            throw new NoResultException();
        }

        // Document must be ready for download
        if ($report->getModifiedAt() === null) {
            throw new \Exception('Report_Not_Ready');
        }

        // File validation
        $source = $report->getSource();

        $csv['tmp_name'] = $this->config['path'] . $source;

        // File must exists physically
        if (!file_exists($csv['tmp_name'])) {
            throw new \Exception('File_Does_Not_Exist');
        }

        $data = $this->importFactory->importCsv($csv);

        // Column validation
        $columns = $report->getColumns();

        $arrColumns = json_decode($columns);

        // Proper column json data
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Malformed_Columns_Information');
        }

        // Fill data with the requested ordered columns
        $items = array();

        // Get report configuration
         $reportsList = $this->config['reports'];

        if (!isset($reportsList[$report->getName()])) {
            throw new \Exception('Invalid_Report_Type');
        }

        $config = $reportsList[$report->getName()];

        // Get headers configuration
        $reportColumns = $config['columns'];

        // Title columns
        $titles = array();

        foreach ($arrColumns as $col) {
            $colData = null;

            foreach ($reportColumns as $row) {
                if ($row['column_name'] == $col) {
                    $colData = $row['display_name'];
                }
            }

            if (!is_null($colData)) {
                $titles[] = $colData;
            } else {
                $titles[] = 'N/D';
            }
        }

        $items[] = $titles;

        // Body columns
        $data->rewind();

        foreach ($data as $fields) {
            $colData = array();

            // Put the requested columns in order
            foreach ($arrColumns as $col) {
                // All requested columns must exist in the file
                if (!isset($fields[$col])) {
                    throw new \Exception('Invalid_Column_Request');
                }

                $colData[] = $fields[$col];
            }

            $items[] = $colData;
        }

        // Report name (lowercase and snake case)
        $reportName = $config['name'] . ($report->getCustomized() ? ' personalizado' : '');
        $fileName = str_replace(' ', '_', mb_strtolower($reportName));

        // Headers
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Expose-Headers: Content-Disposition');
        header('Content-type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $fileName . '.csv"');
        echo "\xEF\xBB\xBF";

        // Export file
        $fp = fopen('php://output', "w+");
        fprintf($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));

        foreach ($items as $item) {
            fputcsv($fp, $item);
        }

        $response = new Stream();
        $response->setStream($this->getDummyStream());
        $response->setStatusCode(200);

        return $response;
    }

    /**
     * Retrieves the configuration from all available reports
     *
     * @return array
     * @throws \Exception
     */
    public function getConfiguration()
    {
        // Validate configuration
        if ($this->config === null) {
            throw new \Exception('Configuration_Not_Set');
        }

        // Validate if configuration is not present
        if (!isset($this->config['reports'])) {
            throw new \Exception('Missing_Configuration');
        }

        // Remove unnecessary fields
        $reportsList = array_map(function ($row) {
            unset($row['class']);
            unset($row['ttl']);
            return $row;
        }, $this->config['reports']);

        // Accommodate configurations as array
        $reportsConfig = array();

        foreach ($reportsList as $row) {
            $reportsConfig[] = $row;
        }

        return $reportsConfig;
    }

    /**
     * Gets the status from reports generated by the user
     *
     * @param string $username
     * @return mixed
     * @throws \Exception
     */
    public function getStatuses($username)
    {
        // Validate configuration
        if ($this->config === null) {
            throw new \Exception('Configuration_Not_Set');
        }

        // Validate users repository
        if ($this->oauthUsersRepository === null) {
            throw new \Exception('OauthUsers_Not_Set');
        }

        $oauthUser = $this->oauthUsersRepository->findOneBy(['username' => $username]);
        $reports = $oauthUser->getReports();

        $reportsArray = array();

        foreach ($reports as $row) {
            // Relevant data
            $report['id'] = $row->getId();
            $report['name'] = $row->getName();
            $report['modifiedAt'] = $row->getModifiedAt() !== null ? $row->getModifiedAt()->format('Y-m-d H:i:s') : null;
            $report['customized'] = $row->getCustomized();

            // File modification time
            $file = $this->config['path'] . $row->getSource();
            $fileModifiedAt = @filemtime($file);

            // If there's no error then set the report status
            if ($fileModifiedAt !== false) {
                $modifiedAt = $row->getModifiedAt() !== null ? $row->getModifiedAt()->getTimestamp() : null;

                if ($modifiedAt === null) {
                    $report['status'] = CoreReports::STATUS_PROCESSING;
                } else {
                    // The document has expired
                    if ($fileModifiedAt > $modifiedAt) {
                        $report['status'] = CoreReports::STATUS_EXPIRED;
                    } else {
                        $report['status'] = CoreReports::STATUS_READY;
                    }
                }
            } else {
                $report['status'] = CoreReports::STATUS_EXPIRED;
            }

            // Exclude expired reports
            if ($report['status'] !== CoreReports::STATUS_EXPIRED) {
                $reportsArray[] = $report;
            }
        }

        return $reportsArray;
    }

    /**
     * Creates a report
     *
     * @param string $username
     * @param mixed $data
     * @return mixed
     * @throws \Exception
     */
    public function generate($username, $data)
    {
        // Validate configuration
        if ($this->config === null) {
            throw new \Exception('Configuration_Not_Set');
        }

        // Validate if configuration is not present
        if (!isset($this->config['reports'])) {
            throw new \Exception('Missing_Configuration');
        }

        // Validate users repository
        if ($this->oauthUsersRepository === null) {
            throw new \Exception('OauthUsers_Not_Set');
        }

        $reportsList = $this->config['reports'];

        // Validate request
        if (!isset($reportsList[$data->id])) {
            throw new \Exception('Invalid_Report_Request');
        }

        $config = $reportsList[$data->id];

        $reportLayout = $config['layout'];

        // Is custom report
        $isCustomLayout = $reportLayout != $data->layout;

        if (count($data->id) == 0) {
            throw new \Exception('No_Selected_Columns');
        }

        // Serialize column request
        $serializedColumns = json_encode($data->layout);

        // Proper column data
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Malformed_Columns_Request');
        }

        // Report full route
        $destination = $this::UPLOAD_ROUTE . $data->id . '.csv';

        // Date
        $now = new \DateTime();

        // Last generated report
        $coreReports = $this->getLastByName($username, $data->id);

        // User reference
        $oauthUser = $this->oauthUsersRepository->findOneBy(['username' => $username]);

        // Server response
        $response = array();

        // Check report expiration
        $this->validateExpiredReport($config['id']);

        // No report available
        if ($coreReports === null || json_decode($coreReports->getColumns()) != $data->layout) {
            if ($coreReports === null) {
                $response['status'] = 'NEW';
            } else {
                $response['status'] = 'NEW_LAYOUT';
            }

            $coreReports = new CoreReports();

            $coreReports->setName($data->id);
            $coreReports->setSource($destination);
            $coreReports->setColumns($serializedColumns);
            $coreReports->setCustomized($isCustomLayout);
            $coreReports->setCreatedAt($now);
            $coreReports->setModifiedAt($now);
            $coreReports->addUser($oauthUser);

            $this->_em->persist($coreReports);
            $this->_em->flush();
        } else {
            // Report may be processing
            if ($coreReports->getModifiedAt() === null) {
                throw new \Exception('Report_Already_Processing');
            }

            $response['status'] = 'EXISTING';
        }

        $response['id'] = $coreReports->getId();

        return $response;
    }

    /**
     * Gets last user generated report by name
     *
     * @param string $username
     * @param string $name
     * @return CoreReports
     */
    private function getLastByName($username, $name)
    {
        return $this
            ->createQueryBuilder('R')
            ->join('R.users', 'U')
            ->andWhere('R.name = :name')
            ->andwhere('U.username = :username')
            ->setParameter('name', $name)
            ->setParameter('username', $username)
            ->orderBy('R.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Runs the procedure to validate the report expiration period
     *
     * @param string $reportName
     */
    private function validateExpiredReport($reportName)
    {
        $command = "php public/index.php reports create $reportName > /dev/null &";
        $output = shell_exec($command);
    }

    /**
     * Creates a dummy stream
     * @return resource
     */
    private function getDummyStream()
    {
        $stream = fopen('php://memory', 'r+');
        fwrite($stream, '');
        rewind($stream);

        return $stream;
    }
}