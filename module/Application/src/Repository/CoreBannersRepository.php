<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\CoreBanners;

/**
 * 
 */
class CoreBannersRepository extends EntityRepository
{
    /**
     * Creates a new Banner
     * 
     * @param array $data
     * @return boolean
     * @throws \Exception
     */
    public function create($data)
    {
        return $this->_em->transactional(function ($em) use($data) {
            $newBanner = new CoreBanners();
        
            foreach($data as $field => $value) {
                $newBanner->{'set'.ucfirst($field)}($value);
            }
            
            $em->persist($newBanner);
            
            return true;
        });
    }
    
    /**
     * Updates an existing banner
     * 
     * @param string $id
     * @param array $data
     * @return boolean
     * @throws \Exception
     */
    public function put($id, $data)
    {
        $currentRepo = $this;
        
        return $this->_em->transactional(function ($em) use($id, $data, $currentRepo) {            
            $banner = $currentRepo->find($id);
            
            if (!is_object($banner)) {
                $banner = new CoreBanners();
            }
            
            foreach($data as $field => $value) {
                //If no file send, do not update the file_name
                if ('fileName' === $field and null === $value) {
                    continue;
                }
                
                $banner->{'set'.ucfirst($field)}($value);
            }
            
            $em->persist($banner);
            
            return true;
        });        
    }
    
    /**
     * Fetch all config values
     * 
     * @return array
     */
    public function fetchAll($serverUrl)
    {
        $banners = $this->createQueryBuilder('B')->select('B')->getQuery()->getArrayResult();
        
        array_map(
            function (&$ele) use ($serverUrl) {
                $ele['fileName'] = empty($ele['fileName']) ? null : $serverUrl.$ele['fileName'];
            },
            $banners
        );
            
        return $banners;
    }
    
    /**
     * PATCH-related method (resets banner file_name {image})
     * 
     * @param string $id
     * @return boolean
     * @throws \Exception
     */
    public function patch($id)
    {
        $currentRepo = $this;
        
        return $this->_em->transactional(function () use ($currentRepo, $id){
            $banner = $currentRepo
                ->createQueryBuilder('B')
                ->where('B.id = :id')
                ->setParameter('id', $id, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getSingleResult();
            
            $banner->setFileName(null);
            
            return true;
        });
    }
}