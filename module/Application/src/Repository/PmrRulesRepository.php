<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

/**
 * Description of PmrRulesRepository
 *
 * @author dev
 */

use Doctrine\ORM\EntityRepository;
use Application\Entity\PmrRules;
use Application\Entity\OauthUsers;
use Application\Entity\CoreUserTransactions;

class PmrRulesRepository  extends EntityRepository
{
    public $errors= [];
    
    /**
     * 
     * @param type $data
     */
    public function create($key,$data)
    {

        try{
            $username = isset($data['user_id'])?$data['user_id']:'';
            $data['user_id'] = $username;            
            $data['user'] = $this->getUserByUsername($data['user_id']);
            $data['username'] =$username;
            $data['user_id'] = $data['user']->getId();

            $this->validate($data);
            $items = $this->exeptionColumns($data);
            $rules = new PmrRules();
 
            foreach($items as $field => $value) {
                if (method_exists($rules, 'set'.ucfirst($field))) {
                    $rules->{'set'.ucfirst($field)}($value);
                }    
            }
            $this->_em->persist($rules);          
            $this->_em->flush();
            $this->setTransactions($data['user'], $items['puntos'],
                    $rules->getId(),$items['anio'],$items['mes']);
        } catch (\Exception $ex) {
            $this->errors[]='Fila '.($key).': '.$ex->getMessage().PHP_EOL;
        }
    }
    
    public function fetchAll()
    {
        $result = $this->_em->getRepository(PmrRules::class)
            ->createQueryBuilder('T')
            ->select('T')
            ->getQuery()
            ->getArrayResult();
        return $result;
    }
    
    /**
     * 
     * @param type $data
     */
    public function validate($data){
        $this->hasColumns($data);
        $this->isNumeric($data);
        $this->hasUniqueKey($data);
        $this->hasRegex(
                '/^(19|20)\d\d$/', 
                $data, 'anio',
                'Formato año debe ser a 4 digitos');
        $this->hasRegex(
                '/^([1-9]|1[012])$/', 
                $data, 'mes',
                'Formato mes debe de 1 digito');        
    }

        /**
     * 
     * @param type $data
     * @return type
     */
    public function exeptionColumns($data)
    {
        $exeption = ['id','created_at','user_id','username'];
        foreach ($exeption as $key){
            unset($data[$key]);
        }
        return $data;
    }
    
    /**
     * 
     * @param type $username
     * @return type
     * @throws \InvalidArgumentException
     */
    public function getUser($username)
    {
        try{
            return $this->_em->getRepository(OauthUsers::class)
                    ->findBy(['username'=>$username,'enabled'=>1]);  
        } catch (\Exception $ex) {
            throw new \InvalidArgumentException(
                    ' el usuario '.$username.' no existe o esta deshabilitado');
        }
    }

    /**
     * verifica especificos campos que sean numericos
     * 
     * @param type $field
     * @param type $value
     * @throws \InvalidArgumentException
     */
    public function isNumeric($data)
    {
        if(!(is_numeric($data['mes'])
                &&is_numeric($data['anio'])
                &&is_numeric($data['puntos']))){
           throw new \InvalidArgumentException(
                'El valor mes, año y puntos se requiere numerico');
        }
    }  
    
    /**
     * Valida que no tenga mas de un registro del
     * key definido mes-año
     * 
     * @param type $data
     * @throws \InvalidArgumentException
     */
    public function hasUniqueKey($data)
    {
        if(!$this->getUniqueKey($data)){
            throw new \InvalidArgumentException(
                    'El usuario '.$data['username'].
                    ' no debe de tener mas de un registro para el mes '.
                    $data['mes'].' año '.$data['anio']);              
        }
    }
    
    /**
     * 
     * @param type $pattern
     * @param type $data
     * @param type $key
     * @param type $msg
     * @throws \InvalidArgumentException
     */
    public function hasRegex($pattern,$data,$key,$msg)
    {
        if(preg_match($pattern, $data[$key])!==1){
           throw new \InvalidArgumentException($msg); 
        }
    }
    
    /**
     * 
     * @param type $user
     * @param type $amount
     * @param type $fileId
     * @param type $year
     * @param type $month
     * @param type $details
     */
    public function setTransactions($user,$amount,$fileId,$year,$month,$details='')
    {     
        $balance = $this->getBalanceSnapshot($user->getId());
        $balance = is_null($balance)?0:$balance;            
        $UserReference = $this->_em->getReference(
                    OauthUsers::class, $user->getId());         
        $newTransaction = new CoreUserTransactions();
        $newTransaction->setUser($UserReference);
        $newTransaction->setAmount($amount);
        $newTransaction->setCorrelationId($fileId); //@todo insert file Id
        $newTransaction->setBalanceSnapshot($balance);
        $newTransaction->setDetails($details); //@todo insert details if requested
        $newTransaction->setType(CoreUserTransactions::TYPE_RESULT);            
        $newTransaction->setCreatedAt(new \DateTime());
        $newTransaction->setAppliedAt(new \DateTime($year.'-'.$month));
        $this->_em->persist($newTransaction);
    }
    
    /**
     * Get User Transaction balance
     * 
     * @param integer $userId
     * @return integer
     */
    public function getBalanceSnapshot($userId) 
    {
        return $this->_em->getRepository(CoreUserTransactions::class)
            ->createQueryBuilder('T')
            ->select('SUM(T.amount)')
            ->where('T.user = :user_id')
            ->setParameter('user_id', $userId)
            ->getQuery()
            ->getSingleScalarResult();
    }
    
    /**
     * 
     * @return type
     */
    public function getQbCollection()
    {
        return $this
                ->createQueryBuilder('U')
                ->select('U.id,U.mes,U.anio,U.ventas,'.
                        'U.cuota,U.puntos,R.displayName as userId')
                ->innerJoin('U.user', 'R');          
    }
    
    /**
     * 
     * @param type $data
     * @return type
     */
    public function getUniqueKey($data){
        $isUnikey = false;
        try{
            $this->_em->getRepository(PmrRules::class)
                ->createQueryBuilder('T')
                ->select('T')
                ->innerJoin('T.user', 'R')
                ->where('T.user = :user_id AND T.mes = :mes AND T.anio = :anio')
                ->setParameter('user_id', $data['user_id'])
                ->setParameter('mes', $data['mes'])
                ->setParameter('anio', $data['anio'])    
                ->getQuery()
                ->getSingleResult();  
        } catch (\Exception $ex) {
            $isUnikey = true;
        }
        return $isUnikey;
    }
    
    public function getUserByUsername($username){
        return $this->_em->getRepository(OauthUsers::class)
            ->createQueryBuilder('U')
            ->where('U.username = :username')
            ->setParameter('username', $username)
            ->where('U.enabled = :enabled')
            ->setParameter('enabled', 1)                   
            ->getQuery()
            ->getSingleResult();       
    }
    

    
    /**
     * Genera archivo csv apartir de nombres de columna de tabla
     * pmr_rules que es customizable
     * 
     * @param type $filename
     * @return type
     */
    public function getColumns()
    {
        $columns = $this->_em->getClassMetadata(PmrRules::class)
                ->getFieldNames();
        return $this->exeptionColumn($columns);
              
    }
    
    /**
     * Quita colmnas no necesarias
     * 
     * @param type $items
     * @return type
     */
    private function exeptionColumn($items)
    {
        $newItems = ['user_id'];
        foreach ($items as $item){
            if($item !== 'id'&& $item!=='createdAt'){
                $newItems[]=$item;
            }
        }
        return $newItems;
    }  
    
    /**
     * Valida que no tenga mas de un registro del
     * key definido mes-año
     * 
     * @param type $data
     * @throws \InvalidArgumentException
     */
    public function hasColumns($data)
    {
        $columns = $this->getColumns();
            foreach ($columns as $column){
                if(!isset($data[$column])){
                    throw new \Exception('Format_invalid_csv');                 
                }  
        }
    }

    public function hasColumnsRequired($data)
    {
        $columns = $this->getColumns();
        if(count($columns)!==count($data)){
            throw new \Exception('Columns_unknow'); 
        }
        
    }
    
}
