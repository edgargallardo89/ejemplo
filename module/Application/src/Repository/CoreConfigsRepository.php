<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Application\Entity\CoreConfigs;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;

/**
 *
 */
class CoreConfigsRepository extends EntityRepository
{
    /**
     * Fetch all config values
     *
     * @return array
     */
    public function fetchAll()
    {
        $settings = $this->createQueryBuilder('C')->select('C.key,C.value')->where('C.visibility = 1')->getQuery()->getArrayResult();
        $items = [];

        array_map(
            function ($e) use (&$items) {
                $items[$e['key']] = $e['value'];
            },
            $settings
        );

        return array_merge(
            ['lang' => [],],
            $items
        );
    }

    /**
     * Search for a specific key in the configuration
     *
     * @param $key
     * @return mixed
     */
    public function findByKey($key)
    {
        return $this
            ->createQueryBuilder('C')
            ->where('C.key = :key')
            ->setParameter('key', $key, Type::STRING)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * Update configuration values
     *
     * @param $data
     * @return bool|mixed
     */
    public function saveConfiguration($data)
    {
        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($currentRepo, $data) {
            // Validate empty
            if (count((array)$data) == 0) {
                throw new \Exception('Empty_Payload');
            }

            // Evaluate keys
            foreach ($data as $key => $value) {
                // Process valid keys and non-empty values
                if ($currentRepo->isValidKey($key) && $this->isNotEmpty($key, $value)) {
                    // Key/value validation
                    $validKeyValue = $this->isValidKeyValue($key, $value);

                    if ($validKeyValue !== true) {
                        throw new \Exception($validKeyValue);
                    }

                    // Search for the registered key
                    $config = $currentRepo->findOneBy(['key' => $key]);

                    // Update if the record exists, insert otherwise
                    if ($config !== null) {
                        $config->setValue((string)$value);
                    } else {
                        $config = new CoreConfigs();
                        $config->setKey($key);
                        $config->setValue((string)$value);
                        $config->setVisibility(false);

                        $em->persist($config);
                    }
                }
            }

            return ["message" => "success"];
        });
    }

    /**
     * Command to compile theme
     *
     * @param string $id
     * @param string $args
     * @return string NULL
     */
    public function compileTheme($id, $args)
    {
        $command = "adbox build --prod --no-maps --project=/home/ubuntu/client/adbox-demo-deployed --only-targets app --only-themes " . $this->escapeshell($id) . " --theme-vars " . $this->escapeshell($args) . " --dist /home/ubuntu/client/adbox-demo-deployed/dist";

        return $this->runCompilation(true, $command);
    }

    /**
     * Command to compile theme preview
     *
     * @param string $id
     * @param string $args
     * @return string Response
     */
    public function compileThemePreview($id, $args)
    {
        $command = "adbox build --dev --theme-preview --project=/home/ubuntu/client/adbox-demo-deployed --only-targets app --only-themes " . $this->escapeshell($id) . " --theme-vars " . $this->escapeshell($args);

        return $this->runCompilation(false, $command);
    }

    /**
     * Compile theme via shell and return the output as a string
     *
     * @param bool $async TRUE executes asynchronous call (no output response)
     * @param $command
     * @return string Output if asynchronous is disabled
     */
    private function runCompilation($async, $command)
    {
        // Set asynchronous if enabled
        $command .= $async ? " > /dev/null &" : "";

        // Execute and wait for response if not asynchronous
        return shell_exec($command);
    }

    /**
     * Escape a string to be used as a shell argument
     *
     * @param string $arg The argument that will be escaped.
     *
     * @return string The escaped string.
     */
    private function escapeshell($arg)
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return '"' . addcslashes($arg, '\\"') . '"';
        } else {
            return escapeshellarg($arg);
        }
    }

    /**
     * Verifies if a key is allowed to be included in the payload
     *
     * @param $key
     * @return bool
     */
    private function isValidKey($key)
    {
        $validKeys = [
            'auth.registration',
            'client.url',
            'catalog.motivale.id',
            'checkout.delivery.mode',
            'checkout.delivery.userEditable',
            'survey.isMandatory',
            'client.id',
            'client.name',
            'email.account.from',
            'email.account.fromname',
            'email.setting.host',
            'email.setting.port',
            'email.setting.connectionConfig.username',
            'email.setting.connectionConfig.password',
            'jira.collectorScript',
            'jira.components',
            'google.analytics.trackingId',
            'adcinema.project.id',
            'adcinema.server.development.url',
            'adcinema.server.production.url',
            'theme.configuration',
            'social.facebook',
            'social.twitter',
            'social.youtube',
            'social.linkedin',
            'modal.home',
            'modal.products',
        ];

        return in_array($key, $validKeys) || strpos($key, 'jira.params.') !== false;
    }

    /**
     * Determine whether a variable is considered to be empty
     *
     * @param $key
     * @param $value
     * @return bool
     */
    private function isNotEmpty($key, $value)
    {
        switch ($key) {
            case 'social.facebook':
            case 'social.twitter':
            case 'social.youtube':
            case 'social.linkedin':
                return isset($value);
            default:
                return isset($value) && $value !== "";
        }
    }

    /**
     * Extra validation per key
     *
     * @param $key
     * @param $value
     * @return bool|string
     */
    private function isValidKeyValue($key, $value)
    {
        switch ($key) {
            case 'theme.configuration':
                // Check for valid json value
                json_decode($value);

                return json_last_error() === JSON_ERROR_NONE ? true : 'Invalid_Theme_Configuration';
            case 'social.facebook':
                return $value === "" || mb_strpos($value, 'facebook') !== false ? true : 'Invalid_Social_Facebook';
            case 'social.twitter':
                return $value === "" || mb_strpos($value, 'twitter') !== false ? true : 'Invalid_Social_Twitter';
            case 'social.youtube':
                return $value === "" || mb_strpos($value, 'youtube') !== false ? true : 'Invalid_Social_Youtube';
            case 'social.linkedin':
                return $value === "" || mb_strpos($value, 'linkedin') !== false ? true : 'Invalid_Social_Linkedin';
            default:
                return true;
        }
    }
}