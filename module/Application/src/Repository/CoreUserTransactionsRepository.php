<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Application\Entity\CoreUserTransactions;
use Application\Entity\OauthUsers;
use Application\Entity\PmrRules;

/**
 *
 */
class CoreUserTransactionsRepository extends EntityRepository
{
    /**
     * Bulk Transaction process from the Adjustment file
     *
     * @param  object $data
     * @param  string $fileName CSV file name (log file)
     * @param  string $fileType Plural transaction type (based on CoreFileUploads constants)
     * @param  integer $fileId Correlation Id (analytics data)
     * @return mixed
     * @throws \Exception
     */
    public function bulkCreate($data, $fileName, $fileType, $fileId)
    {
        $errors = [];

        //Point to first row, and validate headers
        $data->getFile()->seek(0);

        //Get first CSV file row (headers)
        $headers = $data->getFile()->current();

        //Strict match exact 4 headers
        if (4 !== count($headers)) {
            $message = (count($headers) > 4) ? 'Sobran columnas' : 'Faltan columnas';
            throw new \Exception('Formato de CSV Inválido. ' . $message);
        }

        //Check valid headers: [username,amount,month,year]
        foreach ($data->getFile()->current() as $field) {
            if (!in_array($field, ['username', 'amount', 'month', 'year'])) {
                throw new \Exception('Formato de CSV Inválido. Cabeceras inválidas');
            }
        }

        //Rewinds file pointer
        $data->rewind();

        //Fetch CSV rows
        foreach ($data as $row => $fields) {
            $canInsert = true;

            //Check valid headers: [username,amount,month,year]
            foreach (array_keys($fields) as $field) {
                if (!in_array($field, ['username', 'amount', 'month', 'year'])) {
                    throw new \Exception('Formato de CSV Inválido. Cabeceras inválidas');
                }
            }

            //Strict exact 4 fields
            if (4 !== count($fields)) {
                $message = (count($fields) > 4) ? 'Sobran columnas' : 'Faltan columnas';
                throw new \Exception('Formato de CSV Inválido. ' . $message . ' en fila: ' . $row);
            }

            //Reset fetching fields
            $user = $amount = $balance = $year = $month = null;

            foreach ($fields as $field => $value) {
                try {
                    //Get User reference (relation field)            
                    if ('username' === $field) {
                        $qb = $this->_em->getRepository(OauthUsers::class)
                            ->createQueryBuilder('U');

                        $user = $qb
                            ->where('U.username = :username')
                            ->setParameter('username', $value, \Doctrine\DBAL\Types\Type::STRING)
                            ->getQuery()
                            ->getSingleResult();
                    }

                    if (in_array($field, ['amount', 'month', 'year']) and !is_numeric($value) and !is_int($value)) {
                        throw new \InvalidArgumentException('Invalid_Argument_Type');
                    }

                    /**
                     * @var $amount int
                     * @var $month int
                     * @var $year int
                     */
                    if (in_array($field, ['amount', 'month', 'year'])) {
                        $$field = $value;
                    }


                } catch (\Exception $e) {
                    $canInsert = false;

                    switch ($e->getMessage()) {
                        case 'No result was found for query although at least one row was expected.':
                            $errors[] = "Fila $row, Columna $field : El usuario no existe en la base de datos. " . PHP_EOL;
                            break;
                        case 'Invalid_Argument_Type':
                            $errors[] = "Fila $row, Columna $field : Debe ser un número." . PHP_EOL;
                            break;
                        default:
                            $errors[] = $e->getMessage();
                            break;
                    }
                }
            }

            if ($canInsert) {
                $balance = $this->getBalanceSnapshot($user->getId()) ?: 0;
                $newTransaction = new CoreUserTransactions();
                $newTransaction->setUser($user);
                $newTransaction->setAmount($amount);
                $newTransaction->setCorrelationId($fileId); //@todo insert file Id
                $newTransaction->setBalanceSnapshot($balance);
                //                $newTransaction->setDetails(''); //@todo insert details if requested
                $newTransaction->setType(Inflector::singularize($fileType));
                $newTransaction->setCreatedAt(new \DateTime());
                $newTransaction->setAppliedAt(new \DateTime($year . '-' . $month));
                $this->_em->persist($newTransaction);
                $this->_em->flush();
            }
        }

        //Generate Log file if any error found
        if (count($errors) > 0) {
            $this->createLogFile($errors, $fileName);
        }

        return ['errorCount' => count($errors)];
    }

    /**
     * Get User Transaction balance
     *
     * @param integer $userId
     * @return integer
     */
    private function getBalanceSnapshot($userId)
    {
        return $this->createQueryBuilder('T')
            ->select('SUM(T.amount)')
            ->where('T.user = :user_id')
            ->setParameter('user_id', $userId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Create Log File for Adjustment Transactions
     *
     * @param array $errors
     * @param string $fileName
     * @return boolean
     */
    private function createLogFile($errors, $fileName)
    {
        $fp = fopen(__DIR__ . '/../../../../public/logs/' . $fileName . '.log', 'w+');

        foreach ($errors as $error) {
            fwrite($fp, $error);
        }

        return fclose($fp);
    }

    /**
     * Get current user balance values
     *
     * @param string $username Username string
     * @return array           Format: [available,earned,spent]
     */
    public function getBalance($username)
    {
        $dql = 'SELECT ';
        $dql .= 'SUM(T.amount) AS available, ';
        $dql .= 'SUM((CASE WHEN (T.amount > 0 OR (T.amount < 0 AND T.type NOT IN (\'' . CoreUserTransactions::TYPE_ORDER . '\', \'' . CoreUserTransactions::TYPE_ORDER_CANCELLATION . '\'))) THEN T.amount ELSE 0 END)) AS earned, ';
        $dql .= 'ABS(SUM(';
        $dql .= '(';
        $dql .= 'CASE WHEN T.type = \'' . CoreUserTransactions::TYPE_ORDER . '\' ';
        $dql .= 'THEN T.amount ELSE 0 END)';
        $dql .= ')) AS spent, ';
        $dql .= 'SUM((CASE WHEN T.type = \'' . CoreUserTransactions::TYPE_EXTRA . '\' ';
        $dql .= 'THEN T.amount ELSE 0 END)) AS extra ';
        //$dql.= '(SELECT SUM(X.id) FROM '.CoreUserTransactions::class.' X) AS B FROM ';
        $dql .= 'FROM ' . CoreUserTransactions::class . ' T ';
        $dql .= 'INNER JOIN ' . OauthUsers::class . ' U WITH U.id = T.user ';
        $dql .= 'WHERE U.username = :username ';

        return $this->_em->createQuery($dql)
            ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
            ->getScalarResult();
    }

    /**
     * Get user grouped (month,year) transactions
     *
     * @param  string $username Username string
     * @return array            Format: [user_id,month,year,creditsPMR,extraCredits]
     */
    public function getDetails($username)
    {
        $emConfig = $this->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('MONTHNAME', 'Application\Doctrine\MonthName');   
        $emConfig->addCustomDatetimeFunction('YEAR', 'Application\Doctrine\Year'); 
        $emConfig->addCustomDatetimeFunction('MONTH', 'Application\Doctrine\Month');
        
        // TODO: Aplicar solución real: quitar COALESCE y CASE de T.type, tomar únicamente T.appliedAt, en core_user_transactions no se tiene que dejar NULL la columna appliedAt
        $dql = 'SELECT ';
        $dql .= 'MONTHNAME(COALESCE(T.appliedAt, T.createdAt)) AS month,';
        $dql .= 'YEAR(COALESCE(T.appliedAt, T.createdAt)) AS year,';
        $dql .= 'SUM((CASE WHEN T.type IN(\'' . CoreUserTransactions::TYPE_ADJUSTMENT . '\',\'' . CoreUserTransactions::TYPE_CORRECTION . '\',\'' . CoreUserTransactions::TYPE_RESULT . '\') THEN T.amount ELSE 0 END)) as credits,';
        $dql .= '(';
        $dql .= 'SELECT PMR.puntos ';
        $dql .= 'FROM ' . PmrRules::class . ' PMR ';
        $dql .= 'WHERE (MONTHNAME(CONCAT(PMR.anio,\'-\',PMR.mes,\'-\',\'01\')) = MONTHNAME(COALESCE(T.appliedAt, T.createdAt))) AND (PMR.anio = YEAR(COALESCE(T.appliedAt, T.createdAt))) AND (PMR.user = T.user)';
        $dql .= ') AS creditsPMR,';
        $dql .= 'SUM((CASE WHEN T.type = \'' . CoreUserTransactions::TYPE_EXTRA . '\' THEN T.amount ELSE 0 END)) as extraCredits ';
        $dql .= 'FROM ' . CoreUserTransactions::class . ' T ';
        $dql .= 'INNER JOIN ' . OauthUsers::class . ' U WITH U.id = T.user ';
        $dql .= 'WHERE U.username = :username ';
        $dql .= 'AND T.type NOT IN(\'' . CoreUserTransactions::TYPE_ORDER . '\', \'' . CoreUserTransactions::TYPE_ORDER_CANCELLATION . '\') ';
        $dql .= 'GROUP BY T.user, year, month ';
        $dql .= 'HAVING credits != 0 OR extraCredits != 0 ';
        $dql .= 'ORDER BY year DESC, MONTH(COALESCE(T.appliedAt, T.createdAt)) DESC';

        return $this->_em->createQuery($dql)
            ->setParameter('username', $username, \Doctrine\DBAL\Types\Type::STRING)
            ->getScalarResult();
    }
    
    /**
     * Get User Statement
     *
     * @param string $username Username string
     * @return array           Format [credits:{},details:{}]
     */
    public function getStatement($username)
    {

        if($this->getEvent('getStatement')){
            return $this->getEvent('getStatement')->getStatement($username);
        }
        /*
         * Statement data
         * * * Notes : (None)
         * .-------------------------------------------------------.
         * .                    Steps                              |
         * .-------------------------------------------------------.
         * . 1) $credits : Balance data                            .
         * . 2) $details : Explained and grouped user transactions .
         * .-------------------------------------------------------.
         */
        $balance = $this->getBalance($username);
        $credits = array_pop($balance);
        $details = $this->getDetails($username);
        array_walk(
            $credits,
            function (&$ele) {
                $ele = (int)$ele;
            }
        );

        array_walk(
            $details,
            function (&$ele) {
                foreach ($ele as $key => $val) {
                    if (in_array($key, ['credits', 'creditsPMR', 'extraCredits'])) {
                        $ele[$key] = (int)$ele[$key];
                    }
                }
            }
        );

        return [
            'credits' => $credits,
            'details' => $details,
        ];
    }

    /**
     * Gets total points including: available, earned, spent and extra
     * @return mixed
     */
    public function getTotalPoints()
    {
        $results = $this
            ->createQueryBuilder('T')
            ->select('SUM(T.amount) AS available')
            ->addSelect('SUM(CASE WHEN T.amount > 0 OR (T.amount < 0 AND T.type NOT IN (\'' . CoreUserTransactions::TYPE_ORDER . '\', \'' . CoreUserTransactions::TYPE_ORDER_CANCELLATION . '\')) THEN T.amount ELSE 0 END) AS earned')
            ->addSelect('ABS(SUM(CASE WHEN T.type = \'' . CoreUserTransactions::TYPE_ORDER . '\' THEN T.amount ELSE 0 END)) AS spent')
            ->addSelect('SUM(CASE WHEN T.type = \'' . CoreUserTransactions::TYPE_EXTRA . '\' THEN T.amount ELSE 0 END) AS extra')
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);

        return array_map(function ($value) {
            return intval($value);
        }, $results);
    }

    public function getEvent($key)
    {
        $config = $this->service->get('config');
        if(isset($config['application-repository-coreusertransactionsrepository'])){
            $event = $config['application-repository-coreusertransactionsrepository']
                    ['events'][$key];
            return new $event($this->service);            
        }
        return false;
    } 
    
    public function setEvent($service)
    {
        $this->service = $service;
    }   
}
