<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\CoreFileUploads;
use Application\Entity\CoreUserTransactions;
use Application\Entity\OauthUsers;
use Application\Entity\CoreFileUploadReasons;
use Application\Entity\PmrRules;

/**
 * 
 */
class CoreFileUploadsRepository extends EntityRepository
{    
    /**
     * Creates a new File
     * 
     * @param array $data
     * @return boolean
     * @throws \Exception
     */
    public function create($items,$data,$filename,$identity)
    {
        $currentRepo = $this;
        
        $this->_em->transactional(
            function ($em) use($items,$data,$identity,$filename,$currentRepo) {
                $user = $currentRepo->getUser($identity);
                $currentRepo->InsertFileUpload($data, $user, $filename); 
                foreach ($items as $key => $item){
                    $currentRepo->insertItems($key,$item);
                    
                }
            }
        );
        return $this->getErrors();
    }
    
    /**
     * 
     * @return type
     */
    public function getQbCollection()
    {
        $emConfig = $this->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE_FORMAT', 'Application\Doctrine\DateFormat');          
        return $this
                ->createQueryBuilder('U')
                ->select("U.id,U.description,U.fileName,".
                        "CONCAT(U.fileName,'.log') as log,R.displayName as userId, DATE_FORMAT(U.uploadedAt,'%d-%m-%Y %H:%i:%s') as uploadedAt")
                ->innerJoin('U.user', 'R')
                ->where('U.fileType = :fileType')
                ->setParameter('fileType', 'results');          
    } 

    /**
     * 
     * @return type
     */
    public function getQbItem($id)
    {
        return $this
                ->createQueryBuilder('U')
                ->select("U.id,U.description,U.fileName,".
                        "CONCAT(U.fileName,'.log') as log,R.displayName as userId")
                ->innerJoin('U.user', 'R')
                ->where('U.id = :id')
                ->setParameter('id', $id)->getQuery()
                ->getSingleResult();          
    } 
    
    /**
     * Get User
     * 
     * @param type $identity
     * @return type
     */
    public function getUser($identity)
    {
        return $this->_em->getRepository(OauthUsers::class)
                ->fetchByOne($identity['user_id']);        
    }
        
    public function insertItems($key,$data)
    {
        $Table = $this->_em->getRepository(PmrRules::class);
        $Table->hasColumnsRequired($data);
        $Table->hasColumns($data);        
        return $Table->create($key,$data);          
    }
      
    public function getErrors()
    {
        return $this->_em->getRepository(PmrRules::class)
                ->errors; 
    }

    /**
     * 
     * @param type $data
     * @param type $user
     * @param type $filename
     */
    public function InsertFileUpload($data,$user,$filename)
    {            
            $pathinfo = pathinfo($filename['tmp_name']);            
            $entities = new CoreFileUploads(); 
            $userId = $this->_em->getReference(OauthUsers::class, $user['id']);
            $entities->setDescription($data->description)
                     ->setFileName($pathinfo['basename'])
                     ->setFileType(CoreFileUploads::TYPE_RESULTS)
                     ->setUser($userId);            
             $this->_em->persist($entities);         
    }

    
    /**
     * Save Adjustments Upload File and save User Transactions from it.
     * 
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function saveAdjustments($data)
    {
        if(!defined(CoreFileUploads::class.'::'.$data['fileType'])) {
            throw new \Exception('Invalid_FileType');
        }
        
        return $this->_em->transactional(function ($em) use($data) {
            $newFile = new CoreFileUploads();
            
            $fileType = constant(CoreFileUploads::class.'::'.$data['fileType']);
        
            foreach($data as $field => $value) {
                //Get User reference (relation field)
                if ('user' === $field) {
                    $value = $this->_em->getRepository(OauthUsers::class)
                        ->createQueryBuilder('U')
                        ->where('U.username = :username')
                        ->setParameter('username', $value)
                        ->where('U.enabled = :enabled')
                        ->setParameter('enabled', 1)                            
                        ->getQuery()
                        ->getSingleResult();
                }
                
                //Get Reason reference (relation field)
                if ('reasonId' === $field) {
                    $field = 'reason';
                    $value = $this->_em->getReference(CoreFileUploadReasons::class, $value);
                }
                
                //Get File Type from CoreFileUploads class defined constants
                if ('fileType' === $field) {
                    $value = $fileType;
                }
                
                if (method_exists($newFile, 'set'.ucfirst($field))) {
                    $newFile->{'set'.ucfirst($field)}($value);
                }
            }            
            
            $em->persist($newFile);
            $em->flush();
            
            return $this->_em
                ->getRepository(CoreUserTransactions::class)
                ->bulkCreate(
                    $data['transactions'],
                    $newFile->getFileName(),
                    $fileType,
                    $newFile->getId()
                );            
        });
    }
    
    /**
     * Query builder to retrieve paged collection
     * 
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function seekerQb()
    {
        $qb = $this->createQueryBuilder('CFU');
        
        $types = [
            CoreFileUploads::TYPE_ADJUSTMENTS,
            CoreFileUploads::TYPE_CORRECTIONS,
            CoreFileUploads::TYPE_EXTRAS,
        ];
        
        $fileTypes = '(CASE WHEN CFU.fileType = \''.CoreFileUploads::TYPE_ADJUSTMENTS.'\' THEN \'Ajustes\' ';
        $fileTypes.= 'WHEN CFU.fileType = \''.CoreFileUploads::TYPE_CORRECTIONS.'\' THEN \'Correcciones\' ';
        $fileTypes.= 'WHEN CFU.fileType = \''.CoreFileUploads::TYPE_EXTRAS.'\' THEN \'Extras\' ';
        $fileTypes.= 'ELSE CFU.fileType END) AS fileType';
        
        return $qb
            ->select('CFU.id,CFU.description')
            ->addSelect('CFU.fileName')
            //->addSelect('CONCAT(\''.$serverUrl.'\',CFU.fileName) AS fileUrl')
            ->addSelect('CONCAT(U.firstName,\' \',U.lastName,\'(\',U.username,\')\') AS createdByUser')
            ->addSelect($fileTypes)
            ->addSelect('CFU.uploadedAt')
            ->addSelect('R.reason')
            ->innerJoin('CFU.user', 'U')
            ->innerJoin('CFU.reason', 'R')
            ->where($qb->expr()->in('CFU.fileType', $types));
    }
    
    /**
     * Retrieves all "adjustment" files
     * 
     * @return array
     */
    public function fetchAllAjustments()
    {
        $qb = $this->createQueryBuilder('CFU');
        
        $types = [
            CoreFileUploads::TYPE_ADJUSTMENTS,
            CoreFileUploads::TYPE_CORRECTIONS,
            CoreFileUploads::TYPE_EXTRAS,
        ];
        
        $fileTypes = '(CASE WHEN CFU.fileType = \''.CoreFileUploads::TYPE_ADJUSTMENTS.'\' THEN \'Ajustes\' ';
        $fileTypes.= 'WHEN CFU.fileType = \''.CoreFileUploads::TYPE_CORRECTIONS.'\' THEN \'Correcciones\' ';
        $fileTypes.= 'WHEN CFU.fileType = \''.CoreFileUploads::TYPE_EXTRAS.'\' THEN \'Extras\' ';
        $fileTypes.= 'ELSE CFU.fileType END) AS fileType';
        
        return $qb
            ->select('CFU.id,CFU.description')
            ->addSelect('CFU.fileName')
            //->addSelect('CONCAT(\''.$serverUrl.'\',CFU.fileName) AS fileUrl')
            ->addSelect('CONCAT(U.firstName,\' \',U.lastName,\'(\',U.username,\')\') AS createdByUser')
            ->addSelect($fileTypes)
            ->addSelect('CFU.uploadedAt')
            ->addSelect('R.reason')
            ->innerJoin('CFU.user', 'U')
            ->innerJoin('CFU.reason', 'R')
            ->where($qb->expr()->in('CFU.fileType', $types))
            ->getQuery()
            ->getArrayResult();
    }
}