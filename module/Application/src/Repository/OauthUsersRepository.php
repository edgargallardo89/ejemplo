<?php

namespace Application\Repository;

use Application\Entity\CoreCedis;
use Application\Entity\CoreGroups;
use Application\Entity\CoreUserAddresses;
use Application\Entity\OauthUsers;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Zend\Crypt\Password\Bcrypt;
use Application\Entity\CorePasswordRecovery;
use Application\Entity\CoreRoles;
use Application\Entity\CoreSendemail;
use Application\Entity\CoreMessages;
use Application\Entity\CoreConfigs;
use Application\Entity\OauthAccessTokens;
use Application\Entity\CoreUserTransactions;
use Application\Entity\CorePermissions;

/**
 * OauthUsers
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OauthUsersRepository extends EntityRepository
{
    protected $service;
    
    /**
     * Create User
     *
     * @param object $data
     * @return mixed
     */
    public function create($data, $params)
    {
        $currentRepo = $this;
        
         if($this->getEvent('create')){
                return $this->getEvent('create')->create($data);
         }
         
        return $this->_em->transactional(function ($em) use ($data, $currentRepo, $params) {

            $userExists = $currentRepo->exists($data->email);

            if (true === $userExists) {
                throw new \Exception('User_Exists',422);
            }

            $oauthUser = new \Application\Entity\OauthUsers();

            //Secure Password non-encrypted
            $password = bin2hex(random_bytes(4));

            //Generate Secure Password usign BCrypt
            $bcrypt = new Bcrypt();
            $securePass = $bcrypt->create($password);

            //Register Role (id: 6)
            $role = $em->getReference(CoreRoles::class, 6);

            $oauthUser
                ->setUsername($data->email)
                ->setPassword($securePass)
                ->setCreatedAt(new \DateTime())
                ->setEmail($data->email)
                ->setFirstName($data->name)
                ->setLastName($data->lastname)
                ->setDisplayName($data->name . ' ' . $data->lastname)
                ->setProfileFulfilled(0)
                ->setEnabled(1)
                ->setRole($role);

            $em->persist($oauthUser);

            $params['email'] = $data->email;

            $senemailEntry = (new CoreSendemail())
                ->setTemplate($params['template'])
                ->setDateAdded(new \DateTime())
                ->setFrom($params['from'])
                ->setTo($params['email'])
                ->setCc($params['cc'])
                ->setSubject($params['subject'])
                ->setStatus('new');

            $em->persist($senemailEntry);

            return $password;
        });
    }

    /**
     * Bulk insertion from csv upload
     *
     * @param object $data
     * @return mixed|array
     * @throws \Exception
     */
    public function bulkCreate($data)
    {
        $bcrypt = new Bcrypt();

        $errors = [];

        // Point to first row, and validate headers
        $data->getFile()->seek(0);

        // Get first CSV file row (headers)
        $headers = $data->getFile()->current();

        // Strict match exact 7 headers
        if (count($headers) !== 7) {
            $message = (count($headers) > 7) ? 'Sobran columnas' : 'Faltan columnas';
            throw new \Exception('Formato de CSV Inválido. ' . $message);
        }

        // Check valid headers: [firstName, lastName, surname, email, roleId, password, groupId]
        foreach ($data->getFile()->current() as $field) {
            if (!in_array($field, ['firstName', 'lastName', 'surname', 'email', 'roleId', 'password', 'groupId'])) {
                throw new \Exception('Formato de CSV Inválido. Cabeceras inválidas');
            }
        }

        // Rewinds file pointer
        $data->rewind();

        // Fetch CSV rows
        foreach ($data as $row => $fields) {
            $canInsert = true;

            // Check valid headers: [firstName, lastName, surname, email, roleId, password, groupId]
            foreach (array_keys($fields) as $field) {
                if (!in_array($field, ['firstName', 'lastName', 'surname', 'email', 'roleId', 'password', 'groupId'])) {
                    throw new \Exception('Formato de CSV Inválido. Cabeceras inválidas');
                }
            }

            // Strict exact 7 fields
            if (count($fields) !== 7) {
                $message = (count($fields) > 7) ? 'Sobran columnas' : 'Faltan columnas';
                throw new \Exception('Formato de CSV Inválido. ' . $message . ' en fila: ' . $row);
            }

            // Reset fields
            $firstName = $lastName = $surname = $email = $role = $password = $group = null;

            foreach ($fields as $field => $value) {
                try {
                    // Validate required
                    if (in_array($field, ['firstName', 'lastName', 'email', 'roleId', 'password', 'groupId']) && (!isset($value) || $value === '')) {
                        throw new \InvalidArgumentException('Invalid_Argument_Type');
                    }

                    // Validate unique user
                    if ($field === 'email') {
                        if ($this->exists($value) === true) {
                            throw new \Exception('User_Exists');
                        }
                    }

                    // Role reference
                    if ($field === 'roleId') {
                        $role = $this->_em->getRepository(CoreRoles::class)->find($value);

                        if ($role === null) {
                            throw new \Exception('Invalid_Role');
                        }
                    }

                    // Group reference
                    if ($field === 'groupId') {
                        $group = $this->_em->getRepository(CoreGroups::class)->find($value);

                        if ($group === null) {
                            throw new \Exception('Invalid_Group');
                        }

                        if ($group->getDeletedAt() !== null) {
                            throw new \Exception('Deleted_Group');
                        }
                    }

                    // Set password
                    if ($field === 'password') {
                        $password = $bcrypt->create($value);
                    }

                    if (in_array($field, ['firstName', 'lastName', 'surname', 'email'])) {
                        $$field = $value;
                    }
                } catch (\Exception $e) {
                    $canInsert = false;

                    switch ($e->getMessage()) {
                        case 'User_Exists':
                            $errors[] = "Fila $row, Columna $field : El usuario ya existe en la base de datos. " . PHP_EOL;
                            break;
                        case 'Invalid_Role':
                            $errors[] = "Fila $row, Columna $field : El rol no existe en la base de datos. " . PHP_EOL;
                            break;
                        case 'Invalid_Group':
                            $errors[] = "Fila $row, Columna $field : El grupo no existe en la base de datos. " . PHP_EOL;
                            break;
                        case 'Deleted_Group':
                            $errors[] = "Fila $row, Columna $field : El grupo que se quiere asignar se encuentra eliminado. " . PHP_EOL;
                            break;
                        case 'Invalid_Argument_Type':
                            $errors[] = "Fila $row, Columna $field : El valor es requerido. " . PHP_EOL;
                            break;
                        default:
                            $errors[] = $e->getMessage();
                            break;
                    }
                }
            }

            if ($canInsert) {
                // Create user
                $oauthUser = new OauthUsers();

                // Basic information
                $oauthUser
                    ->setUsername($email)
                    ->setEmail($email)
                    ->setFirstName($firstName)
                    ->setLastName($lastName)
                    ->setDisplayName($firstName . ' ' . $lastName . ($surname !== null ? ' ' . $surname : ''))
                    ->setSurname($surname)
                    ->setCreatedAt(new \DateTime())
                    ->setProfileFulfilled(0)
                    ->setEnabled(1)
                    ->setRole($role)
                    ->setPassword($password);

                // Group information
                $oauthUser->addGroup($group);
                $group->addUser($oauthUser);

                $this->_em->persist($oauthUser);
                $this->_em->flush();
            }
        }

        return ['errorCount' => count($errors), 'errors' => $errors];
    }

    /**
     * User-performed Profile fulfillment
     *
     * @param integer $id
     * @param array $data
     * @return boolean
     */
    public function fulfillProfile($id, $data, $params)
    {
        /*
         * Profile fulfillment Transaction Workflow
         * * * Notes : https://bis-adventa.atlassian.net/wiki/display/ADBOX/Complemento+de+datos
         */
        $currentRepo = $this;

        if($this->getEvent('fulfillProfile')){
                $oauthUser = $currentRepo->find($id);
                $params['email'] = isset($data->email) ? $data->email : $oauthUser->getEmail();
                $params['entity'] = ['displayName' => $oauthUser->getDisplayName()];
                return $this->getEvent('fulfillProfile')->fulfillProfile($id, $data, $params);
        }

        return $this->_em->transactional(function ($em) use ($currentRepo, $id, $data, $params) {

            if (property_exists($data, 'email') and $currentRepo->existing($id, $data->email)) {
                throw new \Exception('User_Exists',422);
            }

            $oauthUser = $currentRepo->find($id);
            
            $oauthUser
                ->setFirstName($data->firstName)
                ->setLastName($data->lastName)
                ->setDisplayName($data->firstName . ' ' . $data->lastName)
                ->setMobile($data->mobile)
                ->setModifiedAt(new \DateTime('now', new \DateTimeZone('America/Mexico_City')))
                ->setProfileFulfilled(1)
                ->setSurname(property_exists($data, 'surname') ? $data->surname : null)
                ->setTelephone1(property_exists($data, 'telephone1') ? $data->telephone1 : null)
                ->setTelephone2(property_exists($data, 'telephone2') ? $data->telephone2 : null);

            //If password is submitted: Generate Secure Password usign BCrypt
            if (property_exists($data, 'password')) {
                $bcrypt = new Bcrypt();

                //Update Oauth User Password if Secured new password is not the same that the stored one
                if ($bcrypt->verify($data->password, $oauthUser->getPassword())) {
                    throw new \Exception('Current_Password',422);
                } else {
                    $oauthUser->setPassword($bcrypt->create($data->password));
                }
            }

            //If no creator is set, user assumed as User-created account 
            //and
            //If submitted email is not equals to stored email: Set email and oauth user identifier
            if ((null !== $oauthUser->getCreatedBy()) and property_exists($data, 'email') and ($oauthUser->getEmail() !== $data->email)) {
                $oldUsername = $oauthUser->getUsername();

                // When username equals email then update the username to the new mail
                if ($oldUsername == $oauthUser->getEmail()) {
                    $oauthUser->setUsername($data->email);
                }

                $oauthUser->setEmail($data->email);

                //@Todo: oauth_refresh_tokens, oauth_clients, oauth_access_tokens, oauth_authorization_codes
                $em->getRepository(OauthAccessTokens::class)->updateTokensByUser($oldUsername, $data->email);
            }
            // AD-40 Correo electrónico de bienvenida by eoa
            $params['email'] = isset($data->email) ? $data->email : $oauthUser->getEmail();
            $params['entity'] = ['displayName' => $oauthUser->getDisplayName()];
//            $params['entity'] = ['password' => $oauthUser->getPassword()];
            $this->queueEmail($params);
            
            return $params;
        });
    }

    /**
     * Check existing User
     *
     * @param string $identifier
     * @return boolean
     */
    public function exists($identifier)
    {
        $user = $this->findOneBy(array('email' => $identifier));

        return !(null === $user);
    }

    /**
     * Check existing user by Id. and Email
     *
     * @param integer $id
     * @param string $email
     * @return type
     */
    public function existing($id, $email)
    {
        $count = $this
            ->createQueryBuilder('U')
            ->select('COUNT(1)')
            ->where('U.id <> :id AND U.email = :email')
            ->setParameter('id', $id, \Doctrine\DBAL\Types\Type::INTEGER)
            ->setParameter('email', $email, \Doctrine\DBAL\Types\Type::STRING)
            ->getQuery()
            ->getSingleScalarResult();

        return ($count > 0);
    }

    /**
     * Profile Data
     *
     * @param string $identifier
     * @return array
     */
    public function getProfile($identifier)
    {
        /*
         * Profile data Transaction Workflow
         * * * Notes : (None)
         * .---------------------------------------------.
         * .                    Steps                    |
         * .---------------------------------------------.
         * . 1) Get Core User data (core_users)          .
         * . 2) Get Messages (core_messages)             .
         * . 3) Get Private Config values (core_configs) .
         * .---------------------------------------------.
         */

        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($currentRepo, $identifier) {
            //Get Oauth User
            $user = $currentRepo
                ->createQueryBuilder('U')
                ->select('U, R, CHILD.id as createdBy')
                ->innerJoin('U.role', 'R')
                ->leftJoin('U.createdBy', 'CHILD')
                ->where('U.username = :username')
                ->setParameter('username', $identifier, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

            //Get Messages (a.k.a. translations)
            $messages = $em->getRepository(CoreMessages::class)
                ->createQueryBuilder('M')
                ->select('M.key,M.value')
                ->where('M.visibility = :visibility')
                ->setParameter('visibility', 0, \Doctrine\DBAL\Types\Type::BOOLEAN)
                ->getQuery()
                ->getArrayResult();

            //Get Private Site Configs
            $configs = $em->getRepository(CoreConfigs::class)
                ->createQueryBuilder('M')
                ->select('M.key,M.value')
                ->where('M.visibility = :visibility')
                ->setParameter('visibility', 0, \Doctrine\DBAL\Types\Type::BOOLEAN)
                ->getQuery()
                ->getArrayResult();

            //Reorder config as key-value array
            $config = array();
            foreach ($configs as $item) {
                $config[$item['key']] = $item['value'];
            }

            // Get checkout range dates
            $dateStart = isset($config['checkout.date.start']) ? $config['checkout.date.start'] : 0;
            $dateEnd = isset($config['checkout.date.end']) ? $config['checkout.date.end'] : (pow(2, 32) - 1);

            // Current unix timestamp
            $now = time();

            // Validate if checkout is enabled (start <= now < end)
            $checkoutEnabled = $dateStart <= $now && $now < $dateEnd;

            // Add/replace the checkout.enabled key
            $config['checkout.enabled'] = (int)$checkoutEnabled;

            //Reorder messages as key-value array
            $lang = array();
            foreach ($messages as $message) {
                $lang[$message['key']] = $message['value'];
            }

            //Re-arranging user data
            $user = array_merge($user[0], ['createdBy' => $user['createdBy']]);

            // Rename role
            $user['role']['name'] = $user['role']['role'];
            unset($user['role']['role']);

            // User Address
            $address = $this->_em->getRepository(CoreUserAddresses::class)->getAddress($user['id']);
            $user['address'] = $address;

            // User Cedis
            $cedis = $this->_em->getRepository(CoreCedis::class)->getCedis($user['id']);

            if ($cedis !== null) {
                $cedis['name'] = $cedis['namesCedis'];
                unset($cedis['namesCedis'], $cedis['extra'], $cedis['active'], $cedis['createAt']);
            }

            $user['cedis'] = $cedis;

            //Date Modifiers
            $user['createdAt'] = $user['createdAt']->format('Y-m-d H:i:s');
            $user['modifiedAt'] = (null !== $user['modifiedAt']) ? $user['modifiedAt']->format('Y-m-d H:i:s') : null;


            $configDist = $this->service->get('config');
            if(isset($configDist['updatableFields-oauthusersrepository'])){
                $fieldValues = $configDist['updatableFields-oauthusersrepository'];
                $user['updatableFields'] = $fieldValues;
            }
            else{
                $user['updatableFields'] =  [
                    ['name' => 'firstName'],
                    ['name' => 'lastName'],
                    ['name' => 'surname'],
                    ['name' => 'telephone1'],
                    ['name' => 'telephone2'],
                    ['name' => 'mobile'],
                    ['name' => 'password'],
                    ['name' => 'town'],
                    ['name' => 'distributor'],
                    ['name' => 'branchOffice'],
                    ['name' => 'main'],

                ];
            }

            //Password is not required
            unset($user["password"]);

            //Business rule: if current user is not an admin-created one, email and password must be updatable
            if (null !== $user['createdBy']) {
                $user['updatableFields'][] = ['name' => 'email',];
                $user['updatableFields'][] = ['name' => 'confirmTerms',];
            }

            //Get User Permissions
            $userPermissions = $currentRepo->createQueryBuilder('U')
                ->select('RE.alias,RE.methodhttp,P.permission')
                ->innerJoin('U.role', 'R')
                ->innerJoin('R.permissions', 'P')
                ->innerJoin('P.resource', 'RE')
                ->where('U.username = :username')
                ->andWhere('P.permission = :permission')
                ->setParameter('username', $identifier, \Doctrine\DBAL\Types\Type::STRING)
                ->setParameter('permission', CorePermissions::PERMISSION_ALLOW, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getArrayResult();

            $permissions = [];

            array_map(
                function ($e) use (&$permissions) {
                    $permissions[$e['alias']][$e['permission']][] = $e["methodhttp"];
                },
                $userPermissions
            );

            //Get User Credit Balance
            $balance = $this->_em->getRepository(CoreUserTransactions::class)->getBalance($identifier);
            $credits = ['credits' => array_pop($balance)];

            return [
                'config' => array_merge(['lang' => $lang], $config),
                'user' => array_merge(
                    $user,
                    $credits,
                    [
                        'permissions' => $permissions,
                    ]
                )
            ];
        });
    }

    /**
     * Get User data
     *
     * @param string $identifier
     * @return array
     */
    public function getUser($identifier)
    {
        /*
         * User data Transaction Workflow
         * * * Notes : (None)
         * .------------------------------------------------------------.
         * .                    Steps                                   |
         * .------------------------------------------------------------.
         * . 1) Get Core User data (core_users)                         .
         * . 2) Get Core User Transaction data (core_user_transactions) .
         * .------------------------------------------------------------.
         */

        $currentRepo = $this;

        return $this->_em->transactional(function () use ($currentRepo, $identifier) {
            //Get Core User
            $user = $currentRepo
                ->createQueryBuilder('U')
                ->select('U, R')
                ->innerJoin('U.role', 'R')
                ->where('U.username = :username')
                ->setParameter('username', $identifier, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

            $user['createdAt'] = $user['createdAt']->format('Y-m-d H:i:s');
            $user['modifiedAt'] = (null !== $user['modifiedAt']) ? $user['modifiedAt']->format('Y-m-d H:i:s') : null;

            // Remove password from payload
            unset($user["password"]);

            // Rename role
            $user['role']['name'] = $user['role']['role'];
            unset($user['role']['role']);

            //Get User Credit Balance
            $balance = $this->_em->getRepository(CoreUserTransactions::class)->getBalance($identifier);
            $user['credits'] = array_pop($balance);

            // Get User Address
            $address = $this->_em->getRepository(CoreUserAddresses::class)->getAddress($user['id']);
            $user['address'] = $address;

            // Get User Cedis
            $cedis = $this->_em->getRepository(CoreCedis::class)->getCedis($user['id']);

            if ($cedis !== null) {
                $cedis['name'] = $cedis['namesCedis'];
                unset($cedis['namesCedis'], $cedis['extra'], $cedis['active'], $cedis['createAt']);
            }

            $user['cedis'] = $cedis;

            // Get user group
            $group = $this->_em->getRepository(CoreGroups::class)->fetchOneByUser($user['id']);

            $user['group'] = $group;

            return $user;
        });
    }

    /**
     *
     * @param object $data MUST contain attrs "identifier", "password", "token"
     * @return mixed
     */
    public function resetPassword($data)
    {
        /*
         * Password Recovery Transaction Workflow
         * * * Notes : (None)
         * .-------------------------------------------------------------------------------------------.
         * .                                           Steps                                           |
         * .-------------------------------------------------------------------------------------------.
         * . 1) Verifies the Password Reset Entry (core_password_recovery)                             .
         * .   1.1) If params met: Disables the consumed recovery item (core_password_recovery)        .
         * .   1.2) If not, an exception is thrown                                                     .
         * . 2) Generate Secure Password usign BCrypt                                                  .
         * . 3) Find the Oauth User (oauth_users)                                                      .
         * . 4) Update Oauth User Password                                                             .
         * .   4.1) If Secured new password is not the same that the stored one, password is persisted .
         * .   4.2) If not an exception is thrown                                                      .
         * .-------------------------------------------------------------------------------------------.
         */

        $currentRepo = $this;

        return $this->_em->transactional(function ($em) use ($data, $currentRepo) {
            $emConfig = $currentRepo->getEntityManager()->getConfiguration();
            $emConfig->addCustomDatetimeFunction('TIMESTAMPDIFF', 'Application\Doctrine\TimestampDiff');  
            
            //Verifies the Password Reset Entry
            $selectStmt = 'PR.id, COUNT(1) as Counter,';
            $selectStmt .= '(CASE WHEN TIMESTAMPDIFF(SECOND, PR.createdAt, CURRENT_TIMESTAMP()) <= PR.timeout THEN 0 ELSE 1 END) AS timeOutError';

            $canReset = $em->getRepository(CorePasswordRecovery::class)
                ->createQueryBuilder('PR')
                ->select($selectStmt)
                ->innerJoin('PR.user', 'U')
                ->where('PR.token = :token')
                ->andWhere('TIMESTAMPDIFF(SECOND, PR.createdAt, CURRENT_TIMESTAMP()) <= PR.timeout')
                ->andWhere('U.email = :email')
                ->andWhere('PR.enabled = 1')
                ->setParameters(
                    ['token' => $data->token, 'email' => $data->identifier,],
                    [\Doctrine\DBAL\Types\Type::STRING, \Doctrine\DBAL\Types\Type::STRING,]
                )
                ->groupBy('PR.id,PR.createdAt,PR.timeout')
                ->getQuery()
                ->getScalarResult();

            //No matches found
            if (empty($canReset)) {
                throw new \Exception('Invalid_Request');
            }

            //Query Validations: TimeOut and Invalid Request
            if (false === (boolean)$canReset[0]['Counter']) {
                if (true === (boolean)$canReset[0]['timeOutError']) {

                    throw new \Exception('Petition_Expired');
                }

                throw new \Exception('Invalid_Request');
            }

            //Disables core_password_recovery consumed
            $qb = $em->createQueryBuilder();
            $qb->update(CorePasswordRecovery::class, 'PR')
                ->set('PR.enabled', 0)
                ->where('PR.id = ?1')
                ->setParameter(1, $canReset[0]['id'], \Doctrine\DBAL\Types\Type::BOOLEAN)
                ->getQuery()
                ->execute();

            //Generate Secure Password usign BCrypt
            $bcrypt = new Bcrypt();
            $securePass = $bcrypt->create($data->password);

            //Find the Oauth User
            $oauthUser = $currentRepo
                ->createQueryBuilder('OU')
                ->select('OU')
                ->where('OU.email = :email')
                ->setParameter('email', $data->identifier, \Doctrine\DBAL\Types\Type::STRING)
                ->getQuery()
                ->getSingleResult();

            $currentPassword = $oauthUser->getPassword();

            //Update Oauth User Password if Secured new password is not the same that the stored one
            if ($bcrypt->verify($data->password, $currentPassword)) {
                throw new \Exception('Current_Password');
            } else {
                $oauthUser->setPassword($securePass);
            }
        });
    }

    /**
     *
     * @param type $username
     * @return type
     */
    public function fetchByOne($username)
    {
        return $this
            ->createQueryBuilder('U')->select('R.role,U.id')
            ->join('U.role', 'R')
            ->where('U.username = :username')->setParameter('username', $username)
            ->getQuery()->getSingleResult();
    }

    /**
     *
     * @param type $identifier
     * @return type
     */
    public function hasEnabledUser($identifier)
    {

        $currentRepo = $this;

        return $this->_em->transactional(function () use ($currentRepo, $identifier) {
            //Get Core User
            $user = $currentRepo
                ->createQueryBuilder('U')
                ->where('U.username LIKE :username')
                ->setParameter('username', $identifier, \Doctrine\DBAL\Types\Type::STRING)
                ->andWhere('U.enabled = :enabled')
                ->setParameter('enabled', 1)
                ->getQuery()
                ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
            return $user;
        });
    }

    /**
     * Find all users information
     *
     * @param string|null $username
     * @param  array $params
     * @return array
     */
    public function getUsers($username = null, $params)
    {
        // Get active users
        $query = $this->createQueryBuilder('U');
        if($params->count()>0&&empty($params->get('query'))){
            return [];           
        }
 
        
        $query->select('U, R, partial G.{id, name}')
            ->join('U.role', 'R')
            ->leftJoin('U.groups', 'G');

        // Parameter validation
        if (isset($params['canCheckout'])
            && array_search($params['canCheckout'], [1, 0]) !== false
        ) {
            // Users capable of realize checkout
            if ($params['canCheckout'] == '1') {
                $query
                    ->andWhere('U.enabled = :enabled')
                    ->setParameter('enabled', true, Type::BOOLEAN);
            } else {
                $query
                    ->andWhere('U.enabled = :enabled')
                    ->setParameter('enabled', false, Type::BOOLEAN);
            }
        }

        // User to exclude from the result payload
//        if ($username != null) {
//            $query->andWhere('U.username != :username')
//                ->setParameter('username', $username, Type::STRING);
//        }
            $query
                ->andWhere('U.displayName LIKE :displayName')
                ->setParameter('displayName',$params->get('query').'%' , Type::STRING);  

        $query->setMaxResults(50)->setFirstResult(1);
        $users = $query->getQuery()->getArrayResult();

        // Process each user data
        $users = array_map(function ($user) {
            // Format dates
            $user['createdAt'] = $user['createdAt'] !== null ? $user['createdAt']->format('Y-m-d H:i:s') : null;
            $user['modifiedAt'] = $user['modifiedAt'] !== null ? $user['modifiedAt']->format('Y-m-d H:i:s') : null;

            // Remove password
            unset($user["password"]);

            // Rename role
            $user['role']['name'] = $user['role']['role'];
            unset($user['role']['role']);

            // User group, fix to one only
            $groups = $user['groups'];
            unset($user['groups']);
            $user['group'] = null;

            foreach ($groups as $row) {
                $user['group'] = $row;
            }

            // Get user balance
            $balance = $this->_em->getRepository(CoreUserTransactions::class)->getBalance($user['username']);
            $user['credits'] = array_map(function ($row) {
                return $row === null ? 0 : $row;
            }, array_pop($balance));

            return $user;
        }, $users);

        return $users;
    }

    public function queueEmail($params)
    {
        $emailEntry = (new CoreSendemail())
            ->setTemplate($params['template'])
            ->setDateAdded(new \DateTime())
            ->setFrom($params['from'])
            ->setTo($params['email'])
            ->setCc($params['cc'])
            ->setSubject($params['subject'])
            ->setStatus('new');
        $this->_em->persist($emailEntry);
    }

    /**
     * Gets the report data that contains every available information
     *
     * @return array
     */
    public function getReportData()
    {
        $coreUserTransactions = $this->_em->getRepository(CoreUserTransactions::class);

        $result = $this
            ->createQueryBuilder('U')
            ->select('U.id, U.username, U.firstName, U.lastName, U.surname, U.email, U.displayName,
            U.enabled AS status, U.profileFulfilled, R.role AS roleName,
            C.cedisId AS cedisId, C.namesCedis as cedisName, C.street AS cedisStreet,
            C.extNumber AS cedisExtNumber, C.intNumber AS cedisIntNumber, C.location AS cedisLocation,
            C.reference AS cedisReference, C.city AS cedisCity, C.state AS cedisState, C.zipCode AS cedisZipCode,
            A.street, A.extNumber, A.intNumber, A.location, A.reference, A.city, A.town, A.state, A.zipCode,
            U.telephone1, U.telephone2, U.mobile, U.createdAt, U.modifiedAt')
            ->innerJoin('U.role', 'R')
            ->leftJoin('U.cedis', 'C')
            ->leftJoin('U.addresses', 'A')
            ->orderBy('U.id', 'ASC')
            ->getQuery()
            ->getArrayResult();

        $data = array();

        foreach ($result as $row) {
            $balance = $coreUserTransactions->getBalance($row['username']);

            $credits = array_map(function ($val) {
                return $val === null ? 0 : $val;
            }, array_pop($balance));

            foreach ($credits as $k => $v) {
                $row['credits_' . $k] = $v;
            }

            $data[] = $row;
        }

        return $data;
    }

    /**
     * Gets total users including: active, blocked
     * @return array
     */
    public function getTotals()
    {
        $results = $this
            ->createQueryBuilder('U')
            ->select('COUNT(U.id) AS total')
            ->addSelect('SUM(CASE WHEN U.enabled = 1 THEN 1 ELSE 0 END) AS active')
            ->addSelect('SUM(CASE WHEN U.enabled = 0 THEN 1 ELSE 0 END) AS blocked')
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);

        return array_map(function ($value) {
            return intval($value);
        }, $results);
    }
    
    public function getEvent($key)
    {
        $config = $this->service->get('config');
        if(isset($config['application-repository-oauthusersrepository'])){
            $event = $config['application-repository-oauthusersrepository']
                    ['events'][$key];
            return new $event($this->service);            
        }
        return false;
    } 
    
    public function setEvent($service)
    {
        $this->service = $service;
    }        
}
