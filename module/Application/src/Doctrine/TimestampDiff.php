<?php

namespace Application\Doctrine;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

/**
 * TIMESTAMPDIFF MySQL function implementation
 * 
 * @author Hector Ceron <hector.ceron@adventa.mx> 
 */
class TimestampDiff extends FunctionNode
{
    /** @var string */
    public $firstDatetimeExpression = null;

    /** @var string */   
    public $secondDatetimeExpression = null;

    /** @var string */
    public $unit = null;

    /**
     * 
     * @param \Doctrine\ORM\Query\Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $parser->match(Lexer::T_IDENTIFIER);
        $lexer = $parser->getLexer();
        $this->unit = $lexer->token['value'];
        $parser->match(Lexer::T_COMMA);
        $this->firstDatetimeExpression = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->secondDatetimeExpression = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
    
    /**
     * 
     * @param \Doctrine\ORM\Query\SqlWalker $sql_walker
     * @return string
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sql_walker)
    {
        return sprintf(
            'TIMESTAMPDIFF(%s, %s, %s)',
            $this->unit,
            $this->firstDatetimeExpression->dispatch($sql_walker),
            $this->secondDatetimeExpression->dispatch($sql_walker)
        );
    }
}
