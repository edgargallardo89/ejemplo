<?php
    /*
    * To change this license header, choose License Headers in Project Properties.
    * To change this template file, choose Tools | Templates
    * and open the template in the editor.
    */
namespace Application\Template;
use Zend\View\Renderer\PhpRenderer;

use Zend\View\Model\ViewModel;

use Zend\ServiceManager\ServiceManager;



class Render{





    /**

     * @var ServiceManager

     */

    protected $serviceManager; 

    

    /**

     * Obtiene Service Manager

     * 

     * @return type

     */

    public function getServiceManager(){

        return $this->serviceManager;

    }



    /**

     * Inyecta Service Manager

     * 

     * @param \Zend\ServiceManager\ServiceManager $serviceManager

     * @return \Cscore\Service\Cmf

     */

    public function setServiceManager(ServiceManager $serviceManager){

        $this->serviceManager = $serviceManager;

        return $this;

    }

    

    /**

     * 

     * @param type $_vars

     */

    public function getContent($_vars,$template){

        $config = $this->getServiceManager()->get('config');

        $basePath = $config['view_manager']['template_path_stack'][2];

        $renderer = new PhpRenderer();

        $renderer->resolver()->addPath($basePath);

        $renderer->setHelperPluginManager($this->getServiceManager()->get('ViewHelperManager'));

        $model = new ViewModel();

        $model->setVariable('items' , $_vars);

        $model->setTemplate('application/index/templates/'.$template); 

        $textContent = $renderer->render($model);

        $filename = $config['paramsemail']['path'].'/email/'.$_vars['template'];

        file_put_contents($filename, $textContent);

    }

    

    /**

     * 

     * @return type

     */

    public function getParams($subject)

    {

        $config = $this->getServiceManager()->get('config');

        return  [

            'template'=>  $this->getNameFilehash(),

            'from'=>$config['paramsemail']['setting']['connection_config']['username'],

            'cc'=>'',

            'subject'=>$subject,

            

        ];

    }

    

    /**

     * 

     * @return type

     */

    private function getNameFilehash(){

        return hash('sha1', uniqid(time(),true)).'.phtml';

    }    

}