<?php

namespace Application\Service;

use Application\Entity\CoreOrderProducts;
use Application\Entity\CoreOrders;
use Application\Entity\CoreUserTransactions;
use Application\Entity\OauthUsers;
use Application\Repository\CoreOrderProductsRepository;
use Application\Repository\CoreOrdersRepository;
use Application\Repository\CoreUserTransactionsRepository;
use Application\Repository\OauthUsersRepository;
use Doctrine\ORM\EntityManager;

class DashboardService
{
    /**
     * @var CoreOrdersRepository
     */
    private $coreOrdersRepository;

    /**
     * @var CoreOrderProductsRepository
     */
    private $coreOrderProductsRepository;

    /**
     * @var OauthUsersRepository
     */
    private $oauthUsersRepository;

    /**
     * @var CoreUserTransactionsRepository
     */
    private $coreUserTransactionsRepository;

    /**
     * DashboardService constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct($entityManager)
    {
        $this->coreOrdersRepository = $entityManager->getRepository(CoreOrders::class);
        $this->coreOrderProductsRepository = $entityManager->getRepository(CoreOrderProducts::class);
        $this->oauthUsersRepository = $entityManager->getRepository(OauthUsers::class);
        $this->coreUserTransactionsRepository = $entityManager->getRepository(CoreUserTransactions::class);
    }

    /**
     * Gets all the dashboard current information
     *
     * @return mixed
     * @throws \Exception
     */
    public function getInformation()
    {
        $data['orders_total'] = $this->coreOrdersRepository->getTotals(1);
        $data['points_total'] = $this->coreUserTransactionsRepository->getTotalPoints();
        $data['orders_category'] = $this->coreOrderProductsRepository->getTotalsByCategory();
        $data['orders_month'] = $this->coreOrdersRepository->getTotalsByMonth();
        $data['products_top'] = $this->coreOrderProductsRepository->getTopSelling(5);
        $data['users_total'] = $this->oauthUsersRepository->getTotals();

        return $data;
    }
}