<?php

namespace Application\Service;

use Doctrine\ORM\EntityManager;

interface ReportInterface
{
    /**
     * Init method
     *
     * @param EntityManager $em
     * @param array $config
     * @param string $path
     * @return void
     */
    public function initialize($em, $config, $path);

    /**
     * Generation method
     *
     * @return void
     */
    public function generate();
}