<?php

namespace Application\Service;

use Application\Entity\CoreOrders;
use Application\Repository\CoreOrdersRepository;
use Application\Repository\CoreReportsRepository;
use Doctrine\ORM\EntityManager;

class SLReport implements ReportInterface
{
    /**
     * @var CoreOrdersRepository
     */
    private $coreOrdersRepository;

    /**
     * Report's name
     *
     * @var string
     */
    private $reportName;

    /**
     * Available columns
     * @var array
     */
    private $reportColumns;

    /**
     * Application path
     *
     * @var string
     */
    private $path;

    /**
     * Init method
     *
     * @param EntityManager $em
     * @param array $config
     * @param string $path
     * @return void
     */
    public function initialize($em, $config, $path)
    {
        $this->coreOrdersRepository = $em->getRepository(CoreOrders::class);
        $this->reportName = $config['id'];
        $this->path = $path;

        $columns = array();

        array_walk($config['columns'], function ($row) use (&$columns) {
            $columns[] = $row['column_name'];
        });

        $this->reportColumns = $columns;
    }

    /**
     * Generation method
     *
     * @return void
     */
    public function generate()
    {
        $reportData = $this->coreOrdersRepository->getReportData();

        $temporary = $this->path . CoreReportsRepository::UPLOAD_ROUTE . $this->reportName . '_' . time() . '.csv';
        $destination = $this->path . CoreReportsRepository::UPLOAD_ROUTE . $this->reportName . '.csv';

        $fp = fopen($temporary, 'w+');

        // Headers
        fputcsv($fp, $this->reportColumns);

        // Body
        foreach ($reportData as $row) {
            $data[] = $this->getValue($row['orderId']);
            $data[] = is_null($row['deletedAt']) ? 'Completado' : 'Cancelado';
            $data[] = $row['createdAt'] != null ? $row['createdAt']->format('Y-m-d H:i:s') : 'N/D';
            $data[] = $this->getValue($row['userId']);
            $data[] = $this->getValue($row['username']);
            $data[] = $this->getValue($row['firstName']);
            $data[] = $this->getValue($row['lastName']);
            $data[] = $this->getValue($row['surname']);
            $data[] = $this->getValue($row['email']);
            $data[] = $this->getValue($row['displayName']);
            $data[] = $row['status'] == '1' ? 'Activo' : 'Bloqueado';
            $data[] = $row['profileFulfilled'] == '1' ? 'Si' : 'No';
            $data[] = $this->getValue($row['roleName']);
            $data[] = "N/D";
            $data[] = $this->getValue($row['sku']);
            $data[] = $this->getValue($row['title']);
            $data[] = $this->getValue($row['quantity']);
            $data[] = $this->getValue($row['price']);
            $data[] = $row['price'] * $row['quantity'];
            $data[] = $this->getValue($row['realPrice']);
            $data[] = $row['realPrice'] * $row['quantity'];
            $data[] = $this->getValue($row['productCategory']);
            $data[] = $this->getValue($row['cedisId']);
            $data[] = $this->getValue($row['cedisName']);
            $data[] = $this->getValue($row['street']);
            $data[] = $this->getValue($row['extNumber']);
            $data[] = $this->getValue($row['intNumber']);
            $data[] = $this->getValue($row['location']);
            $data[] = $this->getValue($row['reference']);
            $data[] = $this->getValue($row['city']);
            $data[] = $this->getValue($row['state']);
            $data[] = $this->getValue($row['zipCode']);
            $data[] = $this->getValue($row['telephone1']);
            $data[] = $this->getValue($row['telephone2']);
            $data[] = $this->getValue($row['mobile']);

            fputcsv($fp, $data);

            unset($data);
        }

        fclose($fp);
        rename($temporary, $destination);
    }

    /**
     * Returns the encoded value or 'N/D' when value is empty
     *
     * @param $value
     * @return string
     */
    private function getValue($value)
    {
        return $value == null || empty($value) ? 'N/D' : $value;
    }
}