<?php

namespace Application\Service;

use Application\Entity\CoreConfigs;
use Application\Entity\OauthUsers;
use Application\Repository\CoreConfigsRepository;
use Application\Repository\CoreReportsRepository;
use Application\Repository\OauthUsersRepository;
use Doctrine\ORM\EntityManager;

class UsersReport implements ReportInterface
{
    /**
     * @var OauthUsersRepository
     */
    private $oauthUsersRepository;

    /**
     * @var CoreConfigsRepository
     */
    private $coreConfigRepository;

    /**
     * Report's name
     *
     * @var string
     */
    private $reportName;

    /**
     * Available columns
     * @var array
     */
    private $reportColumns;

    /**
     * Application path
     *
     * @var string
     */
    private $path;

    /**
     * Init method
     *
     * @param EntityManager $em
     * @param array $config
     * @param string $path
     * @return void
     */
    public function initialize($em, $config, $path)
    {
        $this->oauthUsersRepository = $em->getRepository(OauthUsers::class);
        $this->coreConfigRepository = $em->getRepository(CoreConfigs::class);
        $this->reportName = $config['id'];
        $this->path = $path;

        $columns = array();

        array_walk($config['columns'], function ($row) use (&$columns) {
            $columns[] = $row['column_name'];
        });

        $this->reportColumns = $columns;
    }

    /**
     * Generation method
     *
     * @return void
     */
    public function generate()
    {
        $reportData = $this->oauthUsersRepository->getReportData();
        $deliveryMode = $this->coreConfigRepository->findByKey('checkout.delivery.mode');
        $mode = $deliveryMode['value'];

        $temporary = $this->path . CoreReportsRepository::UPLOAD_ROUTE . $this->reportName . '_' . time() . '.csv';
        $destination = $this->path . CoreReportsRepository::UPLOAD_ROUTE . $this->reportName . '.csv';

        $fp = fopen($temporary, 'w+');

        // Headers
        fputcsv($fp, $this->reportColumns);

        // Body
        foreach ($reportData as $row) {
            $data[] = $this->getValue($row['id']);
            $data[] = $this->getValue($row['username']);
            $data[] = $this->getValue($row['firstName']);
            $data[] = $this->getValue($row['lastName']);
            $data[] = $this->getValue($row['surname']);
            $data[] = $this->getValue($row['email']);
            $data[] = $this->getValue($row['displayName']);
            $data[] = $row['status'] == '1' ? 'Activo' : 'Bloqueado';
            $data[] = $row['profileFulfilled'] == '1' ? 'Si' : 'No';
            $data[] = $this->getValue($row['roleName']);
            $data[] = $this->getValue($row['cedisId']);
            $data[] = $this->getValue($row['cedisName']);

            if ($mode == 'cedis') {
                $data[] = $this->getValue($row['cedisStreet']);
                $data[] = $this->getValue($row['cedisExtNumber']);
                $data[] = $this->getValue($row['cedisIntNumber']);
                $data[] = $this->getValue($row['cedisLocation']);
                $data[] = $this->getValue($row['cedisReference']);
                $data[] = $this->getValue($row['cedisCity']);
                $data[] = $this->getValue('N/D');
                $data[] = $this->getValue($row['cedisState']);
                $data[] = $this->getValue($row['cedisZipCode']);
            } else {
                $data[] = $this->getValue($row['street']);
                $data[] = $this->getValue($row['extNumber']);
                $data[] = $this->getValue($row['intNumber']);
                $data[] = $this->getValue($row['location']);
                $data[] = $this->getValue($row['reference']);
                $data[] = $this->getValue($row['city']);
                $data[] = $this->getValue($row['town']);
                $data[] = $this->getValue($row['state']);
                $data[] = $this->getValue($row['zipCode']);
            }

            $data[] = $this->getValue($row['telephone1']);
            $data[] = $this->getValue($row['telephone2']);
            $data[] = $this->getValue($row['mobile']);
            $data[] = $row['createdAt'] != null ? $row['createdAt']->format('Y-m-d H:i:s') : 'N/D';
            $data[] = $row['modifiedAt'] != null ? $row['modifiedAt']->format('Y-m-d H:i:s') : 'N/D';
            $data[] = $row['credits_available'];
            $data[] = $row['credits_earned'];
            $data[] = $row['credits_spent'];
            $data[] = $row['credits_extra'];

            fputcsv($fp, $data);

            unset($data);
        }

        fclose($fp);
        rename($temporary, $destination);
    }

    /**
     * Returns the encoded value or 'N/D' when value is empty
     *
     * @param $value
     * @return string
     */
    private function getValue($value)
    {
        return $value == null || empty($value) ? 'N/D' : $value;
    }
}