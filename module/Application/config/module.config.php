<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014-2016 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use Zend\ServiceManager\Factory\InvokableFactory;

$path=  realpath((__DIR__).'/../../../');
$pathdata=$path.'/data';

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'paramsemail'=>[
        'setting'=>[
            'name'              => 'zit.zitred.com',
            'host'              => 'zit.zitred.com',
            'port'              => 587, // Notice port change for TLS is 587
            'connection_class'  => 'plain',
            'connection_config' => [
                'username' => 'adbox@adventa.mx',
                'password' => 'An=L.mm$H19M',
                'ssl'      => 'tls',
            ],
        ],
        'account'=>[
             'from'=>'adbox@adventa.mx',
             'fromname'=>'adbox@adventa.mx',
             'to'=>'',
             'cc'=>'',
             'subject'=>'Recuperar Contraseña',
             'body'=>''
        ],
        'path'=>$pathdata,
        'nametemplate'=>'application/index/templates/template.phtml'
    ],
    'path'=>$path,
    'reports' => [
        'users' => [
            'id' => 'users',
            'name' => 'Reporte de Usuarios',
            'icon' => 'icon-account-multiple-outline',
            'class' => \Application\Service\UsersReport::class,
            'ttl' => 300,
            'layout' => [
                'username',
                'first_name',
                'email',
                'last_name',
                'surname',
                'role_name',
                'status',
                'street',
                'cedis_id',
                'cedis_name',
                'ext_number',
                'int_number',
                'location',
                'reference',
                'city',
                'state',
                'zip_code',
                'telephone1',
                'mobile',
                'created_at',
                'credits_available',
                'credits_earned',
                'credits_spent',
                'credits_extra',
            ],
            'columns' => [
                [
                    'column_name' => 'id',
                    'display_name' => 'ID Usuario'
                ],
                [
                    'column_name' => 'username',
                    'display_name' => 'Usuario'
                ],
                [
                    'column_name' => 'first_name',
                    'display_name' => 'Nombre'
                ],
                [
                    'column_name' => 'last_name',
                    'display_name' => 'Apellido Paterno'
                ],
                [
                    'column_name' => 'surname',
                    'display_name' => 'Apellido Materno'
                ],
                [
                    'column_name' => 'email',
                    'display_name' => 'E-mail'
                ],
                [
                    'column_name' => 'display_name',
                    'display_name' => 'Nombre Completo'
                ],
                [
                    'column_name' => 'status',
                    'display_name' => 'Estatus'
                ],
                [
                    'column_name' => 'profile_fulfilled',
                    'display_name' => 'Perfil Completado'
                ],
                [
                    'column_name' => 'role_name',
                    'display_name' => 'Perfil'
                ],
                [
                    'column_name' => 'cedis_id',
                    'display_name' => 'ID Cedis'
                ],
                [
                    'column_name' => 'cedis_name',
                    'display_name' => 'Nombre Cedis'
                ],
                [
                    'column_name' => 'street',
                    'display_name' => 'Calle'
                ],
                [
                    'column_name' => 'ext_number',
                    'display_name' => 'N. Exterior'
                ],
                [
                    'column_name' => 'int_number',
                    'display_name' => 'N. Interior'
                ],
                [
                    'column_name' => 'location',
                    'display_name' => 'Colonia'
                ],
                [
                    'column_name' => 'reference',
                    'display_name' => 'Referencia'
                ],
                [
                    'column_name' => 'city',
                    'display_name' => 'Ciudad'
                ],
                [
                    'column_name' => 'town',
                    'display_name' => 'Municipio'
                ],
                [
                    'column_name' => 'state',
                    'display_name' => 'Estado'
                ],
                [
                    'column_name' => 'zip_code',
                    'display_name' => 'C.P.'
                ],
                [
                    'column_name' => 'telephone1',
                    'display_name' => 'Telefono'
                ],
                [
                    'column_name' => 'telephone2',
                    'display_name' => 'Telefono otro'
                ],
                [
                    'column_name' => 'mobile',
                    'display_name' => 'Celular'
                ],
                [
                    'column_name' => 'created_at',
                    'display_name' => 'Fecha de registro'
                ],
                [
                    'column_name' => 'modified_at',
                    'display_name' => 'Fecha de modificacion'
                ],
                [
                    'column_name' => 'credits_available',
                    'display_name' => 'Puntos disponibles'
                ],
                [
                    'column_name' => 'credits_earned',
                    'display_name' => 'Puntos acumulados'
                ],
                [
                    'column_name' => 'credits_spent',
                    'display_name' => 'Puntos canjeados'
                ],
                [
                    'column_name' => 'credits_extra',
                    'display_name' => 'Puntos extra'
                ],
            ],
        ],
        'shipping_list' => [
            'id' => 'shipping_list',
            'name' => 'Reporte de Canjes',
            'icon' => 'icon-cart-outline',
            'class' => \Application\Service\SLReport::class,
            'ttl' => 300,
            'layout' => [
                'order_number',
                'order_status',
                'order_date',
                'display_name',
                'username',
                'status',
                'pmr',
                'sku',
                'description',
                'category',
                'quantity',
                'price',
                'total',
                'price_pesos',
                'total_pesos',
                'cedis_id',
                'cedis_name',
                'street',
                'ext_number',
                'int_number',
                'location',
                'reference',
                'city',
                'state',
                'zip_code',
                'telephone1',
                'mobile',
            ],
            'columns' => [
                [
                    'column_name' => 'order_number',
                    'display_name' => 'No. de canje'
                ],
                [
                    'column_name' => 'order_status',
                    'display_name' => 'Estatus del pedido'
                ],
                [
                    'column_name' => 'order_date',
                    'display_name' => 'Fecha de canje'
                ],
                [
                    'column_name' => 'id',
                    'display_name' => 'ID Usuario'
                ],
                [
                    'column_name' => 'username',
                    'display_name' => 'Usuario'
                ],
                [
                    'column_name' => 'first_name',
                    'display_name' => 'Nombre'
                ],
                [
                    'column_name' => 'last_name',
                    'display_name' => 'Apellido Paterno'
                ],
                [
                    'column_name' => 'surname',
                    'display_name' => 'Apellido Materno'
                ],
                [
                    'column_name' => 'email',
                    'display_name' => 'E-mail'
                ],
                [
                    'column_name' => 'display_name',
                    'display_name' => 'Nombre Completo'
                ],
                [
                    'column_name' => 'status',
                    'display_name' => 'Estatus'
                ],
                [
                    'column_name' => 'profile_fulfilled',
                    'display_name' => 'Perfil Completado'
                ],
                [
                    'column_name' => 'role_name',
                    'display_name' => 'Perfil'
                ],
                [
                    'column_name' => 'pmr',
                    'display_name' => 'Codigo PMR'
                ],
                [
                    'column_name' => 'sku',
                    'display_name' => 'Codigo CU'
                ],
                [
                    'column_name' => 'description',
                    'display_name' => 'Descripcion del articulo'
                ],
                [
                    'column_name' => 'quantity',
                    'display_name' => 'Cantidad'
                ],
                [
                    'column_name' => 'price',
                    'display_name' => 'Precio unitario (en puntos)'
                ],
                [
                    'column_name' => 'total',
                    'display_name' => 'Precio total (en puntos)'
                ],
                [
                    'column_name' => 'price_pesos',
                    'display_name' => 'Precio unitario (en pesos)'
                ],
                [
                    'column_name' => 'total_pesos',
                    'display_name' => 'Precio total (en pesos)'
                ],
                [
                    'column_name' => 'category',
                    'display_name' => 'Categoria'
                ],
                [
                    'column_name' => 'cedis_id',
                    'display_name' => 'ID Cedis'
                ],
                [
                    'column_name' => 'cedis_name',
                    'display_name' => 'Nombre Cedis'
                ],
                [
                    'column_name' => 'street',
                    'display_name' => 'Calle'
                ],
                [
                    'column_name' => 'ext_number',
                    'display_name' => 'N. Exterior'
                ],
                [
                    'column_name' => 'int_number',
                    'display_name' => 'N. Interior'
                ],
                [
                    'column_name' => 'location',
                    'display_name' => 'Colonia'
                ],
                [
                    'column_name' => 'reference',
                    'display_name' => 'Referencia'
                ],
                [
                    'column_name' => 'city',
                    'display_name' => 'Ciudad'
                ],
                [
                    'column_name' => 'state',
                    'display_name' => 'Estado'
                ],
                [
                    'column_name' => 'zip_code',
                    'display_name' => 'C.P.'
                ],
                [
                    'column_name' => 'telephone1',
                    'display_name' => 'Telefono'
                ],
                [
                    'column_name' => 'telephone2',
                    'display_name' => 'Telefono otro'
                ],
                [
                    'column_name' => 'mobile',
                    'display_name' => 'Celular'
                ],
            ],
        ],
    ],
];
