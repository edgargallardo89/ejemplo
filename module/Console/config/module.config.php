<?php

namespace Console;

return [
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => Factory\ConsoleFactory::class,
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'reports-create' => [
                    'options' => [
                        'route' => 'reports create [<reportName>]',
                        'defaults' => [
                            'controller' => Controller\IndexController::class,
                            'action' => 'create',
                        ],
                    ],
                ],
            ],
        ],
    ],
];