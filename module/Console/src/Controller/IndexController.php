<?php

namespace Console\Controller;

use Application\Repository\CoreReportsRepository;
use Doctrine\ORM\EntityManager;
use RuntimeException;
use Zend\Console\Request as ConsoleRequest;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceManager;

class IndexController extends AbstractActionController implements EventManagerAwareInterface
{
    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var array
     */
    private $config;

    /**
     * IndexController constructor.
     * @param $serviceManager
     */
    public function __construct($serviceManager)
    {
        $this->serviceManager = $serviceManager;
        $this->config = $this->serviceManager->get('config');
        $this->entityManager = $this->serviceManager->get(EntityManager::class);
    }

    public function createAction()
    {
        // Validate request
        $request = $this->getRequest();

        if (!$request instanceof ConsoleRequest) {
            throw new RuntimeException('You can only use this action from a console');
        }

        // Get parameters
        $reportName = $request->getParam('reportName');

        // Configuration list
        if (!isset($this->config['reportsCcp'])) {
             $reportsList = $this->config['reports'];
        }
        else
        {
            $reportsList = $this->config['reportsCcp'];
        }       

        // Define which ones to generate
        if ($reportName === null) {
            // All available reports
            foreach ($reportsList as $report) {
                $this->generateSingle($report);
            }
        } else {
            // Report configuration found
            if (!isset($reportsList[$reportName])) {
                return "Report [$reportName] not found...\n";
            } else {
                $this->generateSingle($reportsList[$reportName]);
            }
        }

        return "";
    }

    /**
     * @param array $reportConfiguration
     */
    private function generateSingle($reportConfiguration)
    {
        // Main report properties
        $reportName = $reportConfiguration['id'];
        $reportClass = $reportConfiguration['class'];
        $reportTtl = $reportConfiguration['ttl'];

        // Last modification
        $destination = $this->config['path'] . CoreReportsRepository::UPLOAD_ROUTE . $reportName . '.csv';;
        $fileModifiedAt = @filemtime($destination);

        // Error (file does not exists)
        $fileModifiedAt = $fileModifiedAt === false ? 0 : $fileModifiedAt;

        // Current time vs maximum valid period
        $currentTime = (new \DateTime())->getTimestamp();
        $maxValidTime = $fileModifiedAt + $reportTtl;

        // Regenerate only if current date is later than the maximum valid date
        if ($currentTime > $maxValidTime) {
            // Instance object class
            $reportObject = new $reportClass();
            
            // Report generation
            $reportObject->initialize($this->entityManager, $reportConfiguration, $this->config['path']);
            $reportObject->generate();

            echo "Report [$reportName] done...\n";
        } else {
            echo "Report [$reportName] skipped...\n";
        }
    }
}