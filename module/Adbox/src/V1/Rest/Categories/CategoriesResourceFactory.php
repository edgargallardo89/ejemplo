<?php
namespace Adbox\V1\Rest\Categories;

use Adteam\Core\Common\ViewHelper;
use Doctrine\ORM\EntityManager;

class CategoriesResourceFactory
{
    public function __invoke($services)
    {
        $em = $services->get(EntityManager::class);
        
        return new CategoriesResource($em, new ViewHelper($services));
    }
}
