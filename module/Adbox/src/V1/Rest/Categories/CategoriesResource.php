<?php
namespace Adbox\V1\Rest\Categories;

use Adteam\Core\Common\ViewHelper;
use Application\Entity\CoreProductCategories;
use Doctrine\ORM\EntityManager;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class CategoriesResource extends AbstractResourceListener
{
    /** @var \Doctrine\ORM\EntityManager **/
    private $em;
    
    /** @var \Adteam\Core\Common\ViewHelper **/
    private $serverUrl;

    /**
     *
     * @param EntityManager $em
     * @param \Adteam\Core\Common\ViewHelper $serverUrl
     */
    public function __construct(EntityManager $em, ViewHelper $serverUrl)
    {
        $this->serverUrl = $serverUrl;
        $this->em = $em;
    }


    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $identity = $this->getIdentity()->getAuthenticationIdentity();
        $username = $identity["user_id"];        
        $serverUrl = $this->serverUrl->__invoke('/img/categories/');
        
        return $this->em->getRepository(CoreProductCategories::class)->getCategories($username, $serverUrl);
//        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        $data = (array)$data;
        unset($data['file']);
        $inputFilter = $this->getInputFilter();
        $data['fileName'] = pathinfo($inputFilter->getValue('file')["tmp_name"])['basename'];
        $this->em->getRepository(CoreProductCategories::class)->put($id, $data);

        return [ "message" => "success" ];
//        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
