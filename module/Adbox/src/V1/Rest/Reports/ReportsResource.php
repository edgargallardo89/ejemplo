<?php
namespace Adbox\V1\Rest\Reports;

use Application\Repository\CoreReportsRepository;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ReportsResource extends AbstractResourceListener
{
    /**
     * @var CoreReportsRepository
     */
    private $coreReportsRepository;

    /**
     * ReportsResource constructor.
     * @param CoreReportsRepository $coreReportsRepository
     */
    public function __construct($coreReportsRepository)
    {
        $this->coreReportsRepository = $coreReportsRepository;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try {
            $identityData = $this->getIdentity()->getAuthenticationIdentity();
            return $this->coreReportsRepository->generate($identityData['user_id'], $data);
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case 'Configuration_Not_Set':
                case 'Missing_Configuration':
                case 'OauthUsers_Not_Set':
                case 'Invalid_Report_Request':
                case 'No_Selected_Columns':
                case 'Malformed_Columns_Request':
                case 'Report_Already_Processing':
                    return new ApiProblem(422, $e->getMessage());
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        try {
            return $this->coreReportsRepository->getConfiguration();
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case 'Configuration_Not_Set':
                case 'Missing_Configuration':
                    return new ApiProblem(422, $e->getMessage());
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
