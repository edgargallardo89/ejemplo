<?php
namespace Adbox\V1\Rest\Reports;

use Adbox\Service\Importcsv;
use Application\Entity\CoreReports;
use Application\Entity\OauthUsers;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;

class ReportsResourceFactory
{
    /**
     * @param ServiceManager $services
     * @return ReportsResource
     */
    public function __invoke($services)
    {
        // Get repositories
        $entityManager = $services->get(EntityManager::class);
        $coreReportsRepository = $entityManager->getRepository(CoreReports::class);
        $oauthUsersRepository = $entityManager->getRepository(OauthUsers::class);

        // Get service classes
        $config = $services->get('config');

        // Set classes
        $coreReportsRepository->setConfig($config);
        $coreReportsRepository->setOauthUsersRepository($oauthUsersRepository);

        return new ReportsResource($coreReportsRepository);
    }
}
