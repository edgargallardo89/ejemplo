<?php
namespace Adbox\V1\Rest\Banners;

use Adteam\Core\Common\ViewHelper;
use Application\Entity\CoreBanners;
use Doctrine\ORM\EntityManager;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\ServiceManager\ServiceManager;
use Adteam\Core\Banners\Roles\Component;

class BannersResource extends AbstractResourceListener
{
    /** @var EntityManager $em **/
    private $em;
    
    /** @var \Adteam\Core\Common\ViewHelper $serverUrl **/
    private $serverUrl;

    protected $component;
    
    /**
     * 
     * @param EntityManager $em
     * @param \Adteam\Core\Common\ViewHelper $serverUrl
     */
    public function __construct(EntityManager $em, ViewHelper $serverUrl,ServiceManager $services)
    {
        $this->em = $em;
        $this->serverUrl = $serverUrl;
        $this->component = new Component($services);
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
//        $data = (array)$data;
//        unset($data['file']);
//        $inputFilter = $this->getInputFilter();
//        $data['fileName'] = pathinfo($inputFilter->getValue('file')["tmp_name"])['basename'];
//        $this->em->getRepository(CoreBanners::class)->create($data);
//        
//        return [ "message" => "success" ];
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $serverUrl = $this->serverUrl->__invoke('/img/banners/');
        $banners = $this->em->getRepository(CoreBanners::class)->fetchAll($serverUrl);
        return $this->component->getBanners($banners,$serverUrl);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        try {
            $this->em->getRepository(CoreBanners::class)->patch($id);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return new ApiProblem(404, 'Invalid_Banner');
        } catch (\Exception $e) {
            return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. '.$e->getMessage());
        }
        
        return [ "message" => "success" ];        
//        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {                
        try {
            $data = (array)$data;
            unset($data['file']);
            $inputFilter = $this->getInputFilter();
            $data['fileName'] = !empty($inputFilter->getValue('file')) ? pathinfo($inputFilter->getValue('file')["tmp_name"])['basename'] : null;
            $data['id'] = $id;
            $this->em->getRepository(CoreBanners::class)->put($id, $data);
        } catch (\Exception $e) {
            switch($e->getMessage()) {
                case (true  === (bool)preg_match('/Duplicate entry/', $e->getMessage())):
                    return new ApiProblem(422, 'Banner_Exists');
                default:                    
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. '.$e->getMessage());
            }
        }
        
        return [ "message" => "success" ];
//        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
