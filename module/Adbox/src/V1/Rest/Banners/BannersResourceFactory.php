<?php

namespace Adbox\V1\Rest\Banners;

use Adteam\Core\Common\ViewHelper;
use Doctrine\ORM\EntityManager;

class BannersResourceFactory
{
    public function __invoke($services)
    {
        $em = $services->get(EntityManager::class);
        return new BannersResource($em, new ViewHelper($services),$services);
    }
}
