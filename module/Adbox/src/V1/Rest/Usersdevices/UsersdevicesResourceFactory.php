<?php
namespace Adbox\V1\Rest\Usersdevices;

class UsersdevicesResourceFactory
{
    public function __invoke($services)
    {
        return new UsersdevicesResource($services);
    }
}
