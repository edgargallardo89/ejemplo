<?php
namespace Adbox\V1\Rest\Adminpermissionsbuild;

class AdminpermissionsbuildResourceFactory
{
    public function __invoke($services)
    {
        return new AdminpermissionsbuildResource($services);
    }
}
