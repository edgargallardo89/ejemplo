<?php
namespace Adbox\V1\Rest\Usercart;

use Adteam\Core\Common\ViewHelper;
use Application\Entity\CoreUserCart;
use Application\Entity\CoreUserCartItems;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class UsercartResource extends AbstractResourceListener
{
    /** @var ServiceManager $sm **/
    private $sm;

    /** @var EntityManager $em **/
    private $em;

    /** @var \Adteam\Core\Common\ViewHelper **/
    private $serverUrl;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        $this->em = $sm->get(EntityManager::class);
        $this->serverUrl = new ViewHelper($sm);
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        try {
            $identityData = $this->getIdentity()->getAuthenticationIdentity();
            $version = $this->getEvent()->getQueryParam('version');

            if (!isset($version) or !is_numeric($version)) {
                return new ApiProblem(422, 'Missing_Cart_Version');
            }

            $this->em->getRepository(CoreUserCartItems::class)->removeCartItem($identityData['user_id'], $id, $version);
        } catch (\Doctrine\ORM\OptimisticLockException $e) {
            return new ApiProblem(422, 'Version_Mismatch');
        } catch (\Exception $e) {
            $message = $e->getMessage();

            switch ($message) {
                case (true === (bool)preg_match('/Duplicate entry/', $message)):
                    return new ApiProblem(422, 'Version_Mismatch');
                case 'Invalid_Cart_Item':
                    return new ApiProblem(422, 'Invalid_Cart_Item');
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. '.$e->getMessage());
            }
        }

        return true; //Returns 204 No Content
//        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        try {
            $identityData = $this->getIdentity()->getAuthenticationIdentity();
            $version = $this->getEvent()->getQueryParam('version');

            if (!isset($version) or !is_numeric($version)) {
                return new ApiProblem(422, 'Missing_Cart_Version');
            }

            $this->em->getRepository(CoreUserCart::class)->clearCart($identityData['user_id'], $version);
        } catch (\Doctrine\ORM\OptimisticLockException $e) {
            return new ApiProblem(422, 'Version_Mismatch');
        } catch (\Exception $e) {
            $message = $e->getMessage();

            switch ($message) {
                case (true === (bool)preg_match('/Duplicate entry/', $message)):
                    return new ApiProblem(422, 'Version_Mismatch');
                case 'Invalid_Cart_Item':
                    return new ApiProblem(422, 'Invalid_Cart_Item');
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. '.$e->getMessage());
            }
        }

        return true; //Returns 204 No Content
//        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        /** @todo User validation **/
        $identityData = $this->getIdentity()->getAuthenticationIdentity();
        $serverUrl = $this->serverUrl->__invoke('/img/');
        return $this->em->getRepository(CoreUserCart::class)->getCart($identityData['user_id'],$serverUrl);
//        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        $identityData = $this->getIdentity()->getAuthenticationIdentity();
        $version = $this->getEvent()->getQueryParam('version');

        if (!isset($version) or !is_numeric($version)) {
            return new ApiProblem(422, 'Missing_Cart_Version');
        }

        //Override version value from query parameters
        $data->version = $version;

        try {
            $this->em->getRepository(CoreUserCartItems::class)->setCartItem($identityData['user_id'], $id, (array)$data);
        } catch (\Doctrine\ORM\OptimisticLockException $e) {
            return new ApiProblem(422, 'Version_Mismatch');
        } catch (\Exception $e) {
            $message = $e->getMessage();

            switch ($message) {
                case (true === (bool)preg_match('/Duplicate entry/', $message)):
                    return new ApiProblem(422, 'Version_Mismatch');
                case 'Invalid_Product':
                    return new ApiProblem(422, 'Invalid_Product');
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. '.$e->getMessage());
            }
        }

        return ['message' => 'success'];
    }
}
