<?php
namespace Adbox\V1\Rest\Usercart;

class UsercartResourceFactory
{
    public function __invoke($services)
    {
        return new UsercartResource($services);
    }
}
