<?php
namespace Adbox\V1\Rest\Adminusersdelivery;

use Application\Entity\CoreUsers;
use Application\Entity\OauthUsers;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Zend\ServiceManager\ServiceManager;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AdminusersdeliveryResource extends AbstractResourceListener
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    private $sm;

    /**
     * AdminusersdeliveryResource constructor.
     */
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        $this->em = $sm->get(EntityManager::class);
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        try {
            $oauthUser = $this->em->getRepository(OauthUsers::class)->findOneBy(['id' => $id]);

            if ($oauthUser === null) {
                throw new NoResultException();
            }

            return $this->em->getRepository(CoreUsers::class)->editDelivery($oauthUser->getUsername(), $data);
        } catch (NoResultException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case 'Invalid_Configuration':
                    return new ApiProblem(406, 'La configuración checkout.delivery.mode no es válida');
                case 'Address_Non_Editable':
                    return new ApiProblem(406, 'La dirección no es editable');
                case 'Invalid_Cedis':
                    return new ApiProblem(422, 'El cedis no existe en la base de datos');
                case 'Missing_Cedis_Data':
                    return new ApiProblem(422, 'La información del cedis no existe en el payload');
                case 'Missing_Address_Data':
                    return new ApiProblem(422, 'La información de la dirección no existe en el payload');
                case 'Invalid_Cedis_Data':
                    return new ApiProblem(422, 'La información del cedis no es correcta en el payload');
                case 'Invalid_Address_Data':
                    return new ApiProblem(422, 'La información de la dirección no es correcta en el payload');
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }
}
