<?php
namespace Adbox\V1\Rest\Adminusersdelivery;

class AdminusersdeliveryResourceFactory
{
    public function __invoke($services)
    {
        return new AdminusersdeliveryResource($services);
    }
}
