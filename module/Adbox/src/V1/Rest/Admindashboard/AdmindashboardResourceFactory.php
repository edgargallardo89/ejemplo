<?php
namespace Adbox\V1\Rest\Admindashboard;

use Application\Service\DashboardService;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;

class AdmindashboardResourceFactory
{
    /**
     * @param ServiceManager $services
     * @return AdmindashboardResource
     */
    public function __invoke($services)
    {
        $entityManager = $services->get(EntityManager::class);
        $dashboardService = new DashboardService($entityManager);

        return new AdmindashboardResource($dashboardService);
    }
}
