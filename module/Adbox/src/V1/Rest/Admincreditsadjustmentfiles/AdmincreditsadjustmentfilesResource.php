<?php
namespace Adbox\V1\Rest\Admincreditsadjustmentfiles;

use Adteam\Core\Common\ViewHelper;
use Application\Entity\CoreFileUploads;
use Doctrine\ORM\EntityManager;
use Zend\Http\Headers;
use Zend\Http\Response\Stream;
use Zend\ServiceManager\ServiceManager;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;


class AdmincreditsadjustmentfilesResource extends AbstractResourceListener
{    
    /** @var \Doctrine\ORM\EntityManager **/
    protected $em;
        
    /** @var \Adteam\Core\Common\ViewHelper **/
    protected $serverUrl;
    
    /** @var array **/
    protected $config;

    /**
     * 
     * @param ServiceManager $services
     */
    public function __construct(ServiceManager $services)
    {
        $this->em = $services->get(EntityManager::class);
        $this->serverUrl = new ViewHelper($services);
        $this->config = $services->get('config');
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try {
            $item = $this->em->getRepository(CoreFileUploads::class)->getQbItem($id);
            $filename = $this->config['path'].'/public/csv/'.$item['fileName'];            
            $fp = fopen($filename, "r+");            
            $response = new Stream();
            $headers = new Headers();
            
            if (!file_exists($filename)) {
                throw new \Exception('Not Found');
            }
                        
            $response->setStream($fp);
            $response->setStatusCode(200);
            //$response->setStreamName('a.csv');
            
            $headers->addHeaders(
                array(
                    'Content-Type' => 'text/csv',
                    'Content-Disposition' => 'attachment; filename="'.$item['fileName'].'.log"',
                    'Content-Length' => filesize($filename)
                )
            );

            $response->setHeaders($headers);

            return $response;            
        } catch (\Doctrine\ORM\NoResultException $e) { 
            return new ApiProblem(404, 'Record_Not_Found');
        } catch (\Exception $e) {
            switch($e->getMessage()) {
                case 'Not Found':
                    return new ApiProblem(404, 'File_Not_Found');
                default:
                    return new ApiProblem(500, 'Server Error. '.$e->getMessage());
            }
        }
        
//        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
