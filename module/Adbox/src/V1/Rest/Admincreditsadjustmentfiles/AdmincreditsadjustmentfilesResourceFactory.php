<?php
namespace Adbox\V1\Rest\Admincreditsadjustmentfiles;

class AdmincreditsadjustmentfilesResourceFactory
{
    public function __invoke($services)
    {
        return new AdmincreditsadjustmentfilesResource($services);
    }
}
