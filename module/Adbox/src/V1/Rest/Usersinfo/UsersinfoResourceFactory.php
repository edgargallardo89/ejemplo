<?php
namespace Adbox\V1\Rest\Usersinfo;

class UsersinfoResourceFactory
{
    public function __invoke($services)
    {
        return new UsersinfoResource($services);
    }
}
