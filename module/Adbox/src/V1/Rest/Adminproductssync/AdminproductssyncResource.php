<?php
namespace Adbox\V1\Rest\Adminproductssync;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Doctrine\ORM\EntityManager;
use Application\Entity\CoreMotivaleRequest;
use Application\Entity\CoreUserCartItems;
class AdminproductssyncResource extends AbstractResourceListener
{
    /**
     *
     * @var type 
     */
    private $em;
    
    protected $identity;
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct($services)
    {
        $this->identity = $services->get('authentication')
                ->getIdentity()->getAuthenticationIdentity();
        $this->em = $services->get(EntityManager::class);
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $this->em->
                getRepository(CoreUserCartItems::class)->truncate(); 
        return $this->em->getRepository(CoreMotivaleRequest::class)
                ->create($data,$this->identity);

    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try{
            $result = $this->em->
                    getRepository(CoreMotivaleRequest::class)->fetch($id);  
        } catch (\Exception $ex) {
            return new ApiProblem(500, $ex->getMessage());
        }     
        if($result==='error')
        {
            return new ApiProblem(422, 'Error en catalogo motivale');
        } 
        elseif($result===false)
        {
            return new ApiProblem(404, 'Not Found');
        }          
        return $result;
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
