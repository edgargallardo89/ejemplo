<?php
namespace Adbox\V1\Rest\Adminproductssync;

class AdminproductssyncResourceFactory
{
    public function __invoke($services)
    {
        return new AdminproductssyncResource($services);
    }
    
    
}
