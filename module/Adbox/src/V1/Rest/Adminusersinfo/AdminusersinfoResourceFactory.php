<?php
namespace Adbox\V1\Rest\Adminusersinfo;

class AdminusersinfoResourceFactory
{
    public function __invoke($services)
    {
        return new AdminusersinfoResource($services);
    }
}
