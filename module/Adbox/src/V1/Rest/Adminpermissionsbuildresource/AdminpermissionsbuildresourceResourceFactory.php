<?php
namespace Adbox\V1\Rest\Adminpermissionsbuildresource;

class AdminpermissionsbuildresourceResourceFactory
{
    public function __invoke($services)
    {
        return new AdminpermissionsbuildresourceResource($services);
    }
}
