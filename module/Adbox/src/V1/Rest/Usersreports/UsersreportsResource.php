<?php
namespace Adbox\V1\Rest\Usersreports;

use Application\Repository\CoreReportsRepository;
use Doctrine\ORM\NoResultException;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class UsersreportsResource extends AbstractResourceListener
{
    /**
     * @var CoreReportsRepository
     */
    private $coreReportsRepository;

    /**
     * UsersreportsResource constructor.
     * @param CoreReportsRepository $coreReportsRepository
     */
    public function __construct($coreReportsRepository)
    {
        $this->coreReportsRepository = $coreReportsRepository;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try {
            return $this->coreReportsRepository->downloadCsv($id);
        } catch (NoResultException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case 'Configuration_Not_Set':
                case 'Missing_Report_Configuration':
                case 'ImportFactory_Not_Set':
                case 'Report_Not_Ready':
                case 'File_Does_Not_Exist':
                case 'Malformed_Columns_Information':
                case 'Invalid_Report_Type':
                case 'Invalid_Column_Request':
                    return new ApiProblem(422, $e->getMessage());
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        try {
            $identityData = $this->getIdentity()->getAuthenticationIdentity();
            return $this->coreReportsRepository->getStatuses($identityData['user_id']);
        } catch (NoResultException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case 'Configuration_Not_Set':
                case 'OauthUsers_Not_Set':
                    return new ApiProblem(422, $e->getMessage());
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
