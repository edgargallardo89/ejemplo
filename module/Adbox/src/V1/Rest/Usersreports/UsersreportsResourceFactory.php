<?php
namespace Adbox\V1\Rest\Usersreports;

use Adbox\Service\Importcsv;
use Application\Entity\CoreReports;
use Application\Entity\OauthUsers;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;

class UsersreportsResourceFactory
{
    /**
     * @param ServiceManager $services
     * @return UsersreportsResource
     */
    public function __invoke($services)
    {
        // Get repositories
        $entityManager = $services->get(EntityManager::class);
        $coreReportsRepository = $entityManager->getRepository(CoreReports::class);
        $oauthUsersRepository = $entityManager->getRepository(OauthUsers::class);

        // Get service classes
        $config = $services->get('config');
        $import = $services->get(Importcsv::class);

        // Set classes
        $coreReportsRepository->setConfig($config);
        $coreReportsRepository->setImportFactory($import);
        $coreReportsRepository->setOauthUsersRepository($oauthUsersRepository);

        return new UsersreportsResource($coreReportsRepository);
    }
}
