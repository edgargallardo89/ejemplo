<?php
namespace Adbox\V1\Rest\Admincreditsadjustmentsettings;

class AdmincreditsadjustmentsettingsResourceFactory
{
    public function __invoke($services)
    {
        return new AdmincreditsadjustmentsettingsResource($services);
    }
}
