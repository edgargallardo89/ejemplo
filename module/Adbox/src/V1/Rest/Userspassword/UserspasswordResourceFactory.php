<?php
namespace Adbox\V1\Rest\Userspassword;

class UserspasswordResourceFactory
{
    public function __invoke($services)
    {
        return new UserspasswordResource($services);
    }
}
