<?php
namespace Adbox\V1\Rest\Admincreditsadjustmentlayout;

class AdmincreditsadjustmentlayoutResourceFactory
{
    public function __invoke($services)
    {
        return new AdmincreditsadjustmentlayoutResource($services);
    }
}
