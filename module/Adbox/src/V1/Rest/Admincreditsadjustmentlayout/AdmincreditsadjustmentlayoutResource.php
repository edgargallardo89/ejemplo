<?php
namespace Adbox\V1\Rest\Admincreditsadjustmentlayout;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\ServiceManager\ServiceManager;
use Zend\Http\Response\Stream;
use Zend\Http\Headers;

class AdmincreditsadjustmentlayoutResource extends AbstractResourceListener
{    
    /**
     * 
     * @param ServiceManager $services
     */
    public function __construct(ServiceManager $services) 
    {
        $this->config = $services->get('config');
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $items = [
            ['username','amount','month','year'],
            ['usuario/email', -100, '01', '2015'],
            ['usuario/email', 100, '02', '2016'],
        ];
        
        $fp = fopen('php://output', "w+");
        
        foreach ($items as $item) {
            fputcsv($fp, $item, ',', '"');
        }
        
        //fclose($fp);
        
        $response = new Stream();
        $headers = new Headers();
        
        $response->setStream($fp);
        $response->setStatusCode(200);
        //$response->setStreamName('a.csv');
        $headers->addHeaders(
            array(
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="adjustment_layout.csv"',
                //'Content-Length' => filesize($filePath)
            )
        );
        
        $response->setHeaders($headers);
        
        return $response;        
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
