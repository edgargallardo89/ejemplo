<?php
namespace Adbox\V1\Rest\Admincreditsresultslayout;

class AdmincreditsresultslayoutResourceFactory
{
    public function __invoke($services)
    {
        return new AdmincreditsresultslayoutResource($services);
    }
}
