<?php
namespace Adbox\V1\Rest\Admincreditsresultslayout;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\ServiceManager\ServiceManager;
use Adteam\Core\Common\Streamfile;
use Adteam\Core\Credits\Result\Layout;

class AdmincreditsresultslayoutResource extends AbstractResourceListener
{
    /**
     *
     * @var Layout 
     */
    protected $layout;
    
    /**
     *
     * @var type 
     */
    protected $service;
    
    /**
     * 
     * @param ServiceManager $services
     */
    public function __construct(ServiceManager $services) 
    {
        $this->service = $services;
        $this->config = $this->getConfig();
        if(!isset($this->config['entity'])){
            throw new \InvalidArgumentException(
                    ' Key entity not found');
        }        
        $this->layout = new Layout($services);
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resource');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $filePath = $this->layout->buildFile('pmr_rules.csv');
        $response = new Streamfile();
        return $response->getStreamForceDownloadFromFile($filePath);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
    
    /**
     * 
     * @return type
     */
    public function getConfig()
    {
        $config =  $this->service->get('config');
        return isset($config['adteam-core-credits-result'])
                                  ?$config['adteam-core-credits-result']:[];
    }     
}
