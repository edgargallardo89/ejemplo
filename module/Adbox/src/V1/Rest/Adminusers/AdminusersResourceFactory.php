<?php
namespace Adbox\V1\Rest\Adminusers;

class AdminusersResourceFactory
{
    public function __invoke($services)
    {
        return new AdminusersResource($services);
    }
}
