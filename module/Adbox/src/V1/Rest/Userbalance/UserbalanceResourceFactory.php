<?php
namespace Adbox\V1\Rest\Userbalance;

class UserbalanceResourceFactory
{
    public function __invoke($services)
    {
        return new UserbalanceResource($services);
    }
}
