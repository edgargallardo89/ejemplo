<?php
namespace Adbox\V1\Rest\Adminpush;

class AdminpushResourceFactory
{
    public function __invoke($services)
    {
        return new AdminpushResource();
    }
}
