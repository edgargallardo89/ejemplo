<?php
namespace Adbox\V1\Rest\Admincreditsresultsfiles;

class AdmincreditsresultsfilesResourceFactory
{
    public function __invoke($services)
    {
        return new AdmincreditsresultsfilesResource($services);
    }
}
