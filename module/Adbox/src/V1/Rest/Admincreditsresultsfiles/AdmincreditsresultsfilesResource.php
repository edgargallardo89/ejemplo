<?php
namespace Adbox\V1\Rest\Admincreditsresultsfiles;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Adteam\Core\Common\JsonPaginator;
use Zend\ServiceManager\ServiceManager;
use Doctrine\ORM\EntityManager;
use Application\Entity\CoreFileUploads;
use Adteam\Core\Common\Streamfile;

class AdmincreditsresultsfilesResource extends AbstractResourceListener
{
    /**
     *
     * @var type 
     */
    protected $em;
    
    /**
     *
     * @var type 
     */
    protected $paginator;

    protected $config;
    
    /**
     * 
     * @param ServiceManager $services
     */
    public function __construct(ServiceManager $services)
    {
        $this->em = $services->get(EntityManager::class);
        $this->paginator = new JsonPaginator();
        $this->config = $services->get('config');
        
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try{
            $item = $this->em->getRepository(CoreFileUploads::class)
                    ->getQbItem($id);
            $filename = $this->config['path'].'/data/upload/csv/'.
                    $item['fileName'];
            $response = new Streamfile();
            return $response->getStreamForceDownloadFromFile($filename);            
        } catch (\Exception $ex) {
            return new ApiProblem(404, 'Not Found');
        }

    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $query = $this->em->getRepository(CoreFileUploads::class)->getQbCollection();        
        $this->paginator->setAdapterPaginatorOrm($query,$params);
        $paginator = $this->paginator->getResponse();
        return $this->setExistFile($paginator);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
    
    private function setExistFile($items)
    {
        $path = $this->config['path'].'/public/logs/';
        foreach ($items['data'] as $key=>$item){
            $filename = $path.$item['log'];                    
            if(!file_exists($filename)){
                $items['data'][$key]['log'] = null;

            }
            $filename = $items['data'][$key]['fileName'];
            $items['data'][$key]['fileName'] = $this->getNameReal($filename);            
            
        }
        return $items;
    } 
    
    private function getNameReal($filename)
    {
        $part = explode('.', $filename);
        $endpart = $part[0];
        $partRemplce = substr($endpart, 0,-14);
        return $partRemplce.'.csv';
    }
}
