<?php

namespace Adbox\V1\Rest\Configprivate;

class ConfigprivateResourceFactory
{
    public function __invoke($services)
    {
        return new ConfigprivateResource($services);
    }
}
