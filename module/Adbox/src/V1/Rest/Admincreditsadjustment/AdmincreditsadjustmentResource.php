<?php

namespace Adbox\V1\Rest\Admincreditsadjustment;

//use Adbox\Service\Importcsv;
use Adteam\Core\Common\ViewHelper;
use Application\Entity\CoreFileUploads;
use Doctrine\ORM\EntityManager;
use Zend\Http\Headers;
use Zend\Http\Response\Stream;
use Zend\ServiceManager\ServiceManager;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Adteam\Core\Credits\Adjustment\Import;
use Adteam\Service\Importcsv;

class AdmincreditsadjustmentResource extends AbstractResourceListener
{
    /** @var \Application\Import\ImportFactory **/
    protected $import;
    
    /** @var \Doctrine\ORM\EntityManager **/
    protected $em;
        
    /** @var \Adteam\Core\Common\ViewHelper **/
    protected $serverUrl;
    
    /** @var array **/
    protected $config;

    protected $csv;
    
    /**
     * 
     * @param ServiceManager $services
     */
    public function __construct(ServiceManager $services)
    {
//        $this->import = $services->get(Importcsv::class);
        $this->em = $services->get(EntityManager::class);
        $this->serverUrl = new ViewHelper($services);
        $this->config = $services->get('config');
        $this->csv = $services->get(Importcsv::class); 
        $this->import = new Import($services);        
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try{
            $inputFilter = $this->getInputFilter();
            $csv = $inputFilter->getValue('csv');
            $items = $this->csv->importCsv($csv);
            return $this->import->create($data,$items,$csv);           
        } catch (\Exception $ex) {
            return new ApiProblem($ex->getCode(), $ex->getMessage());
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try {
            $item = $this->em->getRepository(CoreFileUploads::class)->getQbItem($id);
            $filename = $this->config['path'].'/public/logs/'.$item['fileName'].'.log';            
            $fp = fopen($filename, "r+");            
            $response = new Stream();
            $headers = new Headers();
            
            if (!file_exists($filename)) {
                throw new \Exception('Not Found');
            }
                        
            $response->setStream($fp);
            $response->setStatusCode(200);
            //$response->setStreamName('a.csv');
            
            $headers->addHeaders(
                array(
                    'Content-Type' => 'text/csv',
                    'Content-Disposition' => 'attachment; filename="'.$item['fileName'].'.log"',
                    'Content-Length' => filesize($filename)
                )
            );

            $response->setHeaders($headers);

            return $response;            
        } catch (\Doctrine\ORM\NoResultException $e) { 
            return new ApiProblem(404, 'Record_Not_Found');
        } catch (\Exception $e) {
            switch($e->getMessage()) {
                case 'Not Found':
                    return new ApiProblem(404, 'File_Not_Found');
                default:
                    return new ApiProblem(500, 'Server Error. '.$e->getMessage());
            }
        }
//        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $results = $this->em->getRepository(CoreFileUploads::class)->fetchAllAjustments();
        
        //Log files directory
        $fileLogsDir = $this->config['path'].'/public/logs/';

        //If log file is found, add on the item (payload)
        foreach ($results as &$item) {
            $item['log'] = (file_exists($fileLogsDir.$item["fileName"].'.log')) 
                ? $item["fileName"].'.log'
                : null;
            
            //Sets original name
            $item['fileName'] = preg_replace("/_(?=[^_]*$)+.*(?=\.)/",'',$item['fileName']);
                        
            $item['uploadedAt'] = (is_object($item['uploadedAt']) and $item['uploadedAt'] instanceof \DateTime)
                ? $item['uploadedAt']->format('Y-m-d H:i:s')
                : null;
        }
        
        return ['data' => $results];
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
