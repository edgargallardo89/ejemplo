<?php
namespace Adbox\V1\Rest\Admincreditsadjustment;

class AdmincreditsadjustmentResourceFactory
{
    public function __invoke($services)
    {

        return new AdmincreditsadjustmentResource($services);
    }
}
