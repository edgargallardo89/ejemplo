<?php
namespace Adbox\V1\Rest\Admincreditsresults;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\ServiceManager\ServiceManager;
use Adteam\Core\Credits\Result\Component;

class AdmincreditsresultsResource extends AbstractResourceListener
{
    /**
     *
     * @var type 
     */
    protected $component;

    /**
     *
     * @var type 
     */
    protected $services;
    
    /**
     * 
     * @param \Adbox\V1\Rest\Admincreditsresults\ServiceManager $services
     */
    public function __construct(ServiceManager $services)
    {
        
        $this->services = $services;
        $config = $this->getConfig();
        if(!isset($config['entity'])){
            throw new \InvalidArgumentException(
                    ' Key entity not found');
        }
        $this->component = new Component($services);        
    }      
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {   
        try{
            $inputFilter = $this->getInputFilter();
            $csv = $inputFilter->getValue('csv');
            return $this->component->create($data,$csv);           
        } catch (\Exception $ex) {
            return new ApiProblem($ex->getCode(), $ex->getMessage());
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {    
        return [];
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return $this->component->getQbCollection($params);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
    
    /**
     * 
     * @return type
     */
    public function getConfig()
    {
        $config =  $this->services->get('config');
        return isset($config['adteam-core-credits-result'])
                                  ?$config['adteam-core-credits-result']:[];
    }    
}
