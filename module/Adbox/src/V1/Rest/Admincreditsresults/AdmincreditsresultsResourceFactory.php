<?php
namespace Adbox\V1\Rest\Admincreditsresults;

class AdmincreditsresultsResourceFactory
{
    public function __invoke($services)
    {
        return new AdmincreditsresultsResource($services);
    }
}
