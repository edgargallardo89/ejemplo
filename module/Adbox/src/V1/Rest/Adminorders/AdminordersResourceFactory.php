<?php
namespace Adbox\V1\Rest\Adminorders;

class AdminordersResourceFactory
{
    public function __invoke($services)
    {
        return new AdminordersResource($services);
    }
}
