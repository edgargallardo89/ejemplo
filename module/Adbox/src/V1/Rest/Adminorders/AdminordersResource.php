<?php
namespace Adbox\V1\Rest\Adminorders;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\ServiceManager\ServiceManager;
use Adteam\Core\Admin\Checkout\Checkout as Orders;
use Application\Entity\CoreSendemail;
use Doctrine\ORM\EntityManager;

class AdminordersResource extends AbstractResourceListener
{
    protected $checkout;
    
    protected $sm;

    protected $em;
    
    public function __construct(ServiceManager $service) 
    {
        $this->checkout = new Orders($service);
        $this->sm = $service;
        $this->em = $this->sm->get(EntityManager::class);        
    
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        
        try {
            $result = $this->checkout->create($data);
            return ['orderId'=>$result];
        } catch (\Exception $ex) {
            return new ApiProblem(422, $ex->getMessage());
        }        
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        try{
            $render = $this->sm->get('application_template_render');
            $params = $render->getParams('Cancelación de Canje'); 
            $cancel = $this->checkout->delete($id);
            $params['entitites'] = $this->checkout->fetch($id);
            $params['email']= $params['entitites']['user']['email'];
            $this->em->getRepository(CoreSendemail::class)->saveQueue($params);
            $render->getContent($params,'cancelacion.phtml');            
            return $cancel;
        } catch (\Exception $ex) {
            return new ApiProblem(404, $ex->getMessage());
        }
        
        
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return $this->checkout->fetch($id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return $this->checkout->fetchAll($params);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
