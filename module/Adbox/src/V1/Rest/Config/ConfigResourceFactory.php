<?php
namespace Adbox\V1\Rest\Config;

class ConfigResourceFactory
{
    public function __invoke($services)
    {
        return new ConfigResource($services);
    }
}
