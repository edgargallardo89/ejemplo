<?php
namespace Adbox\V1\Rest\Cedis;

class CedisResourceFactory
{
    public function __invoke($services)
    {
        return new CedisResource($services);
    }
}
