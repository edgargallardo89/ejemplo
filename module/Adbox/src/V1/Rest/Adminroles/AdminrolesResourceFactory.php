<?php
namespace Adbox\V1\Rest\Adminroles;

class AdminrolesResourceFactory
{
    public function __invoke($services)
    {
        return new AdminrolesResource($services);
    }
}
