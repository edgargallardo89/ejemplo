<?php
namespace Adbox\V1\Rest\Adminresource;

class AdminresourceResourceFactory
{
    public function __invoke($services)
    {
        return new AdminresourceResource($services);
    }
}
