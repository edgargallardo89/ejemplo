<?php
namespace Adbox\V1\Rest\Adminusersuploadlayout;

class AdminusersuploadlayoutResourceFactory
{
    public function __invoke($services)
    {
        return new AdminusersuploadlayoutResource($services);
    }
}
