<?php

namespace Adbox\V1\Rest\Users;

use Doctrine\ORM\EntityManager;

class UsersResourceFactory
{
    public function __invoke($services)
    {
        $em = $services->get(EntityManager::class);
                
        return new UsersResource($em,$services);
    }
}
