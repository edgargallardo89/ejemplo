<?php

namespace Adbox\V1\Rest\Users;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Doctrine\ORM\EntityManager;
use Application\Entity\OauthUsers;
use Zend\ServiceManager\ServiceManager;

class UsersResource extends AbstractResourceListener
{
    /** @var EntityManager **/
    private $em;
    
    /**
     *
     * @var type 
     */
    private $sm;
    
    /**
     * 
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em,ServiceManager $sm)
    {
        $this->em = $em;
        $this->sm = $sm;
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {        
        try {
            /** @todo mailing action */
            $render = $this->sm->get('application_template_render');
            $params = $render->getParams('Nuevo Usuario');
            $template = 'registro.phtml';
            $this->em->getRepository(OauthUsers::class)->setEvent($this->sm);
            $password = $this->em->getRepository(OauthUsers::class)->create($data,$params);
            $params['password'] = $password;
            $params['username'] = $data->email;
            $render->getContent($params,$template);
        } catch(\Exception $e) {   
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
        
        return ["message" => "success"];
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $identityData = $this->getIdentity()->getAuthenticationIdentity();

        /** @todo User validation **/
        if(true/*$id === $identityData['user_id']*/) {
            return $this->em->getRepository(OauthUsers::class)->getUser($identityData['user_id']);
        } else {
            return new ApiProblem(405, 'The GET method has not been defined for individual resources');
        }
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        try {
            $render = $this->sm->get('application_template_render');
            $params = $render->getParams('Bienvenida');

            $this->em->getRepository(OauthUsers::class)->setEvent($this->sm);
            $params = $this->em->getRepository(OauthUsers::class)
                    ->fulfillProfile($id, $data,$params);
            $render->getContent($params,'fullfill.phtml');
        } catch(\Exception $e) {  
            return new ApiProblem($e->getCode(), $e->getMessage());
        }
        
        return ["message" => "success",];
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {        
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
