<?php
namespace Adbox\V1\Rest\Admingroups;

use Application\Entity\CoreGroups;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;

class AdmingroupsResourceFactory
{
    /**
     * @param ServiceManager $services
     * @return AdmingroupsResource
     */
    public function __invoke($services)
    {
        // Get repositories
        $entityManager = $services->get(EntityManager::class);
        $coreGroupsRepository = $entityManager->getRepository(CoreGroups::class);

        return new AdmingroupsResource($coreGroupsRepository);
    }
}
