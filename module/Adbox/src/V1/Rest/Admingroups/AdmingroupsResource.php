<?php
namespace Adbox\V1\Rest\Admingroups;

use Application\Repository\CoreGroupsRepository;
use Doctrine\ORM\NoResultException;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AdmingroupsResource extends AbstractResourceListener
{
    /**
     * @var CoreGroupsRepository
     */
    private $coreGroupsRepository;

    /**
     * AdmingroupsResource constructor.
     * @param CoreGroupsRepository $coreGroupsRepository
     */
    public function __construct($coreGroupsRepository)
    {
        $this->coreGroupsRepository = $coreGroupsRepository;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try {
            return $this->coreGroupsRepository->create($data);
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case 'Group_Name_Exists':
                    return new ApiProblem(422, 'Ya existe un grupo registrado con ese nombre');
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        try {
            return $this->coreGroupsRepository->delete($id);
        } catch (NoResultException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case 'Group_Already_Deleted':
                    return new ApiProblem(422, 'El grupo ya se encuentra deshabilitado');
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try {
            return $this->coreGroupsRepository->fetchOne($id);
        } catch (NoResultException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        try {
            return $this->coreGroupsRepository->fetchAll();
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        try {
            return $this->coreGroupsRepository->update($id, $data);
        } catch (NoResultException $e) {
            return new ApiProblem(404, $e->getMessage());
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. ' . $e->getMessage());
            }
        }
    }
}
