<?php
namespace Adbox\V1\Rest\Admincheckoutactivationlog;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Adteam\Core\Admin\Checkout\Activation;
use Zend\ServiceManager\ServiceManager;

class AdmincheckoutactivationlogResource extends AbstractResourceListener
{
    protected $activation;
    
    public function __construct(ServiceManager $services) 
    {
         $this->activation = new Activation($services);
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try{
            $result =  $this->activation->create($data);
            return ['enabled'=>$result];
        } catch (\Exception $ex) {
            return $this->setApiproblem($ex->getMessage());
        }
        

    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        try{
            return $this->activation->hasActivation();
        } catch (\Exception $ex) {
            return $this->setApiproblem($ex->getMessage());
        }
                    
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }

    /**
     *
     * @param string $message
     * @return ApiProblem
     */
    public function setApiproblem($message)
    {
        switch ($message) {
            case (true === (bool)preg_match('/Inicializar_canje/', $message)):
                $apiproblem = new ApiProblem(422, 'Se debe inicializar canje');
                break;
            case (true === (bool)preg_match('/canje activo/', $message)):
                $apiproblem = new ApiProblem(422, 'canje activo');
                break;
            case (true === (bool)preg_match('/canje cerrado/', $message)):
                $apiproblem = new ApiProblem(422, 'canje cerrado');
                break;
            case (true === (bool)preg_match('/only_true/', $message)):
                $apiproblem = new ApiProblem(422, 'En este momento solo permite activar');
                break;
            case 'Missing_Arguments':
                $apiproblem = new ApiProblem(406, 'Es necesaria una fecha de apertura o de cierre');
                break;
            case 'Invalid_Range':
                $apiproblem = new ApiProblem(406, 'La fecha de apertura no puede ser mayor a la fecha de cierre');
                break;
            default:
                $apiproblem = new ApiProblem(500, 'Something went wrong Internally.' .
                    ' Please Contact the Administrator. ' . $message);
                break;
        }

        return $apiproblem;
    }
}
