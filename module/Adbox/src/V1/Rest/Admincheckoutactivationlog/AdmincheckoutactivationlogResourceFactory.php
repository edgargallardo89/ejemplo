<?php
namespace Adbox\V1\Rest\Admincheckoutactivationlog;

class AdmincheckoutactivationlogResourceFactory
{
    public function __invoke($services)
    {
        return new AdmincheckoutactivationlogResource($services);
    }
}
