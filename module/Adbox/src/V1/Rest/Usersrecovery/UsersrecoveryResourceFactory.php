<?php
namespace Adbox\V1\Rest\Usersrecovery;

use Doctrine\ORM\EntityManager;

class UsersrecoveryResourceFactory
{
    public function __invoke($services)
    {
        $em = $services->get(EntityManager::class);
        
        return new UsersrecoveryResource($em,$services);
    }
}
