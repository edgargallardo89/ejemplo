<?php
namespace Adbox\V1\Rest\Configpublic;

class ConfigpublicResourceFactory
{
    public function __invoke($services)
    { 
        
        return new ConfigpublicResource($services);
    }
}
