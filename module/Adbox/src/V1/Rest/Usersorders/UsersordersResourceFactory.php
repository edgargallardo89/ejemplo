<?php
namespace Adbox\V1\Rest\Usersorders;

class UsersordersResourceFactory
{
    public function __invoke($services)
    {
        return new UsersordersResource($services);
    }
}
