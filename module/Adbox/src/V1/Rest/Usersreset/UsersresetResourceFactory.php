<?php
namespace Adbox\V1\Rest\Usersreset;

use Doctrine\ORM\EntityManager;

class UsersresetResourceFactory
{
    public function __invoke($services)
    {
        $em = $services->get(EntityManager::class);
        
        return new UsersresetResource($em);
    }
}
