<?php
namespace Adbox\V1\Rest\Zipcode;

class ZipcodeResourceFactory
{
    public function __invoke($services)
    {
        return new ZipcodeResource($services);
    }
}
