<?php
namespace Adbox\V1\Rest\Checkoutsteps;

class CheckoutstepsResourceFactory
{
    public function __invoke($services)
    {
        return new CheckoutstepsResource($services);
    }
}
