<?php
namespace Adbox\V1\Rest\Checkout;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\ServiceManager\ServiceManager;
use Adteam\Core\Checkout\Checkout;
use Adteam\Core\Adcinema\Adcinema;
use Adteam\Core\Admin\Checkout\Checkout as AdminCheckout;
use Application\Entity\CoreSendemail;
use Doctrine\ORM\EntityManager;

class CheckoutResource extends AbstractResourceListener
{
    /**
     *
     * @var type 
     */
    protected $checkout;
    
    protected $sm;
    
    protected $admincheckout;
    
    protected $em;
    
    public function __construct(ServiceManager $services) 
    {
        $this->checkout = new Checkout($services);
        $this->adcinema = new Adcinema($services);
        $this->sm = $services;
        $this->admincheckout = new AdminCheckout($services);
        $this->em = $this->sm->get(EntityManager::class);
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try {
            $render = $this->sm->get('application_template_render');
            $params = $render->getParams('Confirmación de Canje'); 
            $result =  $this->checkout->create($data);            
            $params['entitites'] = $this->admincheckout->fetch($result);
            $params['email']= $params['entitites']['user']['email'];
            $this->em->getRepository(CoreSendemail::class)->saveQueue($params);
            $this->adcinema->create($params);
            $render->getContent($params,'confirmacion.phtml');
             return ['orderId'=>$result];
        } catch (\Exception $ex) {
            return new ApiProblem(422, $ex->getMessage());
        }

    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resource');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
