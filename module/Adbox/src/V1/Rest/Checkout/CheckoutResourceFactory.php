<?php
namespace Adbox\V1\Rest\Checkout;

class CheckoutResourceFactory
{
    public function __invoke($services)
    {
        return new CheckoutResource($services);
    }
}
