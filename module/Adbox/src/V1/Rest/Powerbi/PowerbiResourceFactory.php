<?php
namespace Adbox\V1\Rest\Powerbi;

class PowerbiResourceFactory
{
    public function __invoke($services)
    {
        return new PowerbiResource($services);
    }
}
