<?php
namespace Adbox\V1\Rest\Themepreview;

class ThemepreviewResourceFactory
{
    public function __invoke($services)
    {
        return new ThemepreviewResource($services);
    }
}
