<?php
namespace Adbox\V1\Rest\Adminusersupload;

class AdminusersuploadResourceFactory
{
    public function __invoke($services)
    {
        return new AdminusersuploadResource($services);
    }
}
