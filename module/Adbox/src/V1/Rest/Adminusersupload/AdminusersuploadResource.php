<?php
namespace Adbox\V1\Rest\Adminusersupload;

use Adbox\Service\Importcsv;
use Adteam\Core\Common\ViewHelper;
use Application\Entity\CoreUsers;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class AdminusersuploadResource extends AbstractResourceListener
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    private $sm;

    /**
     * @var \Application\Import\ImportFactory
     */
    protected $import;

    /**
     * @var \Adteam\Core\Common\ViewHelper
     */
    protected $serverUrl;

    /**
     * @var mixed
     */
    protected $config;

    /**
     * AdminusersuploadResource constructor.
     */
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        $this->em = $sm->get(EntityManager::class);
        $this->import = $sm->get(Importcsv::class);
        $this->serverUrl = new ViewHelper($sm);
        $this->config = $sm->get('config');
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try {
            ini_set('max_execution_time', 0);
            $inputFilter = $this->getInputFilter();
            $csv = $inputFilter->getValue('csv');
            $data = (array)$data;
            unset($data['csv']);

            $data = array_merge($data, [
                'fileName' => pathinfo($csv["tmp_name"])['basename'],
                'users' => $this->import->importCsv($csv),
            ]);

            $result = $this->em->getRepository(CoreUsers::class)->uploadCsv($data);
        } catch (\Exception $e) {
            switch ($e->getMessage()) {
                case 'Invalid_file_format_maxExceeded':
                    return new ApiProblem(422, 'Máximo de filas violado. Sólo son admitidas: ' . $this->config['adbox-std-import']['maxfieldcsv']);
                case 'Invalid_file_format_empty':
                    return new ApiProblem(422, 'Documento vacío.');
                case (true === (bool)preg_match('/Formato de CSV/', $e->getMessage())):
                    return new ApiProblem(422, $e->getMessage());
                case 'Invalid_FileType':
                    return new ApiProblem(422, 'Tipo de archivo inválido.');
                case 'Invalid_Argument_Type':
                    return new ApiProblem(422, 'Uno o más argumentos dentro las filas del archivo, son inválidos.');
                default:
                    return new ApiProblem(500, 'Error de servidor. Algo resultó mal Internamente. Por favor contacte al Administrador. ' . $e->getMessage());
            }
        }

        // Check for errors
        $hasErrors = (isset($result['errorCount']) and $result['errorCount'] > 0);

        // Sets proper response
        $response = [
            'message' => $hasErrors ? 'partial_success' : 'success',
        ];

        if ($hasErrors) {
            $response = array_merge(
                $response,
                [
                    'errorCount' => $result['errorCount'],
                    'errors' => $result['errors'],
                ]
            );
        }

        return $response;
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
