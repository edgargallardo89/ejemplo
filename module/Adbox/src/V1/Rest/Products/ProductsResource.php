<?php
namespace Adbox\V1\Rest\Products;

use Adteam\Core\Common\ViewHelper;
use Application\Entity\CoreProducts;
use Doctrine\ORM\EntityManager;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Adteam\Core\Categories\Component;
use Zend\ServiceManager\ServiceManager;

class ProductsResource extends AbstractResourceListener
{
    /** @var Doctrine\ORM\EntityManager **/
    private $em;
    
    /** @var \Adteam\Core\Common\ViewHelper **/
    private $serverUrl;
    
    protected $component;
    
    public function __construct(ServiceManager $services)
    {
        $this->serverUrl = new ViewHelper($services);
        $this->em = $services->get(EntityManager::class);
        $this->component = new Component($services);
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        try {
            $identity = $this->getIdentity()->getAuthenticationIdentity();
            $username = $identity["user_id"];
            $serverUrl = $this->serverUrl->__invoke('/img/');
            return $this->em->getRepository(CoreProducts::class)->getProduct($id, $username, $serverUrl,$this->component);
        } catch (\Exception $e) {
            switch($e->getMessage()) {
                case 'No result was found for query although at least one row was expected.':
                    return null;
                default:
                    return new ApiProblem(500, 'Something went wrong Internally. Please Contact the Administrator. '.$e->getMessage());
            }
        }
//        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $identity = $this->getIdentity()->getAuthenticationIdentity();
        $username = $identity["user_id"];
        $serverUrl = $this->serverUrl->__invoke('/img/');
        return $this->em->getRepository(CoreProducts::class)->getProducts($username, $serverUrl,$this->component);
        //return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}