<?php
namespace Adbox\V1\Rest\Usersdelivery;

class UsersdeliveryResourceFactory
{
    public function __invoke($services)
    {
        return new UsersdeliveryResource($services);
    }
}
