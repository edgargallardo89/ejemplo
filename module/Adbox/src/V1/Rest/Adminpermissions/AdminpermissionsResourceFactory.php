<?php
namespace Adbox\V1\Rest\Adminpermissions;

class AdminpermissionsResourceFactory
{
    public function __invoke($services)
    {
        return new AdminpermissionsResource($services);
    }
}
