<?php
namespace Adbox;

use Zend\Console\Request as ConsoleRequest;
use Zend\Mvc\MvcEvent;
use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'ZF\Apigility\Autoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src',
                ],
            ],
        ];
    }

    public function onBootstrap(MvcEvent $e)
    {
        if (!($e->getRequest() instanceof ConsoleRequest)) {
            $headers = $e->getRequest()->getHeaders();

            if ($headers->has('Origin')) {
                // convert to array because get method throw an exception
                $headersArray = $headers->toArray();
                $origin = $headersArray['Origin'];
                if ($origin === 'file://') {
                    unset($headersArray['Origin']);
                    $headers->clearHeaders();
                    $headers->addHeaders($headersArray);
                    //this is a valid uri
                    $headers->addHeaderLine('Origin', 'file://cordova');
                }
            }
        }
    }
}
