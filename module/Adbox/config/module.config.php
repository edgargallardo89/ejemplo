<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Adbox\\V1\\Rest\\Status\\StatusResource' => 'Adbox\\V1\\Rest\\Status\\StatusResourceFactory',
            'Adbox\\V1\\Rest\\Configpublic\\ConfigpublicResource' => 'Adbox\\V1\\Rest\\Configpublic\\ConfigpublicResourceFactory',
            'Adbox\\V1\\Rest\\Configprivate\\ConfigprivateResource' => 'Adbox\\V1\\Rest\\Configprivate\\ConfigprivateResourceFactory',
            'Adbox\\V1\\Rest\\Usersreset\\UsersresetResource' => 'Adbox\\V1\\Rest\\Usersreset\\UsersresetResourceFactory',
            'Adbox\\V1\\Rest\\Usersrecovery\\UsersrecoveryResource' => 'Adbox\\V1\\Rest\\Usersrecovery\\UsersrecoveryResourceFactory',
            'Adbox\\V1\\Rest\\Users\\UsersResource' => 'Adbox\\V1\\Rest\\Users\\UsersResourceFactory',
            'Adbox\\V1\\Rest\\Products\\ProductsResource' => 'Adbox\\V1\\Rest\\Products\\ProductsResourceFactory',
            'Adbox\\V1\\Rest\\Adminproductssync\\AdminproductssyncResource' => 'Adbox\\V1\\Rest\\Adminproductssync\\AdminproductssyncResourceFactory',
            'Adbox\\V1\\Rest\\Categories\\CategoriesResource' => 'Adbox\\V1\\Rest\\Categories\\CategoriesResourceFactory',
            'Adbox\\V1\\Rest\\Admincreditsadjustment\\AdmincreditsadjustmentResource' => 'Adbox\\V1\\Rest\\Admincreditsadjustment\\AdmincreditsadjustmentResourceFactory',
            'Adbox\\V1\\Rest\\Banners\\BannersResource' => 'Adbox\\V1\\Rest\\Banners\\BannersResourceFactory',
            'Adbox\\V1\\Rest\\Admincreditsresults\\AdmincreditsresultsResource' => 'Adbox\\V1\\Rest\\Admincreditsresults\\AdmincreditsresultsResourceFactory',
            'Adbox\\V1\\Rest\\Admincreditsresultslayout\\AdmincreditsresultslayoutResource' => 'Adbox\\V1\\Rest\\Admincreditsresultslayout\\AdmincreditsresultslayoutResourceFactory',
            'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\AdmincreditsresultsfilesResource' => 'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\AdmincreditsresultsfilesResourceFactory',
            'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\AdmincreditsadjustmentsettingsResource' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\AdmincreditsadjustmentsettingsResourceFactory',
            'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\AdmincreditsadjustmentlayoutResource' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\AdmincreditsadjustmentlayoutResourceFactory',
            'Adbox\\V1\\Rest\\Userbalance\\UserbalanceResource' => 'Adbox\\V1\\Rest\\Userbalance\\UserbalanceResourceFactory',
            'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\AdmincreditsadjustmentfilesResource' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\AdmincreditsadjustmentfilesResourceFactory',
            'Adbox\\V1\\Rest\\Usercart\\UsercartResource' => 'Adbox\\V1\\Rest\\Usercart\\UsercartResourceFactory',
            'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\AdmincheckoutactivationlogResource' => 'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\AdmincheckoutactivationlogResourceFactory',
            'Adbox\\V1\\Rest\\Checkout\\CheckoutResource' => 'Adbox\\V1\\Rest\\Checkout\\CheckoutResourceFactory',
            'Adbox\\V1\\Rest\\Checkoutsteps\\CheckoutstepsResource' => 'Adbox\\V1\\Rest\\Checkoutsteps\\CheckoutstepsResourceFactory',
            'Adbox\\V1\\Rest\\Cedis\\CedisResource' => 'Adbox\\V1\\Rest\\Cedis\\CedisResourceFactory',
            'Adbox\\V1\\Rest\\Zipcode\\ZipcodeResource' => 'Adbox\\V1\\Rest\\Zipcode\\ZipcodeResourceFactory',
            'Adbox\\V1\\Rest\\Adminorders\\AdminordersResource' => 'Adbox\\V1\\Rest\\Adminorders\\AdminordersResourceFactory',
            'Adbox\\V1\\Rest\\Usersinfo\\UsersinfoResource' => 'Adbox\\V1\\Rest\\Usersinfo\\UsersinfoResourceFactory',
            'Adbox\\V1\\Rest\\Userspassword\\UserspasswordResource' => 'Adbox\\V1\\Rest\\Userspassword\\UserspasswordResourceFactory',
            'Adbox\\V1\\Rest\\Usersdelivery\\UsersdeliveryResource' => 'Adbox\\V1\\Rest\\Usersdelivery\\UsersdeliveryResourceFactory',
            'Adbox\\V1\\Rest\\Adminusers\\AdminusersResource' => 'Adbox\\V1\\Rest\\Adminusers\\AdminusersResourceFactory',
            'Adbox\\V1\\Rest\\Messages\\MessagesResource' => 'Adbox\\V1\\Rest\\Messages\\MessagesResourceFactory',
            'Adbox\\V1\\Rest\\Config\\ConfigResource' => 'Adbox\\V1\\Rest\\Config\\ConfigResourceFactory',
            'Adbox\\V1\\Rest\\Powerbi\\PowerbiResource' => 'Adbox\\V1\\Rest\\Powerbi\\PowerbiResourceFactory',
            'Adbox\\V1\\Rest\\Adminusersinfo\\AdminusersinfoResource' => 'Adbox\\V1\\Rest\\Adminusersinfo\\AdminusersinfoResourceFactory',
            'Adbox\\V1\\Rest\\Adminusersdelivery\\AdminusersdeliveryResource' => 'Adbox\\V1\\Rest\\Adminusersdelivery\\AdminusersdeliveryResourceFactory',
            'Adbox\\V1\\Rest\\Adminroles\\AdminrolesResource' => 'Adbox\\V1\\Rest\\Adminroles\\AdminrolesResourceFactory',
            'Adbox\\V1\\Rest\\Adminresource\\AdminresourceResource' => 'Adbox\\V1\\Rest\\Adminresource\\AdminresourceResourceFactory',
            'Adbox\\V1\\Rest\\Adminpermissions\\AdminpermissionsResource' => 'Adbox\\V1\\Rest\\Adminpermissions\\AdminpermissionsResourceFactory',
            'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\AdminpermissionsbuildresourceResource' => 'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\AdminpermissionsbuildresourceResourceFactory',
            'Adbox\\V1\\Rest\\Adminpermissionsbuild\\AdminpermissionsbuildResource' => 'Adbox\\V1\\Rest\\Adminpermissionsbuild\\AdminpermissionsbuildResourceFactory',
            'Adbox\\V1\\Rest\\Adminusersupload\\AdminusersuploadResource' => 'Adbox\\V1\\Rest\\Adminusersupload\\AdminusersuploadResourceFactory',
            'Adbox\\V1\\Rest\\Adminusersuploadlayout\\AdminusersuploadlayoutResource' => 'Adbox\\V1\\Rest\\Adminusersuploadlayout\\AdminusersuploadlayoutResourceFactory',
            'Adbox\\V1\\Rest\\Theme\\ThemeResource' => 'Adbox\\V1\\Rest\\Theme\\ThemeResourceFactory',
            'Adbox\\V1\\Rest\\Themepreview\\ThemepreviewResource' => 'Adbox\\V1\\Rest\\Themepreview\\ThemepreviewResourceFactory',
            'Adbox\\V1\\Rest\\Usersorders\\UsersordersResource' => 'Adbox\\V1\\Rest\\Usersorders\\UsersordersResourceFactory',
            'Adbox\\V1\\Rest\\Reports\\ReportsResource' => 'Adbox\\V1\\Rest\\Reports\\ReportsResourceFactory',
            'Adbox\\V1\\Rest\\Usersreports\\UsersreportsResource' => 'Adbox\\V1\\Rest\\Usersreports\\UsersreportsResourceFactory',
            'Adbox\\V1\\Rest\\Admindashboard\\AdmindashboardResource' => 'Adbox\\V1\\Rest\\Admindashboard\\AdmindashboardResourceFactory',
            'Adbox\\V1\\Rest\\Admingroups\\AdmingroupsResource' => 'Adbox\\V1\\Rest\\Admingroups\\AdmingroupsResourceFactory',
            'Adbox\\V1\\Rest\\Usersdevices\\UsersdevicesResource' => 'Adbox\\V1\\Rest\\Usersdevices\\UsersdevicesResourceFactory',
            'Adbox\\V1\\Rest\\Adminpush\\AdminpushResource' => 'Adbox\\V1\\Rest\\Adminpush\\AdminpushResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'adbox.rest.status' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/status[/:status_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Status\\Controller',
                    ),
                ),
            ),
            'adbox.rest.configpublic' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/configpublic[/:configpublic_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Configpublic\\Controller',
                    ),
                ),
            ),
            'adbox.rest.configprivate' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/configprivate[/:configprivate_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Configprivate\\Controller',
                    ),
                ),
            ),
            'adbox.rest.usersreset' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/usersreset[/:usersreset_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Usersreset\\Controller',
                    ),
                ),
            ),
            'adbox.rest.usersrecovery' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/usersrecovery[/:usersrecovery_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Usersrecovery\\Controller',
                    ),
                ),
            ),
            'adbox.rest.users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users[/:users_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Users\\Controller',
                    ),
                ),
            ),
            'adbox.rest.products' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/products[/:products_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Products\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminproductssync' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/adminproductssync[/:adminproductssync_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminproductssync\\Controller',
                    ),
                ),
            ),
            'adbox.rest.categories' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/categories[/:categories_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Categories\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admincreditsadjustment' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/credits/adjustment[/:admincreditsadjustment_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admincreditsadjustment\\Controller',
                    ),
                ),
            ),
            'adbox.rest.banners' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/banners[/:banners_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Banners\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admincreditsresults' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/credits[/:admincreditsresults_id]/results',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admincreditsresults\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admincreditsresultslayout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/credits/results[/:admincreditsresultslayout_id]/layout',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admincreditsresultslayout\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admincreditsresultsfiles' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/credits/results[/:admincreditsresultsfiles_id]/files',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admincreditsadjustmentsettings' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/credits/adjustment/settings[/:admincreditsadjustmentsettings_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admincreditsadjustmentlayout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/credits/adjustment/layout[/:admincreditsadjustmentlayout_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\Controller',
                    ),
                ),
            ),
            'adbox.rest.userbalance' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users[/:userbalance_id]/balance',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Userbalance\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admincreditsadjustmentfiles' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/credits/adjustment[/:admincreditsadjustmentfiles_id]/files',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\Controller',
                    ),
                ),
            ),
            'adbox.rest.usercart' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users/:user_id/cart[/:product_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Usercart\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admincheckoutactivationlog' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/checkout/status[/:admincheckoutactivationlog_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Controller',
                    ),
                ),
            ),
            'adbox.rest.checkout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/checkout[/:checkout_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Checkout\\Controller',
                    ),
                ),
            ),
            'adbox.rest.checkoutsteps' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/checkout/steps[/:checkoutsteps_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Checkoutsteps\\Controller',
                    ),
                ),
            ),
            'adbox.rest.cedis' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/cedis[/:cedis_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Cedis\\Controller',
                    ),
                ),
            ),
            'adbox.rest.zipcode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/zipcode[/:zipcode_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Zipcode\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminorders' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/orders[/:adminorders_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminorders\\Controller',
                    ),
                ),
            ),
            'adbox.rest.usersinfo' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users/:usersinfo_id/info',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Usersinfo\\Controller',
                    ),
                ),
            ),
            'adbox.rest.userspassword' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users/:userspassword_id/password',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Userspassword\\Controller',
                    ),
                ),
            ),
            'adbox.rest.usersdelivery' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users/:usersdelivery_id/delivery',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Usersdelivery\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminusers' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/users[/:adminusers_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminusers\\Controller',
                    ),
                ),
            ),
            'adbox.rest.messages' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/messages[/:messages_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Messages\\Controller',
                    ),
                ),
            ),
            'adbox.rest.config' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/configuration[/:config_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Config\\Controller',
                    ),
                ),
            ),
            'adbox.rest.powerbi' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/powerbi[/:powerbi_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Powerbi\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminusersinfo' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/users/:adminusersinfo_id/info',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminusersinfo\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminusersdelivery' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/users/:adminusersdelivery_id/delivery',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminusersdelivery\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminroles' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/roles[/:adminroles_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminroles\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminresource' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/resources[/:adminresource_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminresource\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminpermissions' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/permissions[/:adminpermissions_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminpermissions\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminpermissionsbuildresource' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/permissions/resource[/:adminpermissionsbuildresource_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminpermissionsbuild' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/permissions/build[/:adminpermissionsbuild_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminpermissionsbuild\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminusersupload' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/users/upload[/:adminusersupload_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminusersupload\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminusersuploadlayout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/users/upload/layout[/:adminusersuploadlayout_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminusersuploadlayout\\Controller',
                    ),
                ),
            ),
            'adbox.rest.theme' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/theme[/:theme_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Theme\\Controller',
                    ),
                ),
            ),
            'adbox.rest.themepreview' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/theme[/:themepreview_id]/preview',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Themepreview\\Controller',
                    ),
                ),
            ),
            'adbox.rest.usersorders' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users[/:user_id]/orders[/:usersorders_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Usersorders\\Controller',
                    ),
                ),
            ),
            'adbox.rest.reports' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/reports[/:reports_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Reports\\Controller',
                    ),
                ),
            ),
            'adbox.rest.usersreports' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users/reports[/:usersreports_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Usersreports\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admindashboard' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/dashboard[/:admindashboard_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admindashboard\\Controller',
                    ),
                ),
            ),
            'adbox.rest.admingroups' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/groups[/:admingroups_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Admingroups\\Controller',
                    ),
                ),
            ),
            'adbox.rest.usersdevices' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users/devices[/:usersdevices_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Usersdevices\\Controller',
                    ),
                ),
            ),
            'adbox.rest.adminpush' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/push[/:adminpush_id]',
                    'defaults' => array(
                        'controller' => 'Adbox\\V1\\Rest\\Adminpush\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'adbox.rest.status',
            1 => 'adbox.rest.configpublic',
            2 => 'adbox.rest.configprivate',
            3 => 'adbox.rest.usersreset',
            4 => 'adbox.rest.usersrecovery',
            5 => 'adbox.rest.users',
            6 => 'adbox.rest.products',
            7 => 'adbox.rest.adminproductssync',
            8 => 'adbox.rest.categories',
            9 => 'adbox.rest.admincreditsadjustment',
            10 => 'adbox.rest.banners',
            11 => 'adbox.rest.admincreditsresults',
            12 => 'adbox.rest.admincreditsresultslayout',
            15 => 'adbox.rest.admincreditsresultsfiles',
            16 => 'adbox.rest.admincreditsadjustmentsettings',
            17 => 'adbox.rest.admincreditsadjustmentlayout',
            18 => 'adbox.rest.userbalance',
            19 => 'adbox.rest.admincreditsadjustmentfiles',
            20 => 'adbox.rest.usercart',
            21 => 'adbox.rest.admincheckoutactivationlog',
            23 => 'adbox.rest.checkout',
            24 => 'adbox.rest.checkoutsteps',
            25 => 'adbox.rest.cedis',
            26 => 'adbox.rest.zipcode',
            27 => 'adbox.rest.adminorders',
            28 => 'adbox.rest.usersinfo',
            29 => 'adbox.rest.userspassword',
            30 => 'adbox.rest.usersdelivery',
            31 => 'adbox.rest.adminusers',
            32 => 'adbox.rest.messages',
            33 => 'adbox.rest.config',
            34 => 'adbox.rest.powerbi',
            35 => 'adbox.rest.adminusersinfo',
            37 => 'adbox.rest.adminusersdelivery',
            38 => 'adbox.rest.adminroles',
            39 => 'adbox.rest.adminresource',
            40 => 'adbox.rest.adminpermissions',
            41 => 'adbox.rest.adminpermissionsbuildresource',
            42 => 'adbox.rest.adminpermissionsbuild',
            43 => 'adbox.rest.adminusersupload',
            44 => 'adbox.rest.adminusersuploadlayout',
            45 => 'adbox.rest.theme',
            46 => 'adbox.rest.themepreview',
            47 => 'adbox.rest.usersorders',
            48 => 'adbox.rest.reports',
            49 => 'adbox.rest.usersreports',
            50 => 'adbox.rest.admindashboard',
            51 => 'adbox.rest.admingroups',
            52 => 'adbox.rest.usersdevices',
            53 => 'adbox.rest.adminpush',
        ),
        'default_version' => 1,
    ),
    'zf-rest' => array(
        'Adbox\\V1\\Rest\\Status\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Status\\StatusResource',
            'route_name' => 'adbox.rest.status',
            'route_identifier_name' => 'status_id',
            'collection_name' => 'status',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'StatusLib\\Entity',
            'collection_class' => 'StatusLib\\Collection',
            'service_name' => 'Status',
        ),
        'Adbox\\V1\\Rest\\Configpublic\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Configpublic\\ConfigpublicResource',
            'route_name' => 'adbox.rest.configpublic',
            'route_identifier_name' => 'configpublic_id',
            'collection_name' => 'configpublic',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Configpublic\\ConfigpublicEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Configpublic\\ConfigpublicCollection',
            'service_name' => 'configpublic',
        ),
        'Adbox\\V1\\Rest\\Configprivate\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Configprivate\\ConfigprivateResource',
            'route_name' => 'adbox.rest.configprivate',
            'route_identifier_name' => 'configprivate_id',
            'collection_name' => 'configprivate',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Configprivate\\ConfigprivateEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Configprivate\\ConfigprivateCollection',
            'service_name' => 'configprivate',
        ),
        'Adbox\\V1\\Rest\\Usersreset\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Usersreset\\UsersresetResource',
            'route_name' => 'adbox.rest.usersreset',
            'route_identifier_name' => 'usersreset_id',
            'collection_name' => 'usersreset',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PATCH',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Usersreset\\UsersresetEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Usersreset\\UsersresetCollection',
            'service_name' => 'usersreset',
        ),
        'Adbox\\V1\\Rest\\Usersrecovery\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Usersrecovery\\UsersrecoveryResource',
            'route_name' => 'adbox.rest.usersrecovery',
            'route_identifier_name' => 'usersrecovery_id',
            'collection_name' => 'usersrecovery',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Usersrecovery\\UsersrecoveryEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Usersrecovery\\UsersrecoveryCollection',
            'service_name' => 'usersrecovery',
        ),
        'Adbox\\V1\\Rest\\Users\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Users\\UsersResource',
            'route_name' => 'adbox.rest.users',
            'route_identifier_name' => 'users_id',
            'collection_name' => 'users',
            'entity_http_methods' => array(
                0 => 'PATCH',
                1 => 'DELETE',
                2 => 'PUT',
                3 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'POST',
                1 => 'PUT',
                2 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Users\\UsersEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Users\\UsersCollection',
            'service_name' => 'users',
        ),
        'Adbox\\V1\\Rest\\Products\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Products\\ProductsResource',
            'route_name' => 'adbox.rest.products',
            'route_identifier_name' => 'products_id',
            'collection_name' => 'products',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Products\\ProductsEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Products\\ProductsCollection',
            'service_name' => 'products',
        ),
        'Adbox\\V1\\Rest\\Adminproductssync\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminproductssync\\AdminproductssyncResource',
            'route_name' => 'adbox.rest.adminproductssync',
            'route_identifier_name' => 'adminproductssync_id',
            'collection_name' => 'adminproductssync',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminproductssync\\AdminproductssyncEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminproductssync\\AdminproductssyncCollection',
            'service_name' => 'adminproductssync',
        ),
        'Adbox\\V1\\Rest\\Categories\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Categories\\CategoriesResource',
            'route_name' => 'adbox.rest.categories',
            'route_identifier_name' => 'categories_id',
            'collection_name' => 'categories',
            'entity_http_methods' => array(
                0 => 'PUT',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Categories\\CategoriesEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Categories\\CategoriesCollection',
            'service_name' => 'categories',
        ),
        'Adbox\\V1\\Rest\\Admincreditsadjustment\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admincreditsadjustment\\AdmincreditsadjustmentResource',
            'route_name' => 'adbox.rest.admincreditsadjustment',
            'route_identifier_name' => 'admincreditsadjustment_id',
            'collection_name' => 'admincreditsadjustment',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'page',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admincreditsadjustment\\AdmincreditsadjustmentEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admincreditsadjustment\\AdmincreditsadjustmentCollection',
            'service_name' => 'admincreditsadjustment',
        ),
        'Adbox\\V1\\Rest\\Banners\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Banners\\BannersResource',
            'route_name' => 'adbox.rest.banners',
            'route_identifier_name' => 'banners_id',
            'collection_name' => 'banners',
            'entity_http_methods' => array(
                0 => 'PATCH',
                1 => 'PUT',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Banners\\BannersEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Banners\\BannersCollection',
            'service_name' => 'banners',
        ),
        'Adbox\\V1\\Rest\\Admincreditsresults\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admincreditsresults\\AdmincreditsresultsResource',
            'route_name' => 'adbox.rest.admincreditsresults',
            'route_identifier_name' => 'admincreditsresults_id',
            'collection_name' => 'admincreditsresults',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'page',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admincreditsresults\\AdmincreditsresultsEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admincreditsresults\\AdmincreditsresultsCollection',
            'service_name' => 'admincreditsresults',
        ),
        'Adbox\\V1\\Rest\\Admincreditsresultslayout\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admincreditsresultslayout\\AdmincreditsresultslayoutResource',
            'route_name' => 'adbox.rest.admincreditsresultslayout',
            'route_identifier_name' => 'admincreditsresultslayout_id',
            'collection_name' => 'admincreditsresultslayout',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admincreditsresultslayout\\AdmincreditsresultslayoutEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admincreditsresultslayout\\AdmincreditsresultslayoutCollection',
            'service_name' => 'admincreditsresultslayout',
        ),
        'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\AdmincreditsresultsfilesResource',
            'route_name' => 'adbox.rest.admincreditsresultsfiles',
            'route_identifier_name' => 'admincreditsresultsfiles_id',
            'collection_name' => 'admincreditsresultsfiles',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(
                0 => 'page',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\AdmincreditsresultsfilesEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\AdmincreditsresultsfilesCollection',
            'service_name' => 'admincreditsresultsfiles',
        ),
        'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\AdmincreditsadjustmentsettingsResource',
            'route_name' => 'adbox.rest.admincreditsadjustmentsettings',
            'route_identifier_name' => 'admincreditsadjustmentsettings_id',
            'collection_name' => 'admincreditsadjustmentsettings',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\AdmincreditsadjustmentsettingsEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\AdmincreditsadjustmentsettingsCollection',
            'service_name' => 'admincreditsadjustmentsettings',
        ),
        'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\AdmincreditsadjustmentlayoutResource',
            'route_name' => 'adbox.rest.admincreditsadjustmentlayout',
            'route_identifier_name' => 'admincreditsadjustmentlayout_id',
            'collection_name' => 'admincreditsadjustmentlayout',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\AdmincreditsadjustmentlayoutEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\AdmincreditsadjustmentlayoutCollection',
            'service_name' => 'admincreditsadjustmentlayout',
        ),
        'Adbox\\V1\\Rest\\Userbalance\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Userbalance\\UserbalanceResource',
            'route_name' => 'adbox.rest.userbalance',
            'route_identifier_name' => 'userbalance_id',
            'collection_name' => 'userbalance',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Userbalance\\UserbalanceEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Userbalance\\UserbalanceCollection',
            'service_name' => 'userbalance',
        ),
        'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\AdmincreditsadjustmentfilesResource',
            'route_name' => 'adbox.rest.admincreditsadjustmentfiles',
            'route_identifier_name' => 'admincreditsadjustmentfiles_id',
            'collection_name' => 'admincreditsadjustmentfiles',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\AdmincreditsadjustmentfilesEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\AdmincreditsadjustmentfilesCollection',
            'service_name' => 'admincreditsadjustmentfiles',
        ),
        'Adbox\\V1\\Rest\\Usercart\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Usercart\\UsercartResource',
            'route_name' => 'adbox.rest.usercart',
            'route_identifier_name' => 'product_id',
            'collection_name' => 'usercart',
            'entity_http_methods' => array(
                0 => 'PUT',
                1 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'version',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Usercart\\UsercartEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Usercart\\UsercartCollection',
            'service_name' => 'usercart',
        ),
        'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\AdmincheckoutactivationlogResource',
            'route_name' => 'adbox.rest.admincheckoutactivationlog',
            'route_identifier_name' => 'admincheckoutactivationlog_id',
            'collection_name' => 'admincheckoutactivationlog',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\AdmincheckoutactivationlogEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\AdmincheckoutactivationlogCollection',
            'service_name' => 'admincheckoutactivationlog',
        ),
        'Adbox\\V1\\Rest\\Checkout\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Checkout\\CheckoutResource',
            'route_name' => 'adbox.rest.checkout',
            'route_identifier_name' => 'checkout_id',
            'collection_name' => 'checkout',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Checkout\\CheckoutEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Checkout\\CheckoutCollection',
            'service_name' => 'checkout',
        ),
        'Adbox\\V1\\Rest\\Checkoutsteps\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Checkoutsteps\\CheckoutstepsResource',
            'route_name' => 'adbox.rest.checkoutsteps',
            'route_identifier_name' => 'checkoutsteps_id',
            'collection_name' => 'checkoutsteps',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Checkoutsteps\\CheckoutstepsEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Checkoutsteps\\CheckoutstepsCollection',
            'service_name' => 'checkoutsteps',
        ),
        'Adbox\\V1\\Rest\\Cedis\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Cedis\\CedisResource',
            'route_name' => 'adbox.rest.cedis',
            'route_identifier_name' => 'cedis_id',
            'collection_name' => 'cedis',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Cedis\\CedisEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Cedis\\CedisCollection',
            'service_name' => 'cedis',
        ),
        'Adbox\\V1\\Rest\\Zipcode\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Zipcode\\ZipcodeResource',
            'route_name' => 'adbox.rest.zipcode',
            'route_identifier_name' => 'zipcode_id',
            'collection_name' => 'zipcode',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(
                0 => 'query',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Zipcode\\ZipcodeEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Zipcode\\ZipcodeCollection',
            'service_name' => 'zipcode',
        ),
        'Adbox\\V1\\Rest\\Adminorders\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminorders\\AdminordersResource',
            'route_name' => 'adbox.rest.adminorders',
            'route_identifier_name' => 'adminorders_id',
            'collection_name' => 'adminorders',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminorders\\AdminordersEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminorders\\AdminordersCollection',
            'service_name' => 'adminorders',
        ),
        'Adbox\\V1\\Rest\\Usersinfo\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Usersinfo\\UsersinfoResource',
            'route_name' => 'adbox.rest.usersinfo',
            'route_identifier_name' => 'usersinfo_id',
            'collection_name' => 'usersinfo',
            'entity_http_methods' => array(
                0 => 'PUT',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Usersinfo\\UsersinfoEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Usersinfo\\UsersinfoCollection',
            'service_name' => 'usersinfo',
        ),
        'Adbox\\V1\\Rest\\Userspassword\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Userspassword\\UserspasswordResource',
            'route_name' => 'adbox.rest.userspassword',
            'route_identifier_name' => 'userspassword_id',
            'collection_name' => 'userspassword',
            'entity_http_methods' => array(
                0 => 'PUT',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Userspassword\\UserspasswordEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Userspassword\\UserspasswordCollection',
            'service_name' => 'userspassword',
        ),
        'Adbox\\V1\\Rest\\Usersdelivery\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Usersdelivery\\UsersdeliveryResource',
            'route_name' => 'adbox.rest.usersdelivery',
            'route_identifier_name' => 'usersdelivery_id',
            'collection_name' => 'usersdelivery',
            'entity_http_methods' => array(
                0 => 'PUT',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Usersdelivery\\UsersdeliveryEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Usersdelivery\\UsersdeliveryCollection',
            'service_name' => 'usersdelivery',
        ),
        'Adbox\\V1\\Rest\\Adminusers\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminusers\\AdminusersResource',
            'route_name' => 'adbox.rest.adminusers',
            'route_identifier_name' => 'adminusers_id',
            'collection_name' => 'adminusers',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'canCheckout',
                1 => 'query',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminusers\\AdminusersEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminusers\\AdminusersCollection',
            'service_name' => 'adminusers',
        ),
        'Adbox\\V1\\Rest\\Messages\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Messages\\MessagesResource',
            'route_name' => 'adbox.rest.messages',
            'route_identifier_name' => 'messages_id',
            'collection_name' => 'messages',
            'entity_http_methods' => array(
                0 => 'PUT',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Messages\\MessagesEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Messages\\MessagesCollection',
            'service_name' => 'messages',
        ),
        'Adbox\\V1\\Rest\\Config\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Config\\ConfigResource',
            'route_name' => 'adbox.rest.config',
            'route_identifier_name' => 'config_id',
            'collection_name' => 'config',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Config\\ConfigEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Config\\ConfigCollection',
            'service_name' => 'config',
        ),
        'Adbox\\V1\\Rest\\Powerbi\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Powerbi\\PowerbiResource',
            'route_name' => 'adbox.rest.powerbi',
            'route_identifier_name' => 'powerbi_id',
            'collection_name' => 'powerbi',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(
                0 => 'query',
                1 => 'token',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Powerbi\\PowerbiEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Powerbi\\PowerbiCollection',
            'service_name' => 'powerbi',
        ),
        'Adbox\\V1\\Rest\\Adminusersinfo\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminusersinfo\\AdminusersinfoResource',
            'route_name' => 'adbox.rest.adminusersinfo',
            'route_identifier_name' => 'adminusersinfo_id',
            'collection_name' => 'adminusersinfo',
            'entity_http_methods' => array(
                0 => 'PUT',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminusersinfo\\AdminusersinfoEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminusersinfo\\AdminusersinfoCollection',
            'service_name' => 'adminusersinfo',
        ),
        'Adbox\\V1\\Rest\\Adminusersdelivery\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminusersdelivery\\AdminusersdeliveryResource',
            'route_name' => 'adbox.rest.adminusersdelivery',
            'route_identifier_name' => 'adminusersdelivery_id',
            'collection_name' => 'adminusersdelivery',
            'entity_http_methods' => array(
                0 => 'PUT',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminusersdelivery\\AdminusersdeliveryEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminusersdelivery\\AdminusersdeliveryCollection',
            'service_name' => 'adminusersdelivery',
        ),
        'Adbox\\V1\\Rest\\Adminroles\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminroles\\AdminrolesResource',
            'route_name' => 'adbox.rest.adminroles',
            'route_identifier_name' => 'adminroles_id',
            'collection_name' => 'adminroles',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminroles\\AdminrolesEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminroles\\AdminrolesCollection',
            'service_name' => 'adminroles',
        ),
        'Adbox\\V1\\Rest\\Adminresource\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminresource\\AdminresourceResource',
            'route_name' => 'adbox.rest.adminresource',
            'route_identifier_name' => 'adminresource_id',
            'collection_name' => 'adminresource',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminresource\\AdminresourceEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminresource\\AdminresourceCollection',
            'service_name' => 'adminresource',
        ),
        'Adbox\\V1\\Rest\\Adminpermissions\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminpermissions\\AdminpermissionsResource',
            'route_name' => 'adbox.rest.adminpermissions',
            'route_identifier_name' => 'adminpermissions_id',
            'collection_name' => 'adminpermissions',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminpermissions\\AdminpermissionsEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminpermissions\\AdminpermissionsCollection',
            'service_name' => 'adminpermissions',
        ),
        'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\AdminpermissionsbuildresourceResource',
            'route_name' => 'adbox.rest.adminpermissionsbuildresource',
            'route_identifier_name' => 'adminpermissionsbuildresource_id',
            'collection_name' => 'adminpermissionsbuildresource',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\AdminpermissionsbuildresourceEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\AdminpermissionsbuildresourceCollection',
            'service_name' => 'adminpermissionsbuildresource',
        ),
        'Adbox\\V1\\Rest\\Adminpermissionsbuild\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminpermissionsbuild\\AdminpermissionsbuildResource',
            'route_name' => 'adbox.rest.adminpermissionsbuild',
            'route_identifier_name' => 'adminpermissionsbuild_id',
            'collection_name' => 'adminpermissionsbuild',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminpermissionsbuild\\AdminpermissionsbuildEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminpermissionsbuild\\AdminpermissionsbuildCollection',
            'service_name' => 'adminpermissionsbuild',
        ),
        'Adbox\\V1\\Rest\\Adminusersupload\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminusersupload\\AdminusersuploadResource',
            'route_name' => 'adbox.rest.adminusersupload',
            'route_identifier_name' => 'adminusersupload_id',
            'collection_name' => 'adminusersupload',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminusersupload\\AdminusersuploadEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminusersupload\\AdminusersuploadCollection',
            'service_name' => 'adminusersupload',
        ),
        'Adbox\\V1\\Rest\\Adminusersuploadlayout\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminusersuploadlayout\\AdminusersuploadlayoutResource',
            'route_name' => 'adbox.rest.adminusersuploadlayout',
            'route_identifier_name' => 'adminusersuploadlayout_id',
            'collection_name' => 'adminusersuploadlayout',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminusersuploadlayout\\AdminusersuploadlayoutEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminusersuploadlayout\\AdminusersuploadlayoutCollection',
            'service_name' => 'adminusersuploadlayout',
        ),
        'Adbox\\V1\\Rest\\Theme\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Theme\\ThemeResource',
            'route_name' => 'adbox.rest.theme',
            'route_identifier_name' => 'theme_id',
            'collection_name' => 'theme',
            'entity_http_methods' => array(
                0 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Theme\\ThemeEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Theme\\ThemeCollection',
            'service_name' => 'theme',
        ),
        'Adbox\\V1\\Rest\\Themepreview\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Themepreview\\ThemepreviewResource',
            'route_name' => 'adbox.rest.themepreview',
            'route_identifier_name' => 'themepreview_id',
            'collection_name' => 'themepreview',
            'entity_http_methods' => array(
                0 => 'POST',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Themepreview\\ThemepreviewEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Themepreview\\ThemepreviewCollection',
            'service_name' => 'themepreview',
        ),
        'Adbox\\V1\\Rest\\Usersorders\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Usersorders\\UsersordersResource',
            'route_name' => 'adbox.rest.usersorders',
            'route_identifier_name' => 'usersorders_id',
            'collection_name' => 'usersorders',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '25',
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Usersorders\\UsersordersEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Usersorders\\UsersordersCollection',
            'service_name' => 'usersorders',
        ),
        'Adbox\\V1\\Rest\\Reports\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Reports\\ReportsResource',
            'route_name' => 'adbox.rest.reports',
            'route_identifier_name' => 'reports_id',
            'collection_name' => 'reports',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Reports\\ReportsEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Reports\\ReportsCollection',
            'service_name' => 'reports',
        ),
        'Adbox\\V1\\Rest\\Usersreports\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Usersreports\\UsersreportsResource',
            'route_name' => 'adbox.rest.usersreports',
            'route_identifier_name' => 'usersreports_id',
            'collection_name' => 'usersreports',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Usersreports\\UsersreportsEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Usersreports\\UsersreportsCollection',
            'service_name' => 'usersreports',
        ),
        'Adbox\\V1\\Rest\\Admindashboard\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admindashboard\\AdmindashboardResource',
            'route_name' => 'adbox.rest.admindashboard',
            'route_identifier_name' => 'admindashboard_id',
            'collection_name' => 'admindashboard',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admindashboard\\AdmindashboardEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admindashboard\\AdmindashboardCollection',
            'service_name' => 'admindashboard',
        ),
        'Adbox\\V1\\Rest\\Admingroups\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Admingroups\\AdmingroupsResource',
            'route_name' => 'adbox.rest.admingroups',
            'route_identifier_name' => 'admingroups_id',
            'collection_name' => 'admingroups',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Admingroups\\AdmingroupsEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Admingroups\\AdmingroupsCollection',
            'service_name' => 'admingroups',
        ),
        'Adbox\\V1\\Rest\\Usersdevices\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Usersdevices\\UsersdevicesResource',
            'route_name' => 'adbox.rest.usersdevices',
            'route_identifier_name' => 'usersdevices_id',
            'collection_name' => 'usersdevices',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Usersdevices\\UsersdevicesEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Usersdevices\\UsersdevicesCollection',
            'service_name' => 'usersdevices',
        ),
        'Adbox\\V1\\Rest\\Adminpush\\Controller' => array(
            'listener' => 'Adbox\\V1\\Rest\\Adminpush\\AdminpushResource',
            'route_name' => 'adbox.rest.adminpush',
            'route_identifier_name' => 'adminpush_id',
            'collection_name' => 'adminpush',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Adbox\\V1\\Rest\\Adminpush\\AdminpushEntity',
            'collection_class' => 'Adbox\\V1\\Rest\\Adminpush\\AdminpushCollection',
            'service_name' => 'adminpush',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Adbox\\V1\\Rest\\Status\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Configpublic\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Configprivate\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Usersreset\\Controller' => 'HalJson',
            'Adbox\\V1\\Rest\\Usersrecovery\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Users\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Products\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminproductssync\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Categories\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admincreditsadjustment\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Banners\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admincreditsresults\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admincreditsresultslayout\\Controller' => 'HalJson',
            'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\Controller' => 'HalJson',
            'Adbox\\V1\\Rest\\Userbalance\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Usercart\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Checkout\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Checkoutsteps\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Cedis\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Zipcode\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminorders\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Usersinfo\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Userspassword\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Usersdelivery\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminusers\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Messages\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Config\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Powerbi\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminusersinfo\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminusersdelivery\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminroles\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminresource\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminpermissions\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminpermissionsbuild\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminusersupload\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminusersuploadlayout\\Controller' => 'HalJson',
            'Adbox\\V1\\Rest\\Theme\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Themepreview\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Usersorders\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Reports\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Usersreports\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admindashboard\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Admingroups\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Usersdevices\\Controller' => 'Json',
            'Adbox\\V1\\Rest\\Adminpush\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'Adbox\\V1\\Rest\\Status\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Configpublic\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Configprivate\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersreset\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersrecovery\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Products\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminproductssync\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Categories\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustment\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Banners\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresults\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultslayout\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Userbalance\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usercart\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Checkout\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Checkoutsteps\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Cedis\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Zipcode\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminorders\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersinfo\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Userspassword\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersdelivery\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusers\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Messages\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Config\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Powerbi\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusersinfo\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusersdelivery\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminroles\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminresource\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminpermissions\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuild\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusersupload\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusersuploadlayout\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Theme\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Themepreview\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersorders\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Reports\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersreports\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admindashboard\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admingroups\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersdevices\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminpush\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Adbox\\V1\\Rest\\Status\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Configpublic\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Configprivate\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersreset\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersrecovery\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Products\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminproductssync\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Categories\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustment\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ),
            'Adbox\\V1\\Rest\\Banners\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresults\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultslayout\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Userbalance\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usercart\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Checkout\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Checkoutsteps\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Cedis\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Zipcode\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminorders\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersinfo\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Userspassword\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersdelivery\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusers\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Messages\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Config\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Powerbi\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusersinfo\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusersdelivery\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminroles\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminresource\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminpermissions\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuild\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminusersupload\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ),
            'Adbox\\V1\\Rest\\Adminusersuploadlayout\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Theme\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Themepreview\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersorders\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Reports\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersreports\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admindashboard\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Admingroups\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Usersdevices\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
            'Adbox\\V1\\Rest\\Adminpush\\Controller' => array(
                0 => 'application/vnd.adbox.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Adbox\\V1\\Rest\\Status\\StatusEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.status',
                'route_identifier_name' => 'status_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Status\\StatusCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.status',
                'route_identifier_name' => 'status_id',
                'is_collection' => true,
            ),
            'StatusLib\\Entity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.status',
                'route_identifier_name' => 'status_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'StatusLib\\Collection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.status',
                'route_identifier_name' => 'status_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Configpublic\\ConfigpublicEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.configpublic',
                'route_identifier_name' => 'configpublic_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Configpublic\\ConfigpublicCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.configpublic',
                'route_identifier_name' => 'configpublic_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Configprivate\\ConfigprivateEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.configprivate',
                'route_identifier_name' => 'configprivate_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
            ),
            'Adbox\\V1\\Rest\\Configprivate\\ConfigprivateCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.configprivate',
                'route_identifier_name' => 'configprivate_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Usersreset\\UsersresetEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersreset',
                'route_identifier_name' => 'usersreset_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Usersreset\\UsersresetCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersreset',
                'route_identifier_name' => 'usersreset_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Usersrecovery\\UsersrecoveryEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersrecovery',
                'route_identifier_name' => 'usersrecovery_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Usersrecovery\\UsersrecoveryCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersrecovery',
                'route_identifier_name' => 'usersrecovery_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Users\\UsersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.users',
                'route_identifier_name' => 'users_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Users\\UsersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.users',
                'route_identifier_name' => 'users_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Products\\ProductsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.products',
                'route_identifier_name' => 'products_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Products\\ProductsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.products',
                'route_identifier_name' => 'products_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminproductssync\\AdminproductssyncEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminproductssync',
                'route_identifier_name' => 'adminproductssync_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminproductssync\\AdminproductssyncCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminproductssync',
                'route_identifier_name' => 'adminproductssync_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Categories\\CategoriesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.categories',
                'route_identifier_name' => 'categories_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Categories\\CategoriesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.categories',
                'route_identifier_name' => 'categories_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustment\\AdmincreditsadjustmentEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsadjustment',
                'route_identifier_name' => 'admincreditsadjustment_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustment\\AdmincreditsadjustmentCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsadjustment',
                'route_identifier_name' => 'admincreditsadjustment_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Banners\\BannersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.banners',
                'route_identifier_name' => 'banners_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Banners\\BannersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.banners',
                'route_identifier_name' => 'banners_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admincreditsresults\\AdmincreditsresultsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsresults',
                'route_identifier_name' => 'admincreditsresults_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresults\\AdmincreditsresultsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsresults',
                'route_identifier_name' => 'admincreditsresults_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultslayout\\AdmincreditsresultslayoutEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsresultslayout',
                'route_identifier_name' => 'admincreditsresultslayout_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultslayout\\AdmincreditsresultslayoutCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsresultslayout',
                'route_identifier_name' => 'admincreditsresultslayout_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\AdmincreditsresultsfilesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsresultsfiles',
                'route_identifier_name' => 'admincreditsresultsfiles_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\AdmincreditsresultsfilesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsresultsfiles',
                'route_identifier_name' => 'admincreditsresultsfiles_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\AdmincreditsadjustmentsettingsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsadjustmentsettings',
                'route_identifier_name' => 'admincreditsadjustmentsettings_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\AdmincreditsadjustmentsettingsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsadjustmentsettings',
                'route_identifier_name' => 'admincreditsadjustmentsettings_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\AdmincreditsadjustmentlayoutEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsadjustmentlayout',
                'route_identifier_name' => 'admincreditsadjustmentlayout_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentlayout\\AdmincreditsadjustmentlayoutCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsadjustmentlayout',
                'route_identifier_name' => 'admincreditsadjustmentlayout_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Userbalance\\UserbalanceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.userbalance',
                'route_identifier_name' => 'userbalance_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Userbalance\\UserbalanceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.userbalance',
                'route_identifier_name' => 'userbalance_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\AdmincreditsadjustmentfilesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsadjustmentfiles',
                'route_identifier_name' => 'admincreditsadjustmentfiles_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\AdmincreditsadjustmentfilesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincreditsadjustmentfiles',
                'route_identifier_name' => 'admincreditsadjustmentfiles_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Usercart\\UsercartEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usercart',
                'route_identifier_name' => 'product_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Usercart\\UsercartCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usercart',
                'route_identifier_name' => 'product_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\AdmincheckoutactivationlogEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincheckoutactivationlog',
                'route_identifier_name' => 'admincheckoutactivationlog_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\AdmincheckoutactivationlogCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admincheckoutactivationlog',
                'route_identifier_name' => 'admincheckoutactivationlog_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Checkout\\CheckoutEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.checkout',
                'route_identifier_name' => 'checkout_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Checkout\\CheckoutCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.checkout',
                'route_identifier_name' => 'checkout_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Checkoutsteps\\CheckoutstepsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.checkoutsteps',
                'route_identifier_name' => 'checkoutsteps_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Checkoutsteps\\CheckoutstepsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.checkoutsteps',
                'route_identifier_name' => 'checkoutsteps_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Cedis\\CedisEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.cedis',
                'route_identifier_name' => 'cedis_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Cedis\\CedisCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.cedis',
                'route_identifier_name' => 'cedis_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Zipcode\\ZipcodeEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.zipcode',
                'route_identifier_name' => 'zipcode_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Zipcode\\ZipcodeCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.zipcode',
                'route_identifier_name' => 'zipcode_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminorders\\AdminordersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminorders',
                'route_identifier_name' => 'adminorders_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminorders\\AdminordersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminorders',
                'route_identifier_name' => 'adminorders_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Usersinfo\\UsersinfoEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersinfo',
                'route_identifier_name' => 'usersinfo_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Usersinfo\\UsersinfoCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersinfo',
                'route_identifier_name' => 'usersinfo_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Userspassword\\UserspasswordEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.userspassword',
                'route_identifier_name' => 'userspassword_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Userspassword\\UserspasswordCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.userspassword',
                'route_identifier_name' => 'userspassword_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Usersdelivery\\UsersdeliveryEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersdelivery',
                'route_identifier_name' => 'usersdelivery_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Usersdelivery\\UsersdeliveryCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersdelivery',
                'route_identifier_name' => 'usersdelivery_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminusers\\AdminusersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusers',
                'route_identifier_name' => 'adminusers_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminusers\\AdminusersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusers',
                'route_identifier_name' => 'adminusers_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Messages\\MessagesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.messages',
                'route_identifier_name' => 'messages_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Messages\\MessagesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.messages',
                'route_identifier_name' => 'messages_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Config\\ConfigEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.config',
                'route_identifier_name' => 'config_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Config\\ConfigCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.config',
                'route_identifier_name' => 'config_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Powerbi\\PowerbiEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.powerbi',
                'route_identifier_name' => 'powerbi_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Powerbi\\PowerbiCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.powerbi',
                'route_identifier_name' => 'powerbi_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminusersinfo\\AdminusersinfoEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusersinfo',
                'route_identifier_name' => 'adminusersinfo_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminusersinfo\\AdminusersinfoCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusersinfo',
                'route_identifier_name' => 'adminusersinfo_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminusersdelivery\\AdminusersdeliveryEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusersdelivery',
                'route_identifier_name' => 'adminusersdelivery_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminusersdelivery\\AdminusersdeliveryCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusersdelivery',
                'route_identifier_name' => 'adminusersdelivery_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminroles\\AdminrolesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminroles',
                'route_identifier_name' => 'adminroles_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminroles\\AdminrolesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminroles',
                'route_identifier_name' => 'adminroles_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminresource\\AdminresourceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminresource',
                'route_identifier_name' => 'adminresource_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminresource\\AdminresourceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminresource',
                'route_identifier_name' => 'adminresource_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminpermissions\\AdminpermissionsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminpermissions',
                'route_identifier_name' => 'adminpermissions_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminpermissions\\AdminpermissionsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminpermissions',
                'route_identifier_name' => 'adminpermissions_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\AdminpermissionsbuildresourceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminpermissionsbuildresource',
                'route_identifier_name' => 'adminpermissionsbuildresource_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\AdminpermissionsbuildresourceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminpermissionsbuildresource',
                'route_identifier_name' => 'adminpermissionsbuildresource_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuild\\AdminpermissionsbuildEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminpermissionsbuild',
                'route_identifier_name' => 'adminpermissionsbuild_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuild\\AdminpermissionsbuildCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminpermissionsbuild',
                'route_identifier_name' => 'adminpermissionsbuild_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminusersupload\\AdminusersuploadEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusersupload',
                'route_identifier_name' => 'adminusersupload_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminusersupload\\AdminusersuploadCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusersupload',
                'route_identifier_name' => 'adminusersupload_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminusersuploadlayout\\AdminusersuploadlayoutEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusersuploadlayout',
                'route_identifier_name' => 'adminusersuploadlayout_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminusersuploadlayout\\AdminusersuploadlayoutCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminusersuploadlayout',
                'route_identifier_name' => 'adminusersuploadlayout_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Theme\\ThemeEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.theme',
                'route_identifier_name' => 'theme_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Theme\\ThemeCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.theme',
                'route_identifier_name' => 'theme_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Themepreview\\ThemepreviewEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.themepreview',
                'route_identifier_name' => 'themepreview_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Themepreview\\ThemepreviewCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.themepreview',
                'route_identifier_name' => 'themepreview_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Usersorders\\UsersordersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersorders',
                'route_identifier_name' => 'usersorders_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Usersorders\\UsersordersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersorders',
                'route_identifier_name' => 'usersorders_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Reports\\ReportsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.reports',
                'route_identifier_name' => 'reports_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Reports\\ReportsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.reports',
                'route_identifier_name' => 'reports_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Usersreports\\UsersreportsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersreports',
                'route_identifier_name' => 'usersreports_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Usersreports\\UsersreportsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersreports',
                'route_identifier_name' => 'usersreports_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admindashboard\\AdmindashboardEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admindashboard',
                'route_identifier_name' => 'admindashboard_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admindashboard\\AdmindashboardCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admindashboard',
                'route_identifier_name' => 'admindashboard_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Admingroups\\AdmingroupsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admingroups',
                'route_identifier_name' => 'admingroups_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Admingroups\\AdmingroupsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.admingroups',
                'route_identifier_name' => 'admingroups_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Usersdevices\\UsersdevicesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersdevices',
                'route_identifier_name' => 'usersdevices_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Usersdevices\\UsersdevicesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.usersdevices',
                'route_identifier_name' => 'usersdevices_id',
                'is_collection' => true,
            ),
            'Adbox\\V1\\Rest\\Adminpush\\AdminpushEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminpush',
                'route_identifier_name' => 'adminpush_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Adbox\\V1\\Rest\\Adminpush\\AdminpushCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'adbox.rest.adminpush',
                'route_identifier_name' => 'adminpush_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-content-validation' => array(
        'Adbox\\V1\\Rest\\Status\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Status\\Validator',
        ),
        'Adbox\\V1\\Rest\\Configpublic\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Configpublic\\Validator',
        ),
        'Adbox\\V1\\Rest\\Configprivate\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Configprivate\\Validator',
        ),
        'Adbox\\V1\\Rest\\Usersreset\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Usersreset\\Validator',
        ),
        'Adbox\\V1\\Rest\\Usersrecovery\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Usersrecovery\\Validator',
        ),
        'Adbox\\V1\\Rest\\Users\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Users\\Validator',
            'PATCH' => 'Adbox\\V1\\Rest\\Users\\PatchValidator',
        ),
        'Adbox\\V1\\Rest\\Banners\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Banners\\Validator',
            'PUT' => 'Adbox\\V1\\Rest\\Banners\\PutValidator',
            'PATCH' => 'Adbox\\V1\\Rest\\Banners\\PatchValidator',
        ),
        'Adbox\\V1\\Rest\\Admincreditsadjustment\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Admincreditsadjustment\\Validator',
        ),
        'Adbox\\V1\\Rest\\Admincreditsresults\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Admincreditsresults\\Validator',
        ),
        'Adbox\\V1\\Rest\\Categories\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Categories\\Validator',
        ),
        'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Validator',
        ),
        'Adbox\\V1\\Rest\\Checkout\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Checkout\\Validator',
        ),
        'Adbox\\V1\\Rest\\Usercart\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Usercart\\Validator',
            'PUT' => 'Adbox\\V1\\Rest\\Usercart\\Validator',
        ),
        'Adbox\\V1\\Rest\\Usersinfo\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Usersinfo\\Validator',
        ),
        'Adbox\\V1\\Rest\\Userspassword\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Userspassword\\Validator',
        ),
        'Adbox\\V1\\Rest\\Usersdelivery\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Usersdelivery\\Validator',
        ),
        'Adbox\\V1\\Rest\\Messages\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Messages\\Validator',
        ),
        'Adbox\\V1\\Rest\\Config\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Config\\Validator',
        ),
        'Adbox\\V1\\Rest\\Adminusersinfo\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Adminusersinfo\\Validator',
        ),
        'Adbox\\V1\\Rest\\Adminusersdelivery\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Adminusersdelivery\\Validator',
        ),
        'Adbox\\V1\\Rest\\Adminroles\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Adminroles\\Validator',
        ),
        'Adbox\\V1\\Rest\\Adminresource\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Adminresource\\Validator',
        ),
        'Adbox\\V1\\Rest\\Adminpermissions\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Adminpermissions\\Validator',
        ),
        'Adbox\\V1\\Rest\\Adminusers\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Adminusers\\Validator',
        ),
        'Adbox\\V1\\Rest\\Adminusersupload\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Adminusersupload\\Validator',
        ),
        'Adbox\\V1\\Rest\\Theme\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Theme\\Validator',
        ),
        'Adbox\\V1\\Rest\\Themepreview\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Themepreview\\Validator',
        ),
        'Adbox\\V1\\Rest\\Reports\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Reports\\Validator',
        ),
        'Adbox\\V1\\Rest\\Admingroups\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Admingroups\\Validator',
        ),
        'Adbox\\V1\\Rest\\Usersdevices\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Usersdevices\\Validator',
        ),
        'Adbox\\V1\\Rest\\Adminpush\\Controller' => array(
            'input_filter' => 'Adbox\\V1\\Rest\\Adminpush\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Adbox\\V1\\Rest\\Status\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '140',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'message',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(mwop|andi|zeev)$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'user',
                'description' => 'The user submitting the status message.',
                'error_message' => 'The user submitting the status message.',
            ),
            2 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Digits',
                        'options' => array(),
                    ),
                ),
                'filters' => array(),
                'name' => 'timestamp',
                'description' => 'The timestamp when the status message was last modified.',
                'error_message' => 'You must provide a timestamp.',
            ),
        ),
        'Adbox\\V1\\Rest\\Configpublic\\Validator' => array(),
        'Adbox\\V1\\Rest\\Configprivate\\Validator' => array(),
        'Adbox\\V1\\Rest\\Usersreset\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/',
                            'message' => 'Invalid Email.',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'identifier',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(),
                    ),
                ),
                'filters' => array(),
                'name' => 'token',
            ),
            2 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '6',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?=.*[a-z])(?=.*[A-Z-0-9]).+$/',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'password',
            ),
        ),
        'Adbox\\V1\\Rest\\Usersrecovery\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\Apigility\\Doctrine\\Server\\Validator\\ObjectExists',
                        'options' => array(
                            'entity_class' => 'Application\\Entity\\OauthUsers',
                            'fields' => 'email',
                            'message' => 'Email no encontrado.',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'identifier',
            ),
        ),
        'Adbox\\V1\\Rest\\Users\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'name',
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'lastname',
            ),
            2 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/',
                            'message' => 'Invalid Email.',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email',
            ),
        ),
        'Adbox\\V1\\Rest\\Users\\PatchValidator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'firstName',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'lastName',
            ),
            2 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'surname',
            ),
            3 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'telephone1',
            ),
            4 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'telephone2',
            ),
            5 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'mobile',
            ),
            6 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/',
                            'message' => 'Invalid Email.',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email',
            ),
            7 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '6',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?=.*[a-z])(?=.*[A-Z-0-9]).+$/',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'password',
            ),
            8 => array(
                'required' => false,
                'name' => 'distributor',
            ),
            9 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '0',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'branchOffice',
            ),
            10 => array(
                'required' => false,
                'name' => 'address',
            ),
        ),
        'Adbox\\V1\\Rest\\Banners\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\MimeType',
                        'options' => array(
                            'mimeType' => 'image/jpeg,image/pjpeg,image/png,image/svg+xml',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\File\\FilesSize',
                        'options' => array(
                            'max' => '900kB',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\File\\RenameUpload',
                        'options' => array(
                            'use_upload_extension' => true,
                            'target' => './data/uploads/',
                            'use_upload_name' => false,
                            'randomize' => true,
                        ),
                    ),
                ),
                'type' => 'Zend\\InputFilter\\FileInput',
                'name' => 'file',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                            'min' => '1',
                        ),
                    ),
                    1 => array(
                        'name' => 'ZF\\Apigility\\Doctrine\\Server\\Validator\\NoObjectExists',
                        'options' => array(
                            'entity_class' => 'Application\\Entity\\CoreBanners',
                            'fields' => 'id',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'id',
            ),
            2 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                            'min' => '1',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'title',
            ),
            3 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                            'min' => '1',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'description',
            ),
            4 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '2047',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowAbsolute' => true,
                            'allowRelative' => true,
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'href',
            ),
            5 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'misc',
            ),
        ),
        'Adbox\\V1\\Rest\\Banners\\PutValidator' => array(
            0 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\MimeType',
                        'options' => array(
                            'mimeType' => 'image/jpeg,image/pjpeg,image/png,image/svg+xml',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\File\\FilesSize',
                        'options' => array(
                            'max' => '900kB',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\File\\RenameUpload',
                        'options' => array(
                            'use_upload_extension' => true,
                            'target' => './public/img/banners',
                            'use_upload_name' => false,
                            'randomize' => true,
                        ),
                    ),
                ),
                'type' => 'Zend\\InputFilter\\FileInput',
                'name' => 'file',
            ),
            1 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                            'min' => '1',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'title',
            ),
            2 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                            'min' => '1',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'description',
            ),
            3 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '2047',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowAbsolute' => true,
                            'allowRelative' => true,
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'href',
            ),
            4 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'misc',
            ),
        ),
        'Adbox\\V1\\Rest\\Banners\\PatchValidator' => array(
            0 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'file',
            ),
        ),
        'Adbox\\V1\\Rest\\Admincreditsadjustment\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\Size',
                        'options' => array(
                            'max' => '10000kB',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\File\\MimeType',
                        'options' => array(
                            'mimeType' => 'text/plain,text/csv',
                            'message' => 'Formato de Archivo InvÃ¡lido.',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\File\\RenameUpload',
                        'options' => array(
                            'randomize' => true,
                            'use_upload_extension' => true,
                            'use_upload_name' => true,
                            'target' => './public/csv/',
                        ),
                    ),
                ),
                'name' => 'csv',
                'type' => 'Zend\\InputFilter\\FileInput',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'description',
            ),
            2 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'ZF\\Apigility\\Doctrine\\Server\\Validator\\ObjectExists',
                        'options' => array(
                            'entity_class' => 'Application\\Entity\\CoreFileUploadReasons',
                            'fields' => 'id',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'reasonId',
            ),
            3 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(),
                    ),
                ),
                'filters' => array(),
                'name' => 'fileType',
            ),
        ),
        'Adbox\\V1\\Rest\\Admincreditsresults\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\FilesSize',
                        'options' => array(
                            'max' => '2048kB',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\File\\MimeType',
                        'options' => array(
                            'mimeType' => 'text/plain,text/csv',
                            'message' => 'Formato invalido',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\File\\RenameUpload',
                        'options' => array(
                            'target' => './data/upload/csv/',
                            'randomize' => true,
                            'use_upload_extension' => true,
                            'use_upload_name' => true,
                        ),
                    ),
                ),
                'name' => 'csv',
                'type' => 'Zend\\InputFilter\\FileInput',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'description',
            ),
        ),
        'Adbox\\V1\\Rest\\Categories\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\MimeType',
                        'options' => array(
                            'mimeType' => 'image/jpeg,image/pjpeg,image/png,image/svg+xml',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\File\\FilesSize',
                        'options' => array(
                            'max' => '900kB',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\File\\RenameUpload',
                        'options' => array(
                            'randomize' => true,
                            'target' => './public/img/categories/',
                            'use_upload_extension' => true,
                            'use_upload_name' => true,
                        ),
                    ),
                ),
                'name' => 'file',
                'type' => 'Zend\\InputFilter\\FileInput',
            ),
        ),
        'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Validator' => array(
            0 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\ToInt',
                        'options' => array(),
                    ),
                ),
                'name' => 'date.start',
            ),
            1 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\ToInt',
                        'options' => array(),
                    ),
                ),
                'name' => 'date.end',
            ),
        ),
        'Adbox\\V1\\Rest\\Checkout\\Validator' => array(
            0 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'answers',
            ),
            1 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'userAddress',
            ),
            2 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'cedis',
            ),
        ),
        'Adbox\\V1\\Rest\\Usercart\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\NotEmpty',
                        'options' => array(
                            'message' => 'Item_Quantity_Empty',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'quantity',
            ),
        ),
        'Adbox\\V1\\Rest\\Usersinfo\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '255',
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'firstName',
                'description' => 'Nombre(s)',
                'field_type' => 'string',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'lastName',
                'description' => 'Apellido paterno',
                'field_type' => 'string',
            ),
            2 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email',
                'field_type' => 'string',
                'description' => 'Correo electrÃ³nico',
            ),
            3 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'surname',
                'description' => 'Apellido materno',
                'field_type' => 'string',
            ),
            4 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'telephone1',
                'description' => 'NÃºmero telefÃ³nico',
                'field_type' => 'string',
            ),
            5 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'telephone2',
                'description' => 'NÃºmero telefÃ³nico (otro)',
                'field_type' => 'string',
            ),
            6 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'mobile',
                'field_type' => 'string',
                'description' => 'TelÃ©fono MÃ³vil',
            ),
            7 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'password',
            ),
        ),
        'Adbox\\V1\\Rest\\Userspassword\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '6',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?=.*[a-z])(?=.*[A-Z-0-9]).+$/',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'password',
                'field_type' => 'string',
                'description' => 'Nueva contraseÃ±a',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '6',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'currentPassword',
                'field_type' => 'string',
                'description' => 'ContraseÃ±a anterior',
            ),
        ),
        'Adbox\\V1\\Rest\\Usersdelivery\\Validator' => array(
            0 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'cedis',
                'allow_empty' => false,
                'field_type' => 'array',
                'description' => 'Datos del Centro de DistribuciÃ³n',
            ),
            1 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'address',
                'description' => 'Datos de la direcciÃ³n',
                'field_type' => 'array',
            ),
        ),
        'Adbox\\V1\\Rest\\Messages\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '1',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'value',
                'description' => 'Nuevo contenido del mensaje',
                'field_type' => 'string',
            ),
        ),
        'Adbox\\V1\\Rest\\Config\\Validator' => array(
            0 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(open|closed)$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'auth.registration',
                'error_message' => 'Invalid_Auth_Registration',
                'field_type' => 'string',
                'description' => 'Modo de <b>registro pÃºblico</b>:<br>
<ol>
  <li><b>open:</b> Habilita el registro al pÃºblico.</li>
  <li><b>closed:</b> Cierra el registro al pÃºblico.</li>
</ol>',
            ),
            1 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowRelative' => false,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'client.url',
                'error_message' => 'Invalid_Client_Url',
                'field_type' => 'string',
                'description' => 'URL del <b>cliente web pÃºblico</b> para la generaciÃ³n de cookies seguras.<br>
<ol>
  <li>Protocolo <b>http</b>|<b>https</b>.</li>
</ol>',
            ),
            2 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'catalog.motivale.id',
                'error_message' => 'Invalid_Catalog_Motivale_Id',
                'field_type' => 'integer',
                'description' => 'ID del <b>catÃ¡logo de motivale</b>.<br>
<ol>
   <li>Utilizado en la <b>sincronizaciÃ³n de productos</b>.</li>
</ol>',
            ),
            3 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(cedis|user-address)$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'checkout.delivery.mode',
                'error_message' => 'Invalid_Checkout_Delivery_Mode',
                'field_type' => 'string',
                'description' => '<b>Modo de entrega</b> de los productos para el PMR:<br>
<ol>
  <li><b>cedis:</b> Entregas a Centros de DistribuciÃ³n.</li>
  <li><b>user-address:</b> Entregas a direcciÃ³n del usuario.</li>
</ol>',
            ),
            4 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(editable|readOnly)$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'checkout.delivery.userEditable',
                'error_message' => 'Invalid_Checkout_Delivery_UserEditable',
                'field_type' => 'string',
                'description' => 'Permite la <b>ediciÃ³n</b> de los datos de <b>entrega</b>.<br>
La ediciÃ³n dependerÃ¡ de la configuraciÃ³n de direcciÃ³n de entrega (Centro de DistribuciÃ³n, direcciÃ³n del usuario).<br>
<ol>
  <li><b>editable:</b> Habilita la ediciÃ³n de direcciÃ³n de entrega.</li>
  <li><b>readOnly:</b> Deshabilita la ediciÃ³n.</li>
</ol>',
            ),
            5 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(1|0)$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'survey.isMandatory',
                'error_message' => 'Invalid_Survey_IsMandatory',
                'description' => 'Habilita la <b>encuesta de satisfacciÃ³n</b> al realizar el checkout.<br>
<ol>
  <li><b>1:</b> La encuesta serÃ¡ solicitada.</li>
  <li><b>0:</b> Deshabilita la encuesta.</li>
</ol>',
                'field_type' => 'integer',
            ),
            6 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\Alnum',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'client.id',
                'error_message' => 'Invalid_Client_Id',
                'field_type' => 'string',
                'description' => 'ID del <b>PMR</b>.<br>
<ol>
   <li>Usado para <b>integraciÃ³n</b> con terceros y <b>reportes</b>.</li>
</ol>',
            ),
            7 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'client.name',
                'error_message' => 'Invalid_Client_Name',
                'description' => 'Nombre del <b>PMR</b>.<br>
<ol>
   <li>Utlizado como <b>branding</b> del sistema.</li>
</ol>',
                'field_type' => 'string',
            ),
            8 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email.account.from',
                'error_message' => 'Invalid_Email_From',
                'field_type' => 'string',
                'description' => '<br><ol><li><b>Remitente</b> de los correos del sistema.</li></ol>',
            ),
            9 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email.setting.host',
                'error_message' => 'Invalid_Email_Host',
                'field_type' => 'string',
                'description' => '<br><ol><li><b>URL</b> del host SMTP.</li></ol>',
            ),
            10 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\Between',
                        'options' => array(
                            'min' => '1',
                            'max' => '65535',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email.setting.port',
                'error_message' => 'Invalid_Email_Port',
                'field_type' => 'integer',
                'description' => '<br><ol><li><b>Puerto</b> SMTP.</li></ol>',
            ),
            11 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email.setting.connectionConfig.username',
                'error_message' => 'Invalid_Email_User',
                'description' => '<br><ol><li>Nombre de <b>usuario</b> del SMTP.</li></ol>',
                'field_type' => 'string',
            ),
            12 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '6',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'email.setting.connectionConfig.password',
                'error_message' => 'Invalid_Email_Password',
                'field_type' => 'string',
                'description' => '<br><ol><li><b>ContraseÃ±a</b> del SMTP.</li></ol>',
            ),
            13 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowRelative' => false,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'jira.collectorScript',
                'error_message' => 'Invalid_Jira_CollectorScript',
                'description' => '<b>URL</b> del script de <b>JIRA</b>.<br>
<ol>
   <li>Utilizado en el <b>levantamiento</b> de <b>tickets</b>.</li>
</ol>',
                'field_type' => 'string',
            ),
            14 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '1',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'jira.components',
                'error_message' => 'Invalid_Jira_Components',
                'field_type' => 'string',
                'description' => 'Valor de la clave <b>jira.params.components</b>.<br>
<ol>
   <li>Clave enviada al script de <b>JIRA</b> durante el arranque.</li>
</ol>',
            ),
            15 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^UA-[0-9]+-[0-9]+$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'google.analytics.trackingId',
                'error_message' => 'Invalid_Google_Analytics_TrackingId',
                'field_type' => 'string',
                'description' => '<br><ol><li><b>ID</b> del seguimiento con <b>Google Analytics</b>.</li></ol>',
            ),
            16 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'adcinema.project.id',
                'error_message' => 'Invalid_Adcinema_Project_Id',
                'field_type' => 'integer',
                'description' => '<br><ol><li><b>ID</b> del proyecto con <b>AdCinema</b>.</li></ol>',
            ),
            17 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowRelative' => false,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'adcinema.server.development.url',
                'error_message' => 'Invalid_Adcinema_Server_Development_Url',
                'description' => '<br><ol><li><b>URL</b> de <b>pruebas</b> del componente <b>AdCinema</b>.</li></ol>',
                'field_type' => 'string',
            ),
            18 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowRelative' => false,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'adcinema.server.production.url',
                'error_message' => 'Invalid_Adcinema_Server_Production_Url',
                'description' => '<br><ol><li><b>URL</b> de <b>producciÃ³n</b> del componente <b>AdCinema</b>.</li></ol>',
                'field_type' => 'string',
            ),
            19 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email.account.fromname',
                'description' => '<br><ol><li><b>Nombre</b> del <b>remitente</b> de los correos del sistema.</li></ol>',
                'field_type' => 'string',
            ),
            20 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowRelative' => false,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'social.facebook',
                'field_type' => 'string',
                'description' => '<br><ol><li>URL de <b>Facebook</b>.</li></ol>',
            ),
            21 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowRelative' => false,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'social.twitter',
                'description' => '<br><ol><li>URL de <b>Twitter</b>.</li></ol>',
                'field_type' => 'string',
            ),
            22 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowRelative' => false,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'social.youtube',
                'description' => '<br><ol><li>URL de <b>YouTube</b>.</li></ol>',
                'field_type' => 'string',
            ),
            23 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Uri',
                        'options' => array(
                            'allowRelative' => false,
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'social.linkedin',
                'description' => '<br><ol><li>URL de <b>LinkedIn</b>.</li></ol>',
                'field_type' => 'string',
            ),
            24 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(1|0)$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'modal.home',
            ),
            25 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(1|0)$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'modal.products',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminusersinfo\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'firstName',
                'description' => 'Nombre(s)',
                'field_type' => 'string',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'lastName',
                'description' => 'Apellido paterno',
                'field_type' => 'string',
            ),
            2 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email',
                'description' => 'Correo electrÃ³nico',
                'field_type' => 'string',
            ),
            3 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'surname',
                'description' => 'Apellido materno',
                'field_type' => 'string',
            ),
            4 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'telephone1',
                'description' => 'NÃºmero telefÃ³nico',
                'field_type' => 'string',
            ),
            5 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'telephone2',
                'description' => 'NÃºmero telefÃ³nico (otro)',
                'field_type' => 'string',
            ),
            6 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'max' => '10',
                            'min' => '10',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'mobile',
                'description' => 'TelÃ©fono MÃ³vil',
                'field_type' => 'string',
            ),
            7 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\ToInt',
                        'options' => array(),
                    ),
                ),
                'name' => 'roleId',
                'description' => 'ID del rol',
                'field_type' => 'integer',
            ),
            8 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Boolean',
                        'options' => array(),
                    ),
                ),
                'name' => 'profileFulfilled',
                'continue_if_empty' => true,
                'field_type' => 'boolean',
                'description' => 'Perfil completado',
            ),
            9 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Boolean',
                        'options' => array(),
                    ),
                ),
                'name' => 'enabled',
                'continue_if_empty' => true,
                'description' => 'Usuario activo',
                'field_type' => 'boolean',
            ),
            10 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\ToInt',
                        'options' => array(),
                    ),
                ),
                'name' => 'groupId',
                'field_type' => 'integer',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminuserspassword\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '6',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?=.*[a-z])(?=.*[A-Z-0-9]).+$/',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'password',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminusersdelivery\\Validator' => array(
            0 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'cedis',
            ),
            1 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'address',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminroles\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'role',
            ),
            1 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'parentid',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminresource\\Validator' => array(
            0 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'parentid',
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'alias',
            ),
            2 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'resource',
            ),
            3 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'methodhttp',
            ),
            4 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'description',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminpermissions\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'roleid',
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'resourceid',
            ),
            2 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'permission',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminusers\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                            'message' => 'El texto no debe ser menor a 3 caracteres',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'firstName',
                'description' => 'Nombre(s)',
                'field_type' => 'string',
            ),
            1 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                            'message' => 'El texto no debe ser menor a 3 caracteres',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'lastName',
                'description' => 'Apellido paterno',
                'field_type' => 'string',
            ),
            2 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/',
                            'message' => 'El formato del correo no es válido',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'email',
                'description' => 'Correo electrÃ³nico',
                'field_type' => 'string',
            ),
            3 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '255',
                            'message' => 'El texto no debe ser menor a 3 caracteres',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'surname',
                'description' => 'Apellido materno',
                'field_type' => 'string',
            ),
            4 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                            'message' => 'El número debe contener 10 dígitos',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'telephone1',
                'description' => 'NÃºmero telefÃ³nico',
                'field_type' => 'string',
            ),
            5 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                            'message' => 'El número debe contener 10 dígitos',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'telephone2',
                'description' => 'NÃºmero telefÃ³nico (otro)',
                'field_type' => 'string',
            ),
            6 => array(
                'required' => false,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '10',
                            'max' => '10',
                            'message' => 'El número debe contener 10 dígitos',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'mobile',
                'description' => 'TelÃ©fono MÃ³vil',
                'field_type' => 'string',
            ),
            7 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\I18n\\Validator\\IsInt',
                        'options' => array(
                            'message' => 'El ID de perfil es inválido',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\ToInt',
                        'options' => array(),
                    ),
                ),
                'name' => 'roleId',
                'description' => 'ID del rol',
                'field_type' => 'integer',
            ),
            8 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '6',
                            'message' => 'La contraseña debe contener al menos 6 caracteres',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\Regex',
                        'options' => array(
                            'pattern' => '/^(?=.*[a-z])(?=.*[A-Z-0-9]).+$/',
                            'message' => 'El formato de la contraseña no es válido',
                        ),
                    ),
                ),
                'filters' => array(),
                'name' => 'password',
                'field_type' => 'string',
                'description' => 'ContraseÃ±a',
            ),
            9 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'cedis',
                'description' => 'Datos del Centro de DistribuciÃ³n',
                'field_type' => 'array',
            ),
            10 => array(
                'required' => false,
                'validators' => array(),
                'filters' => array(),
                'name' => 'address',
                'description' => 'Datos de la direcciÃ³n',
                'field_type' => 'array',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminusersupload\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\File\\FilesSize',
                        'options' => array(
                            'max' => '2048kB',
                        ),
                    ),
                    1 => array(
                        'name' => 'Zend\\Validator\\File\\MimeType',
                        'options' => array(
                            'message' => 'Formato_Invalido',
                            'mimeType' => 'text/plain,text/csv',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\File\\RenameUpload',
                        'options' => array(
                            'randomize' => true,
                            'target' => './data/upload/csv/',
                            'use_upload_extension' => true,
                            'use_upload_name' => true,
                        ),
                    ),
                ),
                'name' => 'csv',
                'type' => 'Zend\\InputFilter\\FileInput',
            ),
        ),
        'Adbox\\V1\\Rest\\Theme\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'theme.configuration',
            ),
        ),
        'Adbox\\V1\\Rest\\Themepreview\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'theme.configuration',
            ),
        ),
        'Adbox\\V1\\Rest\\Reports\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'id',
                'description' => 'Identificador del reporte',
                'continue_if_empty' => false,
                'field_type' => 'string',
                'allow_empty' => false,
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'layout',
                'description' => 'Arreglo de columnas del reporte a generar',
                'field_type' => 'array',
            ),
        ),
        'Adbox\\V1\\Rest\\Usersgroups\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'name',
                'description' => 'Nombre grupo',
                'field_type' => 'string',
            ),
        ),
        'Adbox\\V1\\Rest\\Adminusersgroups\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'name',
                'description' => 'Group name',
            ),
        ),
        'Adbox\\V1\\Rest\\Admingroups\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '3',
                            'max' => '50',
                            'message' => 'El texto debe contener entre 3 y 50 caracteres',
                        ),
                    ),
                ),
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'name' => 'name',
                'description' => 'Group name',
                'field_type' => 'string',
            ),
        ),
        'Adbox\\V1\\Rest\\Usersdevices\\Validator' => array(),
        'Adbox\\V1\\Rest\\Adminpush\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'title',
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'group',
            ),
            2 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'message',
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'Adbox-V1-Rest-Status-Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Configpublic\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Configprivate\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Usersrecovery\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Usersreset\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Users\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminproductssync\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Categories\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Products\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustment\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Banners\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Admincreditsadjustmentfiles\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Checkoutsteps\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Checkout\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Cedis\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Zipcode\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Usersinfo\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Userspassword\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminorders\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Usersdelivery\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminusers\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Messages\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Config\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Powerbi\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminusersinfo\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminusersdelivery\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminroles\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminresource\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminpermissions\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminpermissionsbuild\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminusersupload\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminusersuploadlayout\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Theme\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Themepreview\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Usersorders\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Reports\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Usersreports\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Admindashboard\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Admingroups\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'Adbox\\V1\\Rest\\Usersdevices\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Adminpush\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Adbox\\V1\\Rest\\Admincreditsresults\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
        ),
    ),
);
