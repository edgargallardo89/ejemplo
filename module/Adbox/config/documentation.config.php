<?php
return [
    'Adbox\\V1\\Rest\\Users\\Controller' => [
        'description' => 'User\'s endpoint',
        'collection' => [
            'GET' => [
                'response' => '{
   "_links": {
       "self": {
           "href": "/users"
       },
       "first": {
           "href": "/users?page={page}"
       },
       "prev": {
           "href": "/users?page={page}"
       },
       "next": {
           "href": "/users?page={page}"
       },
       "last": {
           "href": "/users?page={page}"
       }
   }
   "_embedded": {
       "users": [
           {
               "_links": {
                   "self": {
                       "href": "/users[/:users_id]"
                   }
               }
              "name": "",
              "lastname": "",
              "email": ""
           }
       ]
   }
}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Admincreditsadjustmentsettings\\Controller' => [
        'description' => 'Este endpoint contiene los datos (dinámicos) que se requieren para configurar la subida de archivo (CSV) de correcciones, ajustes y puntos extra.',
        'collection' => [
            'GET' => [
                'description' => 'Obtiene todos los valores de Application\\Entity\\CoreFileUploadReasons (core_file_upload_reasons), y las constantes TYPE_* de la clase Application\\Entity\\CoreFileUpload (core_file_uploads), en forma de key-value.',
                'response' => '{
	"reasons": [{
		"1": "Resultados incompletos"
	}, {
		"2": "Correcciones varias"
	}, {
		"3": "Puntos extra"
	}],
	"fileType": {
		"TYPE_ADJUSTMENTS": "adjustments",
		"TYPE_CORRECTIONS": "corrections",
		"TYPE_EXTRAS": "extras",
		"TYPE_RESULTS": "results"
	}
}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Admincreditsresults\\Controller' => [
        'collection' => [
            'description' => 'CRUD para modulo de carga de resultados',
            'GET' => [
                'description' => 'devuelve lista de resultados procesados',
                'response' => '200 OK
Date:  Tue, 22 Nov 2016 23:29:11 GMT
Server:  Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By:  PHP/7.0.8
Content-Length:  245
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"data":{"0":{"id":5,"mes":1,"anio":2016,"ventas":12,"cuota":100,"puntos":100,"userId":"testuser"},"1":{"id":6,"mes":2,"anio":2016,"ventas":100,"cuota":100,"puntos":10,"userId":"testuser"}},"page_count":1,"page_size":25,"total_items":2,"page":1}',
            ],
            'POST' => [
                'description' => 'carga archivo de resultados',
                'request' => 'POST /adbox-api/public/admin/credits/results HTTP/1.1
Host: localhost
User-Agent: curl/7.47.0
Accept: */*
Authorization: Bearer d17aae914e3cd3e26cd9402e4c9402e5463ba426
Content-Length: 469
Expect: 100-continue
Content-Type: multipart/form-data; boundary=------------------------80c65e54b46204c4

HTTP/1.1 100 Continue',
                'response' => 'HTTP/1.1 201 Created
Date: Wed, 23 Nov 2016 00:20:30 GMT
Perl/v5.16.3
Server: Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By: PHP/7.0.8
Location: http://localhost/adbox-api/public/admin/credits/results
Content-Location: http://localhost/adbox-api/public/admin/credits/results
Content-Length: 21
Content-Type: application/json; charset=utf-8

{"message":"success"}

Error parcial:

{"message":"partial_success","errorCount":1,"logFileUrl":"http:\\/\\/localhost\\/adbox-api\\/public\\/logs\\/pmr_rules_5834e1f96d07b.csv.log"}',
            ],
        ],
        'description' => 'carga de resultados',
    ],
    'Adbox\\V1\\Rest\\Admincreditsresultslayout\\Controller' => [
        'collection' => [
            'description' => 'layput de pmr_rules',
            'GET' => [
                'description' => 'descarga layout en csv',
                'response' => 'GET /admin/credits/results/layout
Accept: application/json
Authorization: Bearer d17aae914e3cd3e26cd9402e4c9402e5463ba426

 -- response --
200 OK
Date:  Wed, 23 Nov 2016 01:08:05 GMT
Server:  Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By:  PHP/7.0.8
Content-Disposition:  attachment; filename="pmr_rules.csv"
Content-Length:  37
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  text/plain; charset=us-ascii

user_id,mes,anio,ventas,cuota,puntos',
            ],
        ],
        'description' => 'carga de resultados - descarga layout en csv',
    ],
    'Adbox\\V1\\Rest\\Categories\\Controller' => [
        'description' => 'A través de este endpoint es posible:
1) obtener las información de las categorías existentes creadas desde el servicio de Motivale, y
2) configurar una imagen personalizada para cada una de ellas.',
        'collection' => [
            'GET' => [
                'description' => 'Obtiene todas las categorías existentes (sin paginado)',
                'response' => '[{
	"id": 1,
	"name": "Test Category 1",
	"description": null,
	"fileName": "http:\\/\\/localhost:8090\\/img\\/categories\\/a_5835bfb711a08.jpg",
	"sort": 1,
	"createdAt": "2016-11-10 10:23:30",
	"modifiedAt": "2016-11-23 10:11:35",
	"productCount": 1
}, {
	"id": 2,
	"name": "Test Category 2",
	"description": null,
	"fileName": "http:\\/\\/localhost:8090\\/img\\/categories\\/a_582e284549f59.jpg",
	"sort": 1,
	"createdAt": "2016-11-10 10:23:30",
	"modifiedAt": "2016-11-17 15:59:33",
	"productCount": 2
}, {
	"id": 3,
	"name": "Test Category 3",
	"description": null,
	"fileName": null,
	"sort": 1,
	"createdAt": "2016-11-10 10:23:30",
	"modifiedAt": "2016-11-10 10:23:30",
	"productCount": 1
}]',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Banners\\Controller' => [
        'description' => 'A través de este endpoint es posible: 1) Crear banners para configurar componentes como sliders, banners (propiamente dichos) y demás que requieran imágenes 2) Obtener todos los banners (imágenes) y su metadata.',
    ],
    'Adbox\\V1\\Rest\\Admincreditsresultsfiles\\Controller' => [
        'collection' => [
            'description' => 'archivos cargados',
            'GET' => [
                'description' => 'lista archivos cargados',
                'response' => 'GET /admin/credits/results/1/files
Accept: application/json
Authorization: Bearer d17aae914e3cd3e26cd9402e4c9402e5463ba426

 -- response --
200 OK
Date:  Wed, 23 Nov 2016 01:00:54 GMT
Server:  Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By:  PHP/7.0.8
Content-Disposition:  attachment; filename="pmr_rules_5834dca2c4607.csv"
Content-Length:  65
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  text/plain; charset=us-ascii

user_id,mes,anio,ventas,cuota,puntos
testuser,1,2017,100,100,100',
            ],
        ],
        'entity' => [
            'description' => 'archivos cargados',
            'GET' => [
                'description' => 'descargar archivo por id',
                'response' => 'GET /admin/credits/results/files
Accept: application/json
Authorization: Bearer d17aae914e3cd3e26cd9402e4c9402e5463ba426

 -- response --

200 OK
Date:  Wed, 23 Nov 2016 01:00:54 GMT
Server:  Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By:  PHP/7.0.8
Content-Disposition:  attachment; filename="pmr_rules_5834dca2c4607.csv"
Content-Length:  65
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  text/plain; charset=us-ascii

user_id,mes,anio,ventas,cuota,puntos
testuser,1,2017,100,100,100',
            ],
        ],
        'description' => 'carga de resultados - lista archivos cargados',
    ],
    'Adbox\\V1\\Rest\\Admincheckoutactivationlog\\Controller' => [
        'description' => 'Abre canje de forma manual',
        'collection' => [
            'GET' => [
                'description' => 'verifica el estutus del canje',
                'response' => 'GET http://sandbox-api.devadventa.com/admin/checkout/status
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Tue, 29 Nov 2016 17:23:47 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  48
Keep-Alive:  timeout=5, max=98
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"enabled":true,"version":"29-11-2016 11:23:41"}',
            ],
            'POST' => [
                'description' => 'crea un evento de apertura o cierre de canje',
                'request' => 'cerrar

POST http://sandbox-api.devadventa.com/admin/checkout/status
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"enabled":0}

abrir

POST http://sandbox-api.devadventa.com/admin/checkout/status
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"enabled":1}',
                'response' => 'Cerrar
201 Created
Date:  Tue, 29 Nov 2016 17:29:33 GMT
Server:  Apache/2.4.18 (Ubuntu)
Location:  http://sandbox-api.devadventa.com/admin/checkout/status
Content-Location:  http://sandbox-api.devadventa.com/admin/checkout/status
Content-Length:  17
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"enabled":false}

Abrir
201 Created
Date:  Tue, 29 Nov 2016 17:30:05 GMT
Server:  Apache/2.4.18 (Ubuntu)
Location:  http://sandbox-api.devadventa.com/admin/checkout/status
Content-Location:  http://sandbox-api.devadventa.com/admin/checkout/status
Content-Length:  16
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"enabled":true}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Checkoutsteps\\Controller' => [
        'description' => 'muestra settings de checkout',
        'collection' => [
            'description' => 'settings de checkout',
            'GET' => [
                'response' => '{"settings":[{"checkout.delivery.mode":"none","survey.isMandatory":"0"}]}',
                'description' => 'GET http://localhost/adbox-api/public/checkout/steps
Accept: application/json
Authorization: Bearer 01c7e9dbdf61a8d8e5f4c357fadfb1968bfe86bc

 -- response --
200 OK
Date:  Fri, 02 Dec 2016 00:04:52 GMT
Server:  Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By:  PHP/7.0.8
Content-Length:  73
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"settings":[{"checkout.delivery.mode":"none","survey.isMandatory":"0"}]}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Checkout\\Controller' => [
        'description' => 'canje de premios',
        'collection' => [
            'POST' => [
                'description' => 'genera orden',
                'request' => 'POST http://localhost/adbox-api/public/checkout
Accept: application/json
Authorization: Bearer 01c7e9dbdf61a8d8e5f4c357fadfb1968bfe86bc
Content-Type: application/json
{"userAddress":{"street":"fsdfsd","extNumber":"ffsdfsdf","intNumber":"fdsdfsdf","zipCode":"dfsdfsdf","reference":"sdfsdfsdfsfsd","location":"dfsdfsdfd","city":"sdfsdf","town":"dsfadfssdf","state":"sdfsdfsdfsdf"}}',
                'response' => '201 Created
Date:  Fri, 02 Dec 2016 00:37:21 GMT
Server:  Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By:  PHP/7.0.8
Location:  http://localhost/adbox-api/public/checkout
Content-Location:  http://localhost/adbox-api/public/checkout
Content-Length:  14
Keep-Alive:  timeout=5, max=98
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"orderId":47}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Cedis\\Controller' => [
        'description' => 'Obtiene cedis de PMR',
        'collection' => [
            'description' => 'lista de cedis',
            'GET' => [
                'description' => 'lista de cedis',
                'response' => '[{"id":1,"cedisId":120,"name":"MILENIO MOTORS"},{"id":2,"cedisId":121,"name":"XXXX MOTORS"},{"id":3,"cedisId":122,"name":"YYY MOTORS"}]',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Zipcode\\Controller' => [
        'description' => 'Obtiene Codigos postales',
        'collection' => [
            'description' => 'busca con el parametro query',
            'GET' => [
                'description' => 'busca con el parametro query',
                'response' => 'GET http://localhost/adbox-api/public/zipcode?query=45608
Accept: application/json
Authorization: Bearer 01c7e9dbdf61a8d8e5f4c357fadfb1968bfe86bc

 -- response --
200 OK
Date:  Sun, 04 Dec 2016 16:04:21 GMT
Server:  Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By:  PHP/7.0.8
Content-Length:  899
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

[{"id":59179,"zipCode":"45608","location":"Loma Bonita Ejidal","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59180,"zipCode":"45608","location":"Tecnol\\u00f3gico","type":"Parque industrial","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59181,"zipCode":"45608","location":"Cerro Del Tesoro","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59182,"zipCode":"45608","location":"Mirador del Tesoro","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59183,"zipCode":"45608","location":"El S\\u00e1uz","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59184,"zipCode":"45608","location":"Parques de Colon","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"}]',
            ],
        ],
        'entity' => [
            'description' => 'busca el código postal especifico',
            'GET' => [
                'description' => 'busca el código postal especifico',
                'response' => 'GET http://localhost/adbox-api/public/zipcode/45608
Accept: application/json
Authorization: Bearer 01c7e9dbdf61a8d8e5f4c357fadfb1968bfe86bc

 -- response --
200 OK
Date:  Sun, 04 Dec 2016 16:11:50 GMT
Server:  Apache/2.4.18 (Unix) OpenSSL/1.0.2h PHP/7.0.8 mod_perl/2.0.8-dev Perl/v5.16.3
X-Powered-By:  PHP/7.0.8
Content-Length:  899
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

[{"id":59179,"zipCode":"45608","location":"Loma Bonita Ejidal","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59180,"zipCode":"45608","location":"Tecnol\\u00f3gico","type":"Parque industrial","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59181,"zipCode":"45608","location":"Cerro Del Tesoro","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59182,"zipCode":"45608","location":"Mirador del Tesoro","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59183,"zipCode":"45608","location":"El S\\u00e1uz","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"},{"id":59184,"zipCode":"45608","location":"Parques de Colon","type":"Colonia","town":"San Pedro Tlaquepaque","city":"Tlaquepaque","state":"Jalisco"}]',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Adminorders\\Controller' => [
        'collection' => [
            'POST' => [
                'request' => '{"userId":30,"products":[{"id":31780,"quantity":1},{"id":31781,"quantity":2}]}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Adminusersdelivery\\Controller' => [
        'entity' => [
            'PUT' => [
                'description' => 'Actualiza la dirección de un usuario dependiendo de la configuración del Adbox: Centro de Distribución (cedis) o dirección del usuario (user-address). En el payload debe estar presente el nodo que se va a actualizar.',
                'request' => '{
  "cedis": {
    "id": "idCedis"
  },
  "address": {
    "street": "calle",
    "extNumber": "numExt",
    "intNumber": "numInt",
    "zipCode": "00000",
    "reference": "referencia",
    "location": "municipio",
    "city": "ciudad",
    "town": "municipio",
    "state": "estado"
  }
}',
                'response' => '{
"message": "success"
}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Adminroles\\Controller' => [
        'description' => 'CRUD de roles',
        'collection' => [
            'GET' => [
                'description' => 'obtiene items',
                'response' => 'GET http://sandbox-api.devadventa.com/admin/roles
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Thu, 22 Dec 2016 17:10:22 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  179
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

[{"id":7,"role":"Admin"},{"id":6,"role":"Cliente"},{"id":5,"role":"Gerente"},{"id":1,"role":"public"},{"id":2,"role":"register"},{"id":3,"role":"test"},{"id":4,"role":"Vendedor"}]',
            ],
            'POST' => [
                'description' => 'crea item',
                'request' => 'POST http://sandbox-api.devadventa.com/admin/roles
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"role":"Admin2"}',
                'response' => '201 Created
Date:  Thu, 22 Dec 2016 17:20:56 GMT
Server:  Apache/2.4.18 (Ubuntu)
Location:  http://sandbox-api.devadventa.com/admin/roles
Content-Location:  http://sandbox-api.devadventa.com/admin/roles
Content-Length:  17
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"role":"Admin2"}',
            ],
        ],
        'entity' => [
            'GET' => [
                'description' => 'obtiene item por ID',
                'response' => 'GET http://sandbox-api.devadventa.com/admin/roles/7
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Thu, 22 Dec 2016 17:15:52 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  25
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

[{"id":7,"role":"Admin"}]',
            ],
            'PATCH' => [
                'description' => 'actualiza item',
                'request' => 'PATCH http://sandbox-api.devadventa.com/admin/roles/7
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"role":"Admin2"}',
                'response' => '200 OK
Date:  Thu, 22 Dec 2016 17:17:20 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  17
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"role":"Admin2"}',
            ],
            'PUT' => [
                'description' => 'actualiza item',
                'request' => 'PUT http://sandbox-api.devadventa.com/admin/roles/7
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"role":"Admin"}',
                'response' => '200 OK
Date:  Thu, 22 Dec 2016 17:17:47 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  16
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"role":"Admin"}',
            ],
            'DELETE' => [
                'description' => 'elimina item',
                'request' => 'DELETE http://sandbox-api.devadventa.com/admin/roles/8
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd',
                'response' => '204 No Content
Date:  Thu, 22 Dec 2016 17:22:31 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  0
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  text/html; charset=UTF-8',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Adminresource\\Controller' => [
        'collection' => [
            'description' => 'CRUD resources',
            'GET' => [
                'response' => 'GET http://sandbox-api.devadventa.com/admin/resources
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Thu, 22 Dec 2016 17:27:58 GMT
Server:  Apache/2.4.18 (Ubuntu)
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Transfer-Encoding:  chunked
Content-Type:  application/json; charset=utf-8

[{"id":17,"alias":"banners\\/:id","resource":"Adbox\\\\V1\\\\Rest\\\\Banners\\\\Controller::entity","methodhttp":"PUT","description":""},......{}]',
                'description' => 'obtienes items',
            ],
            'POST' => [
                'description' => 'crear item',
                'request' => 'POST http://sandbox-api.devadventa.com/admin/resources
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"alias":"POST ZF\\\\OAuth2\\\\Controller\\\\Auth::token","resource":"ZF\\\\OAuth2\\\\Controller\\\\Auth","methodhttp":"POST","description":""}',
                'response' => '201 Created
Date:  Thu, 22 Dec 2016 17:40:58 GMT
Server:  Apache/2.4.18 (Ubuntu)
Location:  http://sandbox-api.devadventa.com/admin/resources
Content-Location:  http://sandbox-api.devadventa.com/admin/resources
Content-Length:  131
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"alias":"POST ZF\\\\OAuth2\\\\Controller\\\\Auth::token","resource":"ZF\\\\OAuth2\\\\Controller\\\\Auth","methodhttp":"POST","description":""}',
            ],
        ],
        'entity' => [
            'description' => 'CRUD resources',
            'GET' => [
                'description' => 'GET http://sandbox-api.devadventa.com/admin/resources/114
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd',
                'response' => '200 OK
Date:  Thu, 22 Dec 2016 17:41:59 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  142
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

[{"id":114,"alias":"POST ZF\\\\OAuth2\\\\Controller\\\\Auth::token","resource":"ZF\\\\OAuth2\\\\Controller\\\\Auth","methodhttp":"POST","description":""}]',
            ],
            'PATCH' => [
                'description' => 'actualiza item',
                'request' => 'PATCH http://sandbox-api.devadventa.com/admin/resources/114
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"alias":"POST ZF\\\\OAuth2\\\\Controller\\\\Auth::token","resource":"ZF\\\\OAuth2\\\\Controller\\\\Auth","methodhttp":"POST","description":""}',
                'response' => '200 OK
Date:  Thu, 22 Dec 2016 17:43:07 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  131
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"alias":"POST ZF\\\\OAuth2\\\\Controller\\\\Auth::token","resource":"ZF\\\\OAuth2\\\\Controller\\\\Auth","methodhttp":"POST","description":""}',
            ],
            'PUT' => [
                'request' => 'PUT http://sandbox-api.devadventa.com/admin/resources/114
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"alias":"POST ZF\\\\OAuth2\\\\Controller\\\\Auth::token","resource":"ZF\\\\OAuth2\\\\Controller\\\\Auth","methodhttp":"POST","description":""}',
                'response' => '200 OK
Date:  Thu, 22 Dec 2016 17:44:30 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  131
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"alias":"POST ZF\\\\OAuth2\\\\Controller\\\\Auth::token","resource":"ZF\\\\OAuth2\\\\Controller\\\\Auth","methodhttp":"POST","description":""}',
                'description' => 'actualiza item',
            ],
            'DELETE' => [
                'description' => 'elimina item',
                'request' => 'DELETE http://sandbox-api.devadventa.com/admin/resources/114
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd',
                'response' => '204 No Content
Date:  Thu, 22 Dec 2016 17:45:03 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  0
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  text/html; charset=UTF-8',
            ],
        ],
        'description' => 'CRUD resources',
    ],
    'Adbox\\V1\\Rest\\Adminpermissions\\Controller' => [
        'description' => 'CRUD permisos',
        'collection' => [
            'description' => 'obtiene items',
            'GET' => [
                'response' => 'GET http://sandbox-api.devadventa.com/admin/permissions
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Thu, 22 Dec 2016 17:47:29 GMT
Server:  Apache/2.4.18 (Ubuntu)
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Transfer-Encoding:  chunked
Content-Type:  application/json; charset=utf-8

[{"id":210,"roleId":7,"role":"Admin","resourceId":4,"resource":"Adbox\\\\V1\\\\Rest\\\\Configprivate\\\\Controller::collection","alias":"config\\/private\\/:id","permission":"allow"},{"id":211,"roleId":7,"role":"Admin","resourceId":5,"resource":"Adbox\\\\V1\\\\Rest\\\\Configprivate\\\\Controller::collection","alias":"config\\/private\\/:id","permission":"deny"}]',
                'description' => 'obtiene items',
            ],
            'POST' => [
                'request' => 'POST http://sandbox-api.devadventa.com/admin/permissions
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"roleid":"1","resourceid":"113","permission":"allow"}',
                'response' => '201 Created
Date:  Thu, 22 Dec 2016 18:04:23 GMT
Server:  Apache/2.4.18 (Ubuntu)
Location:  http://sandbox-api.devadventa.com/admin/permissions
Content-Location:  http://sandbox-api.devadventa.com/admin/permissions
Content-Length:  54
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"roleid":"1","resourceid":"113","permission":"allow"}',
                'description' => 'obtiene items',
            ],
        ],
        'entity' => [
            'GET' => [
                'description' => 'obtiene item',
                'response' => 'GET http://sandbox-api.devadventa.com/admin/permissions/114
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Thu, 22 Dec 2016 18:05:33 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  168
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

[{"id":114,"roleId":4,"role":"Vendedor","resourceId":32,"resource":"Adbox\\\\V1\\\\Rest\\\\Usercart\\\\Controller::collection","alias":"users\\/:id\\/cart","permission":"allow"}]',
            ],
            'description' => 'CRUD permisos',
            'PATCH' => [
                'description' => 'actualiza item',
                'request' => 'PATCH http://sandbox-api.devadventa.com/admin/permissions/210
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"permission":"allow"}',
                'response' => '200 OK
Date:  Thu, 22 Dec 2016 18:07:47 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  22
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"permission":"allow"}',
            ],
            'PUT' => [
                'description' => 'actualiza item',
                'request' => 'PUT http://sandbox-api.devadventa.com/admin/permissions/210
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json
{"roleid":7,"resourceid":4,"permission":"allow"}',
                'response' => '200 OK
Date:  Thu, 22 Dec 2016 18:10:11 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  48
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"roleid":7,"resourceid":4,"permission":"allow"}',
            ],
            'DELETE' => [
                'description' => 'elimina item',
                'request' => 'DELETE http://sandbox-api.devadventa.com/admin/permissions/146
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd',
                'response' => '204 No Content
Date:  Thu, 22 Dec 2016 18:11:15 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  0
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  text/html; charset=UTF-8',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Adminpermissionsbuildresource\\Controller' => [
        'description' => 'crea automáticamente items resource apartir del archivo de configs',
        'collection' => [
            'description' => 'crea automáticamente items resource apartir del archivo de configs',
            'POST' => [
                'description' => 'crea automáticamente items resource apartir del archivo de configs',
                'request' => 'POST http://sandbox-api.devadventa.com/admin/permissions/resource
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json',
                'response' => '204 No Content
Date:  Thu, 22 Dec 2016 18:16:18 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  118
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/problem+json',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Adminpermissionsbuild\\Controller' => [
        'description' => 'crea automáticamente permisos a partir de los roles existente y lo recursos',
        'collection' => [
            'description' => 'crea automáticamente permisos a partir de los roles existente y lo recursos',
            'POST' => [
                'description' => 'crea automáticamente permisos a partir de los roles existente y lo recursos',
                'request' => 'POST http://sandbox-api.devadventa.com/admin/permissions/resource
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json',
                'response' => '204 No Content
Date:  Thu, 22 Dec 2016 18:16:18 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  118
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/problem+json',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Adminusersupload\\Controller' => [
        'collection' => [
            'POST' => [
                'request' => '{
   "csv": ""
}',
                'description' => 'Carga de archivo de usuarios',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Adminusersuploadlayout\\Controller' => [
        'collection' => [
            'GET' => [
                'description' => 'Descarga de layout de carga de usuarios',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Theme\\Controller' => [
        'collection' => [
            'POST' => [
                'description' => 'Edita la configuración de tema y ejecuta la compilación',
                'request' => '{
   "theme.configuration": ""
}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Themepreview\\Controller' => [
        'entity' => [
            'POST' => [
                'description' => 'Ejecuta la compilación del tema',
                'request' => '{
   "theme.configuration": ""
}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Userspassword\\Controller' => [
        'entity' => [
            'PUT' => [
                'description' => 'Actualiza la <b>contraseña</b> del usuario que corresponde al <b>token de autorización (Bearer)</b>.<br>
La <b>contraseña anterior</b> debe coincidir con la actual.',
                'request' => '{
   "password": "Nueva contraseña",
   "currentPassword": "Contraseña anterior"
}',
                'response' => '{
   "message": "success"
}',
            ],
        ],
        'description' => 'Endpoint de contraseña del usuario',
    ],
    'Adbox\\V1\\Rest\\Usersinfo\\Controller' => [
        'entity' => [
            'PUT' => [
                'request' => '{
   "firstName": "Nombre(s)",
   "lastName": "Apellido paterno",
   "email": "Correo electrónico",
   "surname": "Apellido materno",
   "telephone1": "Número telefónico",
   "telephone2": "Número telefónico (otro)",
   "mobile": "Teléfono Móvil"
}',
                'response' => '{
  "message": "success"
}',
                'description' => 'Actualiza la <b>información básica</b> del usuario que corresponde al <b>token de autorización (Bearer)</b>.<br>
Entre los datos editables se incluyen: <b>nombre, apellidos, correo y números telefónicos</b>.',
            ],
            'description' => '',
        ],
        'description' => 'Endpoint de información del usuario',
    ],
    'Adbox\\V1\\Rest\\Usersdelivery\\Controller' => [
        'entity' => [
            'PUT' => [
                'description' => 'Actualiza la <b>dirección</b> del usuario que corresponde al <b>token de autorización (Bearer)</b>.<br>Dependiendo de la configuración del Adbox se actualizará lo siguiente:<br>
<ol>
  <li><b>cedis:</b> Actualiza su Centro de distribución.</li>
  <li><b>user-address:</b> Actualiza la dirección del usuario.</li>
</ol>',
                'request' => '{
  "cedis": {
    "id": "idCedis"
  },
  "address": {
    "street": "calle",
    "extNumber": "numExt",
    "intNumber": "numInt",
    "zipCode": "00000",
    "reference": "referencia",
    "location": "municipio",
    "city": "ciudad",
    "town": "municipio",
    "state": "estado"
  }
}',
                'response' => '{
  "message": "success"
}',
            ],
        ],
        'description' => 'Endpoint de dirección de entrega del usuario',
    ],
    'Adbox\\V1\\Rest\\Adminusers\\Controller' => [
        'description' => 'Endpoint de usuarios',
        'collection' => [
            'GET' => [
                'description' => 'Obtiene la lista de <b>todos los usuarios</b>.<br>
El parámetro <b>canCheckout</b> determina el tipo de usuarios que se obtienen, los posibles valores son:<br>
<ol>
  <li><b>1:</b> Usuarios que pueden realizar canjes.</li>
  <li><b>0:</b> Usuarios que no pueden realizar canjes (inactivos y bloqueados).</li>
  <li><b>Ninguno:</b> Todos los usuarios.</li>
</ol>',
                'response' => '[
  {
    "id": 1,
    "username": "username",
    "firstName": "Nombre",
    "lastName": "Apellido",
    "surname": "Apellido",
    "email": "email@domain.com",
    "displayName": "Nombre apellido",
    "enabled": true,
    "profileFulfilled": true,
    "telephone1": "0123456789",
    "telephone2": "0123456789",
    "mobile": "9876543210",
    "birthday": null,
    "gender": null,
    "jobTitle": null,
    "createdAt": "2016-12-01 12:00:00",
    "modifiedAt": "2016-11-02 15:00:30",
    "deletedAt": null,
    "role": {
      "id": 2,
      "name": "register"
    },
    "credits": {
      "available": 0,
      "earned": 0,
      "spent": 0
    }
  },
  {
    "id": 2,
    "username": "username",
    "firstName": "Nombre",
    "lastName": "Apellido",
    "surname": "Apellido",
    "email": "email@domain.com",
    "displayName": "Nombre apellido",
    "enabled": true,
    "profileFulfilled": true,
    "telephone1": "0123456789",
    "telephone2": "0123456789",
    "mobile": "9876543210",
    "birthday": null,
    "gender": null,
    "jobTitle": null,
    "createdAt": "2016-12-01 12:00:00",
    "modifiedAt": "2016-11-02 15:00:30",
    "deletedAt": null,
    "role": {
      "id": 2,
      "name": "register"
    },
    "credits": {
      "available": 0,
      "earned": 0,
      "spent": 0
    }
  }
]',
            ],
            'POST' => [
                'request' => '{
   "firstName": "Nombre(s)",
   "lastName": "Apellido paterno",
   "email": "Correo electrónico",
   "surname": "Apellido materno",
   "telephone1": "Número telefónico",
   "telephone2": "Número telefónico (otro)",
   "mobile": "Teléfono Móvil",
   "roleId": "ID del rol",
   "password": "Contraseña",
   "cedis": "Datos del Centro de Distribución",
   "address": "Datos de la dirección"
}',
                'description' => 'Registra un usuario <b>nuevo</b>.',
            ],
        ],
        'entity' => [
            'GET' => [
                'description' => 'Obtiene la información completa de <b>un sólo usuario</b>.',
                'response' => '{
  "id": 1,
  "username": "username",
  "firstName": "Nombre",
  "lastName": "Apellido",
  "surname": null,
  "email": "correo@dominio.com",
  "displayName": "Nombre Apellido",
  "enabled": true,
  "profileFulfilled": true,
  "telephone1": null,
  "telephone2": null,
  "mobile": "1234567890",
  "birthday": null,
  "gender": null,
  "jobTitle": null,
  "createdAt": "2016-12-01 12:00:00",
  "modifiedAt": "2016-12-01 13:00:00",
  "deletedAt": null,
  "role": {
    "id": 2,
    "name": "register"
  },
  "credits": {
    "available": "10000",
    "earned": "9000",
    "spent": "1000"
  },
  "address": null,
  "cedis": {
    "id": 3,
    "cedisId": 122,
    "street": "Manuel G\\u00f3mez Mor\\u00edn",
    "extNumber": 2001,
    "intNumber": "23",
    "location": "Lomas del Colli",
    "reference": "Entre Acueducto y 5 de mayo",
    "city": "Zapopan",
    "state": "Jalisco",
    "zipCode": 45136,
    "telephone": "01 33 3832 2800",
    "name": "YYY MOTORS"
  }
}',
            ],
            'DELETE' => [
                'request' => '',
                'description' => 'Cambia el status de un usuario a <b>eliminado</b>.',
                'response' => '{
  "message": "success"
}',
            ],
        ],
    ],
    'Adbox\\V1\\Rest\\Messages\\Controller' => [
        'entity' => [
            'PUT' => [
                'description' => 'Modifica los <b>mensajes configurados</b> en el sistema a partir de la <b>clave enviada</b> en la url (id).<br>
Si el <b>mensaje no existe</b> en la base de datos entonces se <b>registra</b> su clave => valor, de lo contrario su valor es <b>actualizado</b>.',
                'request' => '{
   "value": "Nuevo contenido del mensaje"
}',
                'response' => '{
  "message": "success"
}',
            ],
        ],
        'description' => 'Endpoint de mensajes del sistema',
    ],
    'Adbox\\V1\\Rest\\Config\\Controller' => [
        'collection' => [
            'POST' => [
                'description' => 'Edita las configuraciones del adbox.',
                'request' => '{
  "auth.registration": "",
  "client.url": "",
  "catalog.motivale.id": 0,
  "checkout.delivery.mode": "",
  "checkout.delivery.userEditable": "",
  "survey.isMandatory": 0,
  "client.id": "",
  "client.name": "",
  "email.account.from": "",
  "email.setting.host": "",
  "email.setting.port": 0,
  "email.setting.connectionConfig.username": "",
  "email.setting.connectionConfig.password": "",
  "jira.collectorScript": "",
  "jira.components": "",
  "google.analytics.trackingId": "",
  "adcinema.project.id": 0,
  "adcinema.server.development.url": "",
  "adcinema.server.production.url": "",
  "email.account.fromname": "",
  "social.facebook": "",
  "social.twitter": "",
  "social.youtube": "",
  "social.linkedin": ""
}',
                'response' => '{
  "message": "success"
}',
            ],
        ],
        'description' => 'Endpoint de configuraciones del sistema',
    ],
    'Adbox\\V1\\Rest\\Adminusersinfo\\Controller' => [
        'entity' => [
            'PUT' => [
                'description' => 'Actualiza la <b>información básica</b> del usuario solicitado.<br>
Entre los datos editables se incluyen: <b>nombre, apellidos, correo y números telefónicos</b>.',
                'request' => '{
   "firstName": "Nombre(s)",
   "lastName": "Apellido paterno",
   "email": "Correo electrónico",
   "surname": "Apellido materno",
   "telephone1": "Número telefónico",
   "telephone2": "Número telefónico (otro)",
   "mobile": "Teléfono Móvil",
   "roleId": "ID del rol",
   "profileFulfilled": "Perfil completado",
   "enabled": "Usuario activo"
}',
                'response' => '{
  "message": "success"
}',
            ],
        ],
        'description' => 'Endpoint de información del usuario',
    ],
];
