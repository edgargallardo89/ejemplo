## Administrador - Usuarios
---

### Obtener Usuarios
Obtiene la lista de todos los usuarios.

### HTTP Request
`GET /admin/users`

### URL Params

##### Opcional:
- `canCheckout=[integer]` Determina el tipo de usuarios que se obtienen:
  - `1` Usuarios que pueden realizar canjes.
  - `0` Usuarios que no pueden realizar canjes (inactivos y bloqueados).
  - `Predeterminado` Todos los usuarios.

### Ejemplo
```text
http://localhost:3001/admin/users
http://localhost:3001/admin/users?canCheckout=0
http://localhost:3001/admin/users?canCheckout=1
```

### Respuesta Exitosa
Status: 200 OK
```json
[
  {
    "id": 1,
    "username": "username",
    "firstName": "Nombre",
    "lastName": "Apellido",
    "surname": "Apellido",
    "email": "email@domain.com",
    "displayName": "Nombre apellido",
    "enabled": true,
    "profileFulfilled": true,
    "telephone1": "0123456789",
    "telephone2": "0123456789",
    "mobile": "9876543210",
    "birthday": null,
    "gender": null,
    "jobTitle": null,
    "createdAt": "2016-12-01 12:00:00",
    "modifiedAt": "2016-11-02 15:00:30",
    "deletedAt": null,
    "role": {
      "id": 2,
      "name": "register"
    },
    "credits": {
      "available": 0,
      "earned": 0,
      "spent": 0
    }
  },
  {
    "id": 2,
    "username": "username",
    "firstName": "Nombre",
    "lastName": "Apellido",
    "surname": "Apellido",
    "email": "email@domain.com",
    "displayName": "Nombre apellido",
    "enabled": true,
    "profileFulfilled": true,
    "telephone1": "0123456789",
    "telephone2": "0123456789",
    "mobile": "9876543210",
    "birthday": null,
    "gender": null,
    "jobTitle": null,
    "createdAt": "2016-12-01 12:00:00",
    "modifiedAt": "2016-11-02 15:00:30",
    "deletedAt": null,
    "role": {
      "id": 2,
      "name": "register"
    },
    "credits": {
      "available": 0,
      "earned": 0,
      "spent": 0
    }
  }
]
```

### Errores