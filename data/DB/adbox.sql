-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 28, 2016 at 05:19 PM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adbox`
--

-- --------------------------------------------------------

--
-- Table structure for table `core_audit_logs`
--

CREATE TABLE `core_audit_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_type` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `object_type` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `object_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID de la tabla respectiva'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_configs`
--

CREATE TABLE `core_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `value` text COLLATE utf8_spanish2_ci,
  `visibility` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_messages`
--

CREATE TABLE `core_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `value` text COLLATE utf8_spanish2_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `visibility` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_permissions`
--

CREATE TABLE `core_permissions` (
  `id` int(11) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL,
  `resource_id` int(11) NOT NULL,
  `permission` enum('allow','deny') COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `core_permissions`
--

INSERT INTO `core_permissions` (`id`, `role_id`, `resource_id`, `permission`) VALUES
(1, 1, 1, 'allow'),
(2, 1, 3, 'allow'),
(3, 2, 4, 'allow'),
(4, 2, 5, 'deny'),
(5, 2, 6, 'allow'),
(6, 2, 7, 'allow'),
(7, 2, 8, 'allow'),
(8, 2, 9, 'allow'),
(9, 2, 10, 'allow');

-- --------------------------------------------------------

--
-- Table structure for table `core_products`
--

CREATE TABLE `core_products` (
  `id` int(10) UNSIGNED NOT NULL,
--  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `description` text COLLATE utf8_spanish2_ci,
  `brand` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `real_price` decimal(8,2) DEFAULT NULL COMMENT 'campo `Precio` en motivale',
  `price` int(10) UNSIGNED DEFAULT NULL COMMENT 'campo `Puntos` desde motivale',
  `payload` text COLLATE utf8_spanish2_ci COMMENT 'JSON original recibido desde motivale',
  `enabled` bit(1) NOT NULL,
  `editable` bit(1) NOT NULL COMMENT 'si es verdadero, indica que el producto fue creada por un usuario, en vez de cargada desde motivale',
  `deleted_at` timestamp NULL,
  `created_at` timestamp NOT NULL,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_products_x_categories`
--

CREATE TABLE `core_products_x_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_product_categories`
--

CREATE TABLE `core_product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `editable` bit(1) NOT NULL COMMENT 'si es verdadero, indica que la categoría fue creada por un usuario, en vez de cargada desde motivale',
  `name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `sort` int(11) NOT NULL COMMENT 'el orden a mostrar en las vistas',
  `enabled` bit(1) NOT NULL,
  `deleted_at` timestamp NULL,
  `created_at` timestamp NOT NULL,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_resources`
--

CREATE TABLE `core_resources` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `alias` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `resource` varchar(125) COLLATE utf8_spanish2_ci NOT NULL,
  `methodhttp` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `description` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `core_resources`
--

INSERT INTO `core_resources` (`id`, `parent_id`, `alias`, `resource`, `methodhttp`, `description`) VALUES
(1, 0, 'POST ZF\\OAuth2\\Controller\\Auth::token', 'ZF\\OAuth2\\Controller\\Auth::token', 'POST', ''),
(3, 0, 'GET ZF\\OAuth2\\Controller\\Auth::token', 'ZF\\OAuth2\\Controller\\Auth::token', 'GET', ''),
(4, 0, 'GET Adbox\\V1\\Rest\\Configprivate\\Controller::collection', 'Adbox\\V1\\Rest\\Configprivate\\Controller::collection', 'GET', ''),
(5, 0, 'POST Adbox\\V1\\Rest\\Configprivate\\Controller::collection', 'Adbox\\V1\\Rest\\Configprivate\\Controller::collection', 'POST', ''),
(6, 0, 'GET Adbox\\V1\\Rest\\Configpublic\\Controller::collection', 'Adbox\\V1\\Rest\\Configpublic\\Controller::collection', 'GET', ''),
(7, 0, 'POST Adbox\\V1\\Rest\\Configpublic\\Controller::collection', 'Adbox\\V1\\Rest\\Configpublic\\Controller::collection', 'POST', ''),
(8, 0, 'PUT Adbox\\V1\\Rest\\Users\\Controller::entity', 'Adbox\\V1\\Rest\\Users\\Controller::entity', 'PUT', ''),
(9, 0, 'GET Adbox\\V1\\Rest\\Users\\Controller::entity', 'Adbox\\V1\\Rest\\Users\\Controller::entity', 'GET', ''),
(10, 0, 'PATCH Adbox\\V1\\Rest\\Users\\Controller::entity', 'Adbox\\V1\\Rest\\Users\\Controller::entity', 'PATCH', '');

-- --------------------------------------------------------

--
-- Table structure for table `core_roles`
--

CREATE TABLE `core_roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role` varchar(40) COLLATE utf8_spanish2_ci NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `core_roles`
--

INSERT INTO `core_roles` (`id`, `role`, `parent_id`) VALUES
(1, 'public', 0),
(2, 'register', 0);

-- --------------------------------------------------------

--
-- Table structure for table `core_roles_x_products_categories`
--

CREATE TABLE `core_roles_x_products_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='categor�as accesibles por rol';

-- --------------------------------------------------------

--
-- Table structure for table `core_sendemail`
--

CREATE TABLE `core_sendemail` (  
    `id` int(10) UNSIGNED NOT NULL,  
    `template` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
    `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `from` varchar(125) COLLATE utf8_spanish2_ci NOT NULL,
    `to` varchar(125) COLLATE utf8_spanish2_ci NOT NULL,
    `cc` varchar(255) NULL COLLATE utf8_spanish2_ci, 
    `subject` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
    `status` enum('new','sent','failed') COLLATE utf8_spanish2_ci DEFAULT 'new'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_users` (DEPRECATED)
--

-- CREATE TABLE `core_users` (
--   `id` int(10) UNSIGNED NOT NULL,
--   `role_id` int(11) UNSIGNED NOT NULL,
--   `created_by_id` int(11) UNSIGNED NULL DEFAULT NULL,
--   `username` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
--   `first_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `surname` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `last_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `email` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `display_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `enabled` bit(1) NOT NULL,
--   `profile_fulfilled` bit(1) NOT NULL,
--   `telephone1` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `telephone2` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `mobile` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `birthday` date DEFAULT NULL,
--   `gender` enum('male','female') COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `job_title` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--   `modified_at` timestamp NULL DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_user_addresses`
--

CREATE TABLE `core_user_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `street` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ext_number` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `int_number` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `town` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `main` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `core_user_transactions`
--

CREATE TABLE `core_user_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_spanish2_ci NOT NULL COMMENT 'tipo de transacci�n order|adjustment|cancelation|etc',
  `correlation_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'id de la entidad relacionada con esta transacci�n',
  `balance_snapshot` int(11) NOT NULL COMMENT 'el saldo total del usuario despu�s de aplicar esta transacci�n',
  `details` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'detalles extra relacionados con la transacci�n',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `applied_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) COLLATE utf8_spanish2_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scope` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`access_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('04439e87c7a8f421374092e8b3bdf40e467dfbcd', 'testclient2', 'testuser', '2016-11-17 08:17:43', NULL),
('1b777e60920ab8e3517390c8acf3d8a4a039a667', 'testclient2', 'testuser', '2016-10-15 07:43:05', NULL),
('1fb4e1fd405136d554039153d2b0bea9c052bc28', 'testclient2', 'testuser', '2016-10-18 03:14:46', NULL),
('23fdc38eb6381dc7bcb35029eae92cc15b1636a6', 'testclient2', 'testuser', '2016-10-15 02:34:07', NULL),
('2421417aa681731f73a2b897d596a699a3af7533', 'testclient2', 'testuser', '2016-10-18 03:18:18', NULL),
('2e2c6727059b9b331247a70e8f491b3d24a9854c', 'testclient2', 'testuser2', '2016-10-14 03:06:15', NULL),
('3395a23863c7a44acf6ed9030b6e1b1db8c24b06', 'testclient2', 'testuser', '2016-10-15 23:08:43', NULL),
('4afd3fa5e2b8fd341a887abe108b487ad2cc5cdb', 'testclient2', 'testuser', '2016-11-17 08:16:15', NULL),
('4b52cd6f4e802b5959ca8f1a2b9aa2cb709f190f', 'testclient2', 'testuser2', '2016-10-14 04:55:12', NULL),
('4c9558ac384914cd8ea3a9697b5e0ff07ae2bd22', 'testclient2', 'testuser', '2016-11-17 03:26:26', NULL),
('60d0b851ee201882098b064ed7f54bd753a64138', 'testclient2', 'testuser', '2016-11-17 05:44:06', NULL),
('60dc7102c3c83a1016810449c1a8364efaacbb68', 'testclient2', 'testuser', '2016-11-17 08:17:44', NULL),
('6525b76047ccc7714a0483ae82fa626f4bd71fcc', 'testclient2', 'testuser', '2016-10-09 07:31:07', NULL),
('6f7aa49738c341e0266cf21349653f16eccb76d3', 'testclient2', 'testuser', '2016-10-18 03:20:36', NULL),
('7195578f1e5dd2497ba9cee74d2e6767fbf9a80a', 'testclient2', 'testuser', '2016-11-17 05:58:58', NULL),
('7466c272d99537a1b0c6d25919a58e450305541e', 'testclient2', 'testuser', '2016-11-17 08:17:51', NULL),
('8b6b63ef780c8e758b4fae7429e2a42814dab497', 'testclient2', 'testuser', '2016-10-15 23:25:58', NULL),
('8cb1c52d507073450c3b208637c5617c230d545b', 'testclient2', 'testuser', '2016-10-14 14:36:44', NULL),
('908e006947557e2cee7043a29ed5ab6ebe2e1e81', 'testclient2', 'testuser2', '2016-10-14 05:06:20', NULL),
('91a1a04ca7076224b4d688d97c08906091e8ed18', 'testclient2', 'testuser', '2016-10-18 03:16:02', NULL),
('92279f6364d75041988b348fc0733fd4c3c7bf1a', 'testclient2', 'testuser', '2016-10-15 02:33:15', NULL),
('92704fc1c2f642c15e6e7329efe2967f3e30b318', 'testclient2', 'testuser', '2016-10-15 08:32:41', NULL),
('9da5bcd7dd27e178125b07435d9862db4bc3e13a', 'testclient2', 'testuser', '2016-10-18 03:19:53', NULL),
('a36055086eb14ba9c34e688e9444540e775379c9', 'testclient2', 'testuser', '2016-10-15 10:16:16', NULL),
('a4d5e349d43272b049e944638f950511cf6d63a1', 'testclient2', 'testuser2', '2016-10-14 03:43:35', NULL),
('a555e08805e16173f7b093d726d85973f629a677', 'testclient2', 'testuser', '2016-10-15 10:59:09', NULL),
('b31a7b64a633d409bb60d7b544a7ce3b3bb890ee', 'testclient2', 'testuser', '2016-10-15 08:27:08', NULL),
('b328ef311be7b8fd4ceb54b2b1bc29710e89b520', 'testclient2', 'testuser', '2016-10-18 03:17:00', NULL),
('bc70588871648f38b2dec700652f3154e2843817', 'testclient2', 'testuser', '2016-10-18 03:18:10', NULL),
('e63d8d9598733174a3a4ed6b36952bd676b92142', 'testclient2', 'testuser2', '2016-10-14 04:56:03', NULL),
('e74957a5d35e1d9425ff984bd9bb02c6a339cd99', 'testclient2', 'testuser', '2016-10-18 03:25:13', NULL),
('e8ac6f43ace686bfa3e4bf85e9f8955d4f4d7a8c', 'testclient2', 'testuser', '2016-10-15 23:09:10', NULL),
('e8fa3fcc41ba59eb07f5e3b214abddc1dd94af99', 'testclient2', 'testuser', '2016-10-15 08:09:29', NULL),
('eac68b75f01d706a34ee8a692632084e68d9bf91', 'testclient2', 'testuser2', '2016-10-14 06:08:25', NULL),
('ed67c9e37d7e101e5cb14d991125d79da95f0ba9', 'testclient2', 'testuser2', '2016-10-09 07:32:16', NULL),
('eddd8a243b84b8b00be6e9445c26f9e4e250bd08', 'testclient2', 'testuser', '2016-10-18 03:23:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_authorization_codes`
--

CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) COLLATE utf8_spanish2_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scope` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_token` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `client_secret` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8_spanish2_ci NOT NULL,
  `grant_types` varchar(80) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `scope` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES
('testclient', '$2y$10$5ICo6mbnWLsptjCZVfMu1e7p04FYpgiZydEG1KD4MI8Q2fcwuCu8e', '/oauth/receivecode', NULL, NULL, '1'),
('testclient2', '', '/oauth/receivecode', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_jwt`
--

CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `subject` varchar(80) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `public_key` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) COLLATE utf8_spanish2_ci NOT NULL,
  `client_id` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scope` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`refresh_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('03d86f031414a62e6564833d4f61f81dd532da52', 'testclient2', 'testuser', '2016-10-29 09:16:16', NULL),
('0550a820f3c04cc92b4ae5123d4db55173b5501b', 'testclient2', 'testuser', '2016-11-01 03:26:26', NULL),
('09fc8a5ca453d96c29da31a31069dd0b8126cda2', 'testclient2', 'testuser', '2016-10-29 22:09:10', NULL),
('0fbc17f47c195e8fbd6bd912d34fa3708f13e6da', 'testclient2', 'testuser', '2016-10-29 22:08:43', NULL),
('132688a264e4f99488ed4adc0446c0394b3bbcc1', 'testclient2', 'testuser', '2016-11-01 03:18:10', NULL),
('19de56f8a89f3d826a829650bfe6703ffc5a6b42', 'testclient2', 'testuser', '2016-10-29 07:32:41', NULL),
('2c33c0251f0ef81e734242986b436b5959509d96', 'testclient2', 'testuser2', '2016-10-28 03:55:12', NULL),
('3363fc4cd98e6590c8beeb1985f5e66c24d24448', 'testclient2', 'testuser2', '2016-10-28 03:56:03', NULL),
('429e0cf501f542bb9ec97a4649055996aa04d3e9', 'testclient2', 'testuser', '2016-11-01 03:25:13', NULL),
('42eee06d3436eca7e1060dbc4b8d785d0ef6beb0', 'testclient2', 'testuser', '2016-11-01 08:17:51', NULL),
('51d0cc4308b99e1250d869d178ffcf328eedb10b', 'testclient2', 'testuser', '2016-11-01 03:16:02', NULL),
('5e627cddb18617eafa9c7fb1192a38b524eafac6', 'testclient2', 'testuser2', '2016-10-28 04:06:20', NULL),
('6552e0576d4f7bf08f4ca7e084660df269068b2f', 'testclient2', 'testuser', '2016-10-29 07:09:29', NULL),
('6591cc2d088576fa7a0b9912d492f96182a84dc0', 'testclient2', 'testuser', '2016-11-01 03:14:46', NULL),
('6b5055154aa60c6bf6dcdc80a1c773e425f3f1f5', 'testclient2', 'testuser', '2016-10-29 07:27:08', NULL),
('76404e8d593acecdf7552e2026c7e499b98355d4', 'testclient2', 'testuser', '2016-11-01 03:23:20', NULL),
('7b83518f24b73abc7b3edc27b78d0fd5a87cac37', 'testclient2', 'testuser2', '2016-10-28 02:43:35', NULL),
('84292d8573db9969c1a4d421a52636b4387b01e6', 'testclient2', 'testuser', '2016-10-28 13:36:45', NULL),
('8fbf35d0518061740a3446b98d3886b969106037', 'testclient2', 'testuser', '2016-10-29 01:34:07', NULL),
('90a6430a13d986a20e0329832cfb4c34ee849bad', 'testclient2', 'testuser2', '2016-10-28 02:06:15', NULL),
('930ab5e914d1049f3d2239c564250ff9544d5da9', 'testclient2', 'testuser', '2016-10-29 06:43:05', NULL),
('943d0a707efa8e1e2d1a71e4fa58e5f7098890dd', 'testclient2', 'testuser', '2016-10-29 01:33:16', NULL),
('96adf36a62bb31c77a39058aeed5fe7f618947f2', 'testclient2', 'testuser', '2016-10-29 22:25:58', NULL),
('97d97be46a1009724de9f31222647841a7ba4e64', 'testclient2', 'testuser', '2016-11-01 03:18:19', NULL),
('a3a2705a2298f6cb960f6838456a1db614b76647', 'testclient2', 'testuser', '2016-11-01 03:20:36', NULL),
('b6a0576af3833237cbe5ae10d829d298895dc1f8', 'testclient2', 'testuser', '2016-11-01 05:58:58', NULL),
('bf0fa16d3db6e8d659910768f466164e79690ccb', 'testclient2', 'testuser', '2016-10-23 06:31:07', NULL),
('c0d1378f17092ec9a098d1be0ffe70f9dbae3d9f', 'testclient2', 'testuser', '2016-11-01 08:16:15', NULL),
('c52955213db788c5e6eacfbcc4c47b475475e883', 'testclient2', 'testuser', '2016-11-01 03:19:53', NULL),
('c70c119df2b26e2d3d2e7ff7060b662f91f5eaa8', 'testclient2', 'testuser', '2016-11-01 08:17:43', NULL),
('d41007aef2cc34beb3f8dbb72bbc565f4e4d96a4', 'testclient2', 'testuser', '2016-10-29 09:59:09', NULL),
('d604c8161db140cf6c09a6610199a1ff26ed2e9d', 'testclient2', 'testuser2', '2016-10-23 06:32:16', NULL),
('da09f9602028060193e2587ebe9116562e6dbdb5', 'testclient2', 'testuser', '2016-11-01 03:17:00', NULL),
('e8fedd2315f0e31b7609bf85860c18a0e426d7f2', 'testclient2', 'testuser', '2016-11-01 08:17:44', NULL),
('ee6641db87182e3585852f6cba291a9b0df87a52', 'testclient2', 'testuser2', '2016-10-28 05:08:25', NULL),
('f3b229e5a00a8bc10c5829bb99621c006b8dc1be', 'testclient2', 'testuser', '2016-11-01 05:44:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `type` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'supported',
  `scope` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `client_id` varchar(80) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_users`
--

CREATE TABLE `oauth_users` (
  `id` int(10) UNSIGNED NOT NULL,
  -- Added from legacy core_users  
  `role_id` int(11) UNSIGNED NOT NULL, 
  `username` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(2000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,

  -- Added from legacy core_users  
  `created_by_id` int(11) UNSIGNED NULL DEFAULT NULL,
--   `first_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
--   `last_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `enabled` bit(1) NOT NULL DEFAULT 0,
  `profile_fulfilled` bit(1) NOT NULL,
  `telephone1` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telephone2` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `job_title` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `oauth_users`
--

INSERT INTO `oauth_users` (`id`, `role_id`, `username`, `password`, `first_name`, `last_name`) VALUES
(1, 2, 'testuser', '$2y$10$5ICo6mbnWLsptjCZVfMu1e7p04FYpgiZydEG1KD4MI8Q2fcwuCu8e', NULL, NULL),
(2, 2, 'testuser2', '$2y$10$5ICo6mbnWLsptjCZVfMu1e7p04FYpgiZydEG1KD4MI8Q2fcwuCu8e', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `core_passwordrecovery`
--

-- username, token, created_at, timeout

CREATE TABLE `core_password_recovery` (
    `id` int(10) UNSIGNED NOT NULL,
    `user_id` int(10) UNSIGNED NOT NULL,
    `token` varchar(20) NOT NULL,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `timeout` int(10) UNSIGNED NOT NULL,
    `enabled` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `core_audit_logs`
--
ALTER TABLE `core_audit_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `core_configs`
--
ALTER TABLE `core_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_messages`
--
ALTER TABLE `core_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_password_recovery`
--
ALTER TABLE `core_password_recovery`
  ADD PRIMARY KEY (`id`);
  -- ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `core_permissions`
--
ALTER TABLE `core_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_key` (`role_id`,`resource_id`);

--
-- Indexes for table `core_products`
--
ALTER TABLE `core_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `AK_sku` (`sku`);

--
-- Indexes for table `core_products_x_categories`
--
ALTER TABLE `core_products_x_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `category_id` (`category_id`),
  ADD UNIQUE KEY `AK_products_categories` (`product_id`,`category_id`);

--
-- Indexes for table `core_product_categories`
--
ALTER TABLE `core_product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `core_resources`
--
ALTER TABLE `core_resources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `resource` (`resource`,`methodhttp`) USING BTREE;

--
-- Indexes for table `core_roles`
--
ALTER TABLE `core_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_roles_x_products_categories`
--
ALTER TABLE `core_roles_x_products_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `core_sendemail`
--
ALTER TABLE `core_sendemail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_users`
--
-- ALTER TABLE `core_users`
--   ADD PRIMARY KEY (`id`),
--   ADD UNIQUE KEY `AK_username` (`username`),
--   ADD UNIQUE KEY `AK_email` (`email`),
--   ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `core_user_addresses`
--
ALTER TABLE `core_user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `core_user_transactions`
--
ALTER TABLE `core_user_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`access_token`);

--
-- Indexes for table `oauth_authorization_codes`
--
ALTER TABLE `oauth_authorization_codes`
  ADD PRIMARY KEY (`authorization_code`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `oauth_jwt`
--
ALTER TABLE `oauth_jwt`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`refresh_token`);

--
-- Indexes for table `oauth_users`
--
ALTER TABLE `oauth_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `AK_username` (`username`),
  ADD UNIQUE KEY `AK_email` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `core_audit_logs`
--
ALTER TABLE `core_audit_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_configs`
--
ALTER TABLE `core_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_messages`
--
ALTER TABLE `core_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_password_recovery`
--
ALTER TABLE `core_password_recovery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_permissions`
--
ALTER TABLE `core_permissions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `core_products`
--
ALTER TABLE `core_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_products_x_categories`
--
ALTER TABLE `core_products_x_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_product_categories`
--
ALTER TABLE `core_product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_resources`
--
ALTER TABLE `core_resources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `core_roles`
--
ALTER TABLE `core_roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `core_roles_x_products_categories`
--
ALTER TABLE `core_roles_x_products_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_sendemail`
--
ALTER TABLE `core_sendemail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_users`
--
-- ALTER TABLE `core_users`
--   MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_user_addresses`
--
ALTER TABLE `core_user_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_user_transactions`
--
ALTER TABLE `core_user_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;


ALTER TABLE `oauth_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `core_audit_logs`
--
ALTER TABLE `core_audit_logs`
  ADD CONSTRAINT `core_audit_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `oauth_users` (`id`);

--
-- Constraints for table `core_password_recovery`
--
ALTER TABLE `core_password_recovery`
  ADD CONSTRAINT `core_password_recovery_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `oauth_users` (`id`);

--
-- Constraints for table `core_products_x_categories`
--
ALTER TABLE `core_products_x_categories`
  ADD CONSTRAINT `core_products_x_categories_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `core_products` (`id`),
  ADD CONSTRAINT `core_products_x_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `core_product_categories` (`id`);

--
-- Constraints for table `core_product_categories`
--
ALTER TABLE `core_product_categories`
  ADD CONSTRAINT `core_product_categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `core_product_categories` (`id`),
  ADD CONSTRAINT `core_product_categories_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `oauth_users` (`id`);

--
-- Constraints for table `core_roles_x_products_categories`
--
ALTER TABLE `core_roles_x_products_categories`
  ADD CONSTRAINT `core_roles_x_products_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `core_product_categories` (`id`),
  ADD CONSTRAINT `core_roles_x_products_categories_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `core_roles` (`id`);

--
-- Constraints for table `core_users`
--
-- ALTER TABLE `core_users`
-- --   ADD CONSTRAINT `core_users_ibfk_1` FOREIGN KEY (`username`) REFERENCES `oauth_users` (`username`),
--   ADD CONSTRAINT `core_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `core_roles` (`id`),
--   ADD CONSTRAINT `core_users_ibfk_2` FOREIGN KEY (`created_by_id`) REFERENCES `core_users` (`id`);

--
-- Constraints for table `core_user_addresses`
--
ALTER TABLE `core_user_addresses`
  ADD CONSTRAINT `core_user_addresses_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `oauth_users` (`id`);

--
-- Constraints for table `core_user_transactions`
--
ALTER TABLE `core_user_transactions`
  ADD CONSTRAINT `core_user_transactions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `oauth_users` (`id`);

--
-- Constraints for table `oauth_users`
--
ALTER TABLE `oauth_users`
  ADD CONSTRAINT `oauth_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `core_roles` (`id`),
  ADD CONSTRAINT `oauth_users_ibfk_2` FOREIGN KEY (`created_by_id`) REFERENCES `oauth_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;