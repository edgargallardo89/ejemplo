INSERT INTO `core_messages` (`id`, `key`, `description`, `value`, `visibility`) VALUES
	(1, 'auth.reset.title', '', 'Restablecer Contraseña', b'1'),
	(2, 'auth.login.header', '', 'AdBox', b'1'),
	(3, 'auth.login.password-label', '', 'Contraseña', b'1'),
	(4, 'auth.login.username-placeholder', '', 'Usuario', b'1'),
	(5, 'auth.login.password-placeholder', '', 'Contraseña', b'1'),
	(7, 'auth.login.login-button', '', 'Entrar', b'1'),
	(8, 'auth.login.lostPassword-link', '', '¿Olvidaste tu contraseña? Clic aquí', b'1'),
	(9, 'auth.recovery.title', '', '¿Olvidaste tu contraseña?', b'1'),
	(10, 'auth.recovery.header', '', '¿Olvidaste tu contraseña?', b'1'),
	(11, 'auth.recovery.help-text', '', 'Ingresa tu correo electrónico', b'1'),
	(12, 'auth.recovery.identifier-placeholder', '', 'Correo electrónico', b'1'),
	(13, 'auth.recovery.recover-button', '', 'Enviar', b'1'),
	(14, 'auth.recovery.login-link', '', 'Regresar al Inicio de Sesión', b'1'),
	(16, 'auth.reset.header', '', 'Restablecer Contraseña', b'1'),
	(17, 'auth.reset.help-text', '', 'Favor de registrar tu nueva contraseña', b'1'),
	(20, 'auth.reset.password-label', '', 'Contraseña', b'1'),
	(21, 'component.password-input.password-placeholder', '', 'Nueva contraseña', b'1'),
	(22, 'component.password-input.password2-label', '', 'Confirmar contraseña', b'1'),
	(23, 'component.password-input.password2-placeholder', '', 'Confirmar contraseña', b'1'),
	(24, 'auth.reset.reset-button', '', 'Enviar', b'1'),
	(25, 'auth.reset.login-link', '', 'Regresar al Inicio de Sesión', b'1'),
	(26, 'form.validation.required', '', 'Este campo es necesario', b'1'),
	(27, 'form.validation.minlength', '', 'El valor ingresado debe ser mas largo', b'1'),
	(28, 'form.validation.password-strength-length', '', 'La contraseña debe tener al menos 6 caracteres\r\n', b'1'),
	(29, 'form.validation.password-match', '', 'Las contraseñas no coinciden', b'1'),
	(30, 'auth.login.username-label', '', 'Usuario', b'1'),
	(31, 'auth.recovery.identifier-label', '', 'Correo electrónico', b'1'),
	(32, 'form.validation.email', '', 'Ingresa un email válido', b'1'),
	(35, 'auth.login.response.unknown_error', '', 'No se ha completado tu solicitud, por favor intenta más tarde.\r\n', b'1'),
	(36, 'auth.recovery.identifier.validation.noObjectFound', '', 'El e-mail proporcionado no se encuentra registrado en el sistema', b'1'),
	(38, 'auth.recovery.response.success', '', 'Te enviamos un correo electrónico con las instrucciones para restablecer tu contraseña', b'1'),
	(39, 'general.error.unknown', '', 'Ocurrió un error inesperado, por favor contacte al administrador', b'1'),
	(40, 'auth.reset.response.success', '', 'Tu contraseña fue actualizada con éxito', b'1'),
	(41, 'auth.login.response.invalid_grant', '', 'Usuario y/o contraseña inválidos', b'1'),
	(42, 'user.registration.header', '', 'Registrarse', b'1'),
	(43, 'user.registration.name-label', '', 'Nombre', b'1'),
	(44, 'user.registration.name-placeholder', '', 'Nombre', b'1'),
	(45, 'user.registration.lastname-label', '', 'Apellido Paterno', b'1'),
	(46, 'user.registration.lastname-placeholder', '', 'Apellido Paterno', b'1'),
	(47, 'auth.user-registration.email-label', '', 'Email', b'1'),
	(48, 'auth.user-registration.email-placeholder', '', 'Email', b'1'),
	(49, 'user.registration.register-button', '', 'Registrarse', b'1'),
	(50, 'user.registration.login-link', '', '¿Ya tienes cuenta? Inicia sesión', b'1'),
	(51, 'user.registration.response.success', '', 'Te hemos enviado un email con tus datos de inicio de sesión', b'1'),
	(52, 'user.registration.error.User_Exists', '', 'Ya existe un usuario con este correo', b'1'),
	(53, 'form.validation.password-strength-upper', '', 'La contraseña debe tener al menos 1 mayúscula o numero\r\n', b'1'),
	(54, 'form.validation.password-strength-lower', '', 'La contraseña debe tener al menos 1 minúscula', b'1'),
	(55, 'auth.login.userRegistration-link', '', 'Registrarse', b'1'),
	(56, 'auth.reset.error.Current_Password', '', 'No puedes reutilizar tu contraseña', b'1'),
	(57, 'auth.reset.error.Petition_Expired', '', 'La liga ha expirado', b'1'),
	(58, 'component.password-input.password-label', '', 'Contraseña', b'1'),
	(59, 'user.profile-fulfillment.header', '', 'Complemento de datos', b'0'),
	(60, 'user.profile-fulfillment.title', '', 'Complemento de datos', b'0'),
	(61, 'user.profile-fulfillment.firstName-label', '', 'Nombre', b'0'),
	(62, 'user.profile-fulfillment.lastName-label', '', 'Apellido paterno', b'0'),
	(63, 'user.profile-fulfillment.surname-label', '', 'Apellido materno', b'0'),
	(64, 'user.profile-fulfillment.telephone1-label', '', 'Número de teléfono', b'0'),
	(65, 'user.profile-fulfillment.telephone2-label', '', 'Número de celular', b'0'),
	(66, 'auth.profile-fulfillment.email-label', '', 'Correo electrónico', b'0'),
	(67, 'user.profile-fulfillment.firstName-placeholder', '', 'Nombre', b'0'),
	(68, 'user.profile-fulfillment.lastName-placeholder', '', 'Apellido paterno', b'0'),
	(69, 'user.profile-fulfillment.surname-placeholder', '', 'Apellido materno', b'0'),
	(70, 'user.profile-fulfillment.telephone1-placeholder', '', 'Número de teléfono', b'0'),
	(71, 'user.profile-fulfillment.telephone2-placeholder', '', 'Número de celular', b'0'),
	(72, 'auth.profile-fulfillment.email-placeholder', '', 'Correo electrónico', b'0'),
	(73, 'user.profile-fulfillment.register-button', '', 'Enviar', b'0'),
	(74, 'user.profile-fulfillment.response.success', '', '¡Tus datos se han guardado con éxito!', b'0'),
	(75, 'form.validation.telephone', '', 'Ingresa un telefono correcto', b'0');



INSERT INTO core_configs (`key`,value, visibility) 
    VALUES ("auth.registration","open",1),('client.url','http://bootstrap.devadventa.com/', b'0');

INSERT INTO core_products (sku,title,enabled,editable,created_at) 
	VALUES ('C001','Sample Product',1,0,NOW()),
	('C002','Sample Product',2,0,NOW()),
	('C003','Sample Product',3,0,NOW()),
	('C004','Sample Product',4,0,NOW()),
	('C005','Sample Product',5,0,NOW()),
	('C006','Sample Product',6,0,NOW());

INSERT INTO core_product_categories (name,editable,sort,enabled,created_at) 
	VALUES ('Test Category 1',0,1,1,NOW()),
	('Test Category 2',0,1,1,NOW()),
	('Test Category 3',0,1,1,NOW()),
	('Test Category 4',0,1,1,NOW()),
	('Test Category 5',0,1,1,NOW()),
	('Test Category 6',0,1,1,NOW());

INSERT INTO core_products_x_categories (product_id,category_id) 
    VALUES (1,1),(1,2),(1,3);

INSERT INTO core_resources (alias,resource,methodhttp) 
    VALUES ('GET Adbox\\V1\\Rest\\Products\\Controller::collection','Adbox\\V1\\Rest\\Products\\Controller::collection', 'GET');