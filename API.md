============================================================
### API Endpoints

* La forma de generar la URL de las imágenes es mediante el identificador del registro + un path hardcodeado
para poder tener flexibilidad en el storage de las imágenes.


GET /products

    Devuelve todos los productos visibles para el usuario actual

    [{            
        volcar todos los campos de core_products
        +
        categories: [1,2,3,4..] //ids
    }]

GET /categories
    
    Devuelve todos las categoría visibles para el usuario actual
    
    [{            
        volcar todos los campos de core_product_categories
    }]

    
GET /admin-products

    Devuelve todos los productos visibles para el usuario actual. Incluye productos inactivos.

    [{            
        volcar todos los campos de core_products
        +
        categories: [1,2,3,4..] //ids
    }]

POST /admin-products

    Crea un nuevo producto
    
PUT /admin-products/:id
    
    Actualiza un producto existente

    
GET /admin-categories
    
    Devuelve todos las categoría visibles para el usuario actual. Incluye productos inactivos.

    [{
        volcar todos los campos de core_product_categories
    }]
    
POST /admin-categories

    Crea una nueva categoria
    
PUT /admin-categories/:id

    Actualiza una categoria existente
   

GET banners/
    
    Devuelve todos los banners existentes

POST banners/
    
    Crea un nuevo banner, validando dato: {id} como unique.

    curl -X POST \
        -H "Accept: application/json" \
        -H "Content-Type: multipart/form-data" \
        -H "Authorization: Bearer 28f07bbe1f51e24c86c80e2592efb90720dab9de" \
        -F 'id=2' -F "title=Title" \
        -F "description=Description" \
        -F "misc=[{},{},{},{}]" \
        -F file=@/home/telematica/Downloads/a.jpg \
        localhost:8090/banners

PATCH banners/

    Actualiza/edita un banner existente, validando dato: {id} como unique.

    curl -X POST \
        -H "Accept: application/json" \
        -H "Content-Type: multipart/form-data" \
        -H "Authorization: Bearer 28f07bbe1f51e24c86c80e2592efb90720dab9de" \
        -F 'id=2' -F "title=Title" \
        -F "description=Description" \
        -F "misc=[{},{},{},{}]" \
        -F file=@/home/telematica/Downloads/a.jpg \
        localhost:8090/banners/{identificador}

    
--------------------------------------------
crear nueva
--------------------------------------------
POST http://sandbox-api.devadventa.com/adminproductssync
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd
Content-Type: application/json

 -- response --
201 Created
Date:  Thu, 10 Nov 2016 19:48:08 GMT
Server:  Apache/2.4.18 (Ubuntu)
Location:  http://sandbox-api.devadventa.com/adminproductssync/16
Content-Location:  http://sandbox-api.devadventa.com/adminproductssync/16
Content-Length:  24
Keep-Alive:  timeout=5, max=83
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"id":16,"status":"new"}


------------------------------------------------------------------
estado inicial
-----------------------------------------------
GET http://sandbox-api.devadventa.com/adminproductssync/16
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Thu, 10 Nov 2016 19:48:43 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  54
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"id":16,"status":"new","count":{"new":0,"process":0}}

-----------------------------------------------------------------
en "new" 597  y el "process" 0 la cantidad total a procesar
----------------------------------------------------------------

GET http://sandbox-api.devadventa.com/adminproductssync/16
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Thu, 10 Nov 2016 19:49:06 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  56
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"id":16,"status":"new","count":{"new":597,"process":0}}

-------------------------------------------------------------------
en proceso
-------------------------------------------------------------------
GET http://sandbox-api.devadventa.com/adminproductssync/16
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
200 OK
Date:  Thu, 10 Nov 2016 19:51:29 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  58
Keep-Alive:  timeout=5, max=100
Connection:  Keep-Alive
Content-Type:  application/json; charset=utf-8

{"id":16,"status":"new","count":{"new":437,"process":160}}

--------------------------------------------------------------
termino
--------------------------------------------------------------
GET http://sandbox-api.devadventa.com/adminproductssync/16
Accept: application/json
Authorization: Bearer 04439e87c7a8f421374092e8b3bdf40e467dfbcd

 -- response --
404 Not Found
Date:  Thu, 10 Nov 2016 19:55:00 GMT
Server:  Apache/2.4.18 (Ubuntu)
Content-Length:  119
Keep-Alive:  timeout=5, max=91
Connection:  Keep-Alive
Content-Type:  application/problem+json

{"type":"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html","title":"Not Found","status":404,"detail":"Not Found"}