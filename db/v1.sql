CREATE TABLE core_permissions (
  id          INT UNSIGNED           NOT NULL AUTO_INCREMENT PRIMARY KEY,
  role_id     INT                    NOT NULL,
  resource_id INT                    NOT NULL,
  permission  ENUM ('allow', 'deny') NOT NULL
)
  ENGINE = InnoDB;

ALTER TABLE core_permissions
  ADD UNIQUE KEY CK_role_id_resource_id (role_id, resource_id);


CREATE TABLE core_resources (
  id          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  parent_id   INT UNSIGNED NOT NULL,
  type_id     INT UNSIGNED NOT NULL,
  nombre      VARCHAR(255) NOT NULL,
  resource    VARCHAR(125) NOT NULL,
  descripcion TEXT         NOT NULL
)
  ENGINE = InnoDB;

ALTER TABLE core_resources
  ADD UNIQUE KEY AK_resource (resource);


CREATE TABLE core_roles (
  id        INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  role      VARCHAR(40)  NOT NULL,
  parent_id INT UNSIGNED NOT NULL
)
  ENGINE = InnoDB;

CREATE TABLE core_audit_logs (
  id          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  event_type  VARCHAR(255) NOT NULL,
  event_time  TIMESTAMP    NOT NULL,
  user_id     INT UNSIGNED,
  description VARCHAR(255),
  object_type VARCHAR(255),
  object_id   INT UNSIGNED
);

CREATE TABLE core_users (
  id                INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username          VARCHAR(255) NOT NULL,
  first_name        VARCHAR(255),
  surname           VARCHAR(255),
  last_name         VARCHAR(255),
  email             VARCHAR(255),
  display_name      VARCHAR(255),
  enabled           BIT          NOT NULL,
  profile_fulfilled BIT          NOT NULL,
  telephone1        VARCHAR(255),
  telephone2        VARCHAR(255),
  telephone3        VARCHAR(255),
  birthday          DATE,
  gender            ENUM ('m', 'f'),
  job_title         VARCHAR(255),
  created_at        TIMESTAMP    NOT NULL,
  modified_at       TIMESTAMP    NOT NULL
)
  ENGINE = InnoDB;

ALTER TABLE core_users
  ADD UNIQUE KEY AK_username (username);


CREATE TABLE core_user_addresses (
  id         INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id    INT UNSIGNED NOT NULL,
  street     VARCHAR(255),
  ext_number VARCHAR(255),
  int_number VARCHAR(255),
  zip_code   VARCHAR(255),
  reference  VARCHAR(255),
  location   VARCHAR(255),
  city       VARCHAR(255),
  town       VARCHAR(255),
  state      VARCHAR(255),
  main       BIT          NOT NULL
)
  ENGINE = InnoDB;


CREATE TABLE core_product_categories (
  id          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  parent_id   INT UNSIGNED,
  user_id     INT UNSIGNED COMMENT 'si existe, indica que la categoría fue creada por un usuario, en vez de cargada desde motivale',
  name        VARCHAR(255),
  description VARCHAR(255),
  sort        INT          NOT NULL COMMENT 'el orden a mostrar en las vistas',
  enabled     BIT          NOT NULL,
  created_at  TIMESTAMP    NOT NULL,
  modified_at TIMESTAMP    NOT NULL
)
  ENGINE = InnoDB;


CREATE TABLE core_products (
  id          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  sku         VARCHAR(255) NOT NULL,
  title       VARCHAR(255) NOT NULL,
  description TEXT,
  brand       VARCHAR(255),
  real_price  DECIMAL(8, 2) COMMENT 'campo `Precio` en motivale',
  price       INT UNSIGNED COMMENT 'campo `Puntos` desde motivale',
  payload     TEXT COMMENT 'JSON original recibido desde motivale',
  enabled     BIT          NOT NULL,
  created_at  TIMESTAMP    NOT NULL,
  modified_at TIMESTAMP    NOT NULL
)
  ENGINE = InnoDB;

ALTER TABLE core_products
  ADD CONSTRAINT AK_sku UNIQUE (sku);


CREATE TABLE core_products_x_categories (
  id          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  product_id  INT UNSIGNED NOT NULL,
  category_id INT UNSIGNED NOT NULL
)
  ENGINE = InnoDB;


CREATE TABLE core_roles_x_products_categories (
  id          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  role_id     INT UNSIGNED NOT NULL,
  category_id INT UNSIGNED NOT NULL
)
  COMMENT 'categorías accesibles por rol'
  ENGINE = InnoDB;


CREATE TABLE core_user_transactions (
  id               INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id          INT UNSIGNED NOT NULL,
  amount           INT          NOT NULL,
  type             VARCHAR(255) NOT NULL COMMENT 'tipo de transacción order|adjustment|cancelation|etc',
  correlation_id   INT UNSIGNED COMMENT 'id de la entidad relacionada con esta transacción',
  balance_snapshot INT          NOT NULL COMMENT 'el saldo total del usuario después de aplicar esta transacción',
  details          VARCHAR(255) COMMENT 'detalles extra relacionados con la transacción',
  created_at       TIMESTAMP    NOT NULL,
  applied_at       TIMESTAMP    NOT NULL
)
  ENGINE = InnoDB;
